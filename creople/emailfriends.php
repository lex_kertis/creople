<?php
 define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$matches=array();
$term = trim(strip_tags($_GET['term']));
global $user;
$query=db_query("	

SELECT users.name AS users_name, users.uid AS uid, users.mail AS users_mail, users.picture AS users_picture, users.created AS users_created
FROM 
{users} users
LEFT JOIN {user_relationships} user_relationships ON users.uid = user_relationships.requestee_id
LEFT JOIN {users} users_user_relationships ON user_relationships.requester_id = users_user_relationships.uid
LEFT JOIN {field_data_field_name} field_data_field_name ON users.uid = field_data_field_name.entity_id AND (field_data_field_name.entity_type = 'user' AND field_data_field_name.deleted = '0')
WHERE (( (users_user_relationships.uid = '1' ) )AND(( (users.status <> '0') AND (user_relationships.approved = '1') AND (field_data_field_name.field_name_value LIKE '%".$term."%' ) )))");
foreach($query as $q){

$output['label'] = $q->users_name;
$output['value'] = "{$q->users_mail}";
$matches[]=$output;
}
$matches = array_slice($matches, 0, 5);
print json_encode($matches);
?>