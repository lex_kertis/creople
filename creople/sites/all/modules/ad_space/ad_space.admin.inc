<?php
/**
 * @file
 * This module provides place holders for ads in your site.
 *
 * @ingroup ad_space
 */

/**
 * Implementation of hook_admin_settings().
 */
function ad_space_admin_settings() {
  $form['#submit'][] = 'ad_space_admin_settings_submit';
  $form['ad_space_admin'] = array(
  '#type' => 'fieldset',
  '#title' => t('Ad Space Settings'),
  '#description'=> t('Option for the Ad Space module.'),
  '#weight' => -2,
  '#collapsible' => FALSE,
  '#collapsed' => FALSE,
);
  /*Description: Format a multiple-line text field.
  http://api.drupal.org/api/drupal/developer--topics--forms_api_reference.html/6#textfield*/
    $form['ad_space_admin']['ad_space_ad_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter an Ad Link'),
    '#default_value' => variable_get('ad_space_ad_link', 'none'),
    '#description'=> t('By entering an Ad Link, all the ads on your site will point to that URL- in a blank page. Please use this format: <em>http://www.example.com</em> NOTE: this doesn\'t work with the default option!'),
    '#required' => FALSE
    );
    $form['ad_space_admin']['ad_space_user_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Define your own Ad Class'),
    '#default_value' => variable_get('ad_space_user_class', 'none'),
    '#description'=> t('By entering an ad class, you can populate the advertisements with your own! Just copy the default ad-sprite.png (if you edit it in Fireworks you will see all the layers!) and copy the default.css (in the "assets" directory) to alter/add your class name!'),
    '#required' => FALSE
    );
    $form['ad_space_admin']['ad_space_graphical_ads'] = array(
    '#type' => 'select',
    '#title' => t('Select Default (inline-styles) or a Graphic Ads Set'),
    '#default_value' => variable_get('ad_space_graphical_ads', 'default'),
    '#options' => array(
       'default' => t('Default'),
       'drupal' => t('Drupal.org'),
       'nps' => t('NorthPoint Solutions'),
      ),
    '#description' => t('Select the type of mock graphical ads you would like to use.'),
    '#required' => TRUE
    );
    return system_settings_form($form);
}

function ad_space_admin_settings_validate($form, &$form_state) {
  //nothing to do here, this is just a placeholder
}

function ad_space_admin_settings_submit($form, &$form_state) {
  variable_set('ad_space_user_class', $form_state['values']['ad_space_user_class']);
  variable_set('ad_space_graphical_ads', $form_state['values']['ad_space_graphical_ads']);
  variable_set('ad_space_ad_link', $form_state['values']['ad_space_ad_link']);
}