<?php
// $Id$
/**
 * @file
 * Advanced title fields helper.
 */

function advanced_title_get_field_instance($entity, $bundle, $include_deleted = FALSE) {
  $instance = array();
  foreach (array_keys(advanced_title_fields()) as $field) {
    $include_additional = array();
    if ($include_deleted) {
      $include_additional['include_deleted'] = 1;      
    }
    $instance[] =  field_read_instance($entity, $field, $bundle, $include_additional);
  }
  return array_filter($instance);
}

function advanced_title_insert_new_field_instance($entity, $bundle) {
  $modes = entity_get_info($entity);
  // Create all the instances for our fields.
  foreach (advanced_title_instances() as $instance) {
    $instance['entity_type'] = $entity;
    $instance['bundle'] = $bundle;
    // Or use field_ui_field_ui_view_modes_tabs()
    foreach ($modes['view modes'] as $view_mode => $value) {
      $instance['display'][$view_mode]['label']  = $instance['display']['default']['label'];
      $instance['display'][$view_mode]['type']   = $instance['display']['default']['type'];
      $instance['display'][$view_mode]['weight'] = $instance['display']['default']['weight'];
    }
    field_create_instance($instance);
  }
}

function advanced_title_undelete_field_instance($entity, $bundle) {
  $return = advanced_title_get_field_instance($entity, $bundle, TRUE);
  foreach (array_keys(advanced_title_fields()) as $field) {
    db_update('field_config_instance')
      ->fields(array('deleted' => 0))
      ->condition('field_name', $field)
      ->condition('entity_type', $entity)
      ->condition('bundle', $bundle)
      ->condition('deleted', 1)
      ->execute();
    db_update('field_data_' . $field)
      ->fields(array('deleted' => 0))
      ->condition('bundle', $bundle)
      ->condition('deleted', 1)
      ->execute();
    db_update('field_revision_' . $field)
      ->fields(array('deleted' => 0))
      ->condition('bundle', $bundle)
      ->condition('deleted', 1)
      ->execute();
  }
  return $return;
}

/**
 * Delete all fields marked as deleted
 *
 * @see field_purge_batch()
 */
function advanced_title_delete_permanent_field_data($entity, $bundle, $batch_size = ADVANCED_TITLE_MAX_FIELD_BATCH) {
  $instances = advanced_title_get_field_instance($entity, $bundle, TRUE);
  $balance = 0;
  foreach ($instances as $instance) {
    $field = field_info_field_by_id($instance['field_id']);
    // Retrieve some pseudo-entities.
    $query = new EntityFieldQuery();
    $results = $query
      ->fieldCondition($field)
      ->entityCondition('bundle', $bundle)
      ->deleted(TRUE)
      ->range(0, $batch_size)
      ->execute();
    $balance += count($results);
    if ($results) {
      // Field data for the instance still exists.
      foreach ($results as $entity_type => $stub_entities) {
        field_attach_load($entity_type, $stub_entities, FIELD_LOAD_CURRENT, array('field_id' => $field['id'], 'deleted' => 1));
        foreach ($stub_entities as $id => $entity) {
          // Purge the data for the entity.
          field_purge_data($entity_type, $entity, $field, $instance);
        }
      }      
    }
    // Purges a field instance record from the database
    field_purge_instance($instance);
  }
  return $balance;
}

function advanced_title_fields() {
  return array(
    'titles' => array(
      'field_name'  => 'titles',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'type'        => 'text',
    ),
    'title_image' => array(
      'field_name'  => 'title_image',
      'type'        => 'title_image',
      'cardinality' => 1,
    ),
  );
}

function advanced_title_instances() {
  return array(
    'titles' => array(
      'field_name' => 'titles',
      'label'      => t('Title'),
      'required' => TRUE,
      'widget' => array(
        'type'   => 'text_textfield',
        'weight' => -5,
      ),
      'display' => array(
        'default' => array(
          'label'  => 'hidden',
          'type'   => 'hidden',
          'weight' => 0,
        ),
      ),
    ),
    'title_image' => array(
      'field_name'  => 'title_image',
      'label'       => t('Content title image logo'),
      'description' => t('Upload image that will appear as logo of the title.'),
      'widget' => array(
        'type' => 'title_image_builder',
      ),
      'display' => array(
        'default' => array(
          'label'  => 'hidden',
          'type'   => 'title_image_formatter',
          'weight' => 0,
        ),
      ),
      'settings' => array(
        'file_extensions' => 'png gif jpg jpeg',
        'file_directory'  => 'pictures/title_logo',
        'max_filesize'    => '',
        'alt_field'       => TRUE,
        'title_field'     => FALSE,
        'max_resolution'  => '',
        'min_resolution'  => '',
      ),
    ),
  );
}
