// $Id$

(function ($) {
  Drupal.behaviors.advancedTitle = {
    attach: function (context, settings) {
      $('fieldset.advanced-title-form', context).drupalSetSummary(function (context) {
        var values   = new Array();
        var elements = new Array();
        var lang;
        if (settings.advanced_title == undefined) {
          lang = 'und';
        }
        else {
          lang = settings.advanced_title.language;
        }
        if ($(':input[name="title_image_' + lang + '_0_upload_button"]').attr('value')) {
          elements[':input[name="files[title_image_' + lang + '_0]"]'] = Drupal.t('No logo');
        }
        elements[':input[name="menu_item_class"]'] = Drupal.t('No CSS class');
        for (var index in elements) {
          if (!$(index, context).attr('value')) {
            values.push(elements[index]);
          }
        }
        return values.join(', ');
      });
    }
  };
})(jQuery);