<?php

function creopleapi_services_resources() {
  $resources = array(
/*
    'creople_contact' => array(
      'operations' => array(
          'create' => array(
              'help' => 'Send contact form',
              'callback' => '_creopleapi_node_send_contact_form',
              //'access arguments' => array('access content'),
              'args' => array(
                  array(
                      'name' => 'data',
                      'type' => 'array',
                      'description' => 'A valid contact form data',
                      'source' => 'data',
                      'optional' => FALSE,
                  ),
              ),
              //'access callback' => true,
              'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          ),),),
*/
    'creople_user' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a user',
          'callback' => '_creopleapi_user_retrieve',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'access callback' => '_user_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'uid',
              'type' => 'int',
              'description' => 'The uid of the user to retrieve.',
              'source' => array('path' => 0),
              'optional' => FALSE,
            ),
          ),
        ),

        'update' => array(
          'help' => 'Update a user',
          'callback' => '_creopleapi_user_update',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'access callback' => '_user_resource_access',
          'access callback file' => array('type' => 'inc', 'module' => 'services', 'name' => 'resources/user_resource'),
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'uid',
              'type' => 'int',
              'description' => 'Unique identifier for this user',
              'source' => array('path' => 0),
              'optional' => FALSE,
            ),
            array(
              'name' => 'data',
              'type' => 'array',
              'description' => 'The user object with updated information',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
      'actions' => array(
        'send_contact' => array(
          'help' => t('Send the Make Creople Better form'),
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_user_send_contact_form',
          'args' => array(
            array(
              'name' => 'data',
              'type' => 'array',
              'description' => 'The contact form object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
          'access callback' => '_creople_contact_form_access',
          'access callback file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'access arguments' => array('send'),
          'access arguments append' => FALSE,
        ),

        'register' => array(
          'help' => t('Register a new user'),
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_user_register',
          'args' => array(
            array(
              'name' => 'data',
              'type' => 'array',
              'description' => 'The user object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
          'access callback' => '_user_resource_access',
          'access callback file' => array('type' => 'inc', 'module' => 'services', 'name' => 'resources/user_resource'),
          'access arguments' => array('create'),
          'access arguments append' => FALSE,
        ),
        'login' => array(
          'help' => 'Login a user for a new session',
          'callback' => '_creopleapi_user_login',
          'args' => array(
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
          ),
          'access callback' => 'services_access_menu',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
        ),
        'logout' => array(
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'help' => 'Logout a user session',
          'callback' => '_creopleapi_user_logout',
          'access callback' => 'services_access_menu',
        ),
      ),
      'targeted_actions' => array(
        'attach_file' => array(
          'help' => 'Upload and attach file(s) to a user. POST multipart/form-data to creople_user/123/attach_file',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_user_attach_file',
          'access callback' => '_user_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'uid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The uid of the user to attach a file to',
            )
          ),
        ),
        'node_list' => array(
          'help' => 'Get user nodes',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_user_node_list',
          'access callback' => '_user_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'uid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The uid of the user to attach a file to',
            ),
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('data' => 'page'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('data' => 'pagesize'),
            ),
          ),
        )
      ),
      'relationships' => array(
        'creopling' => array(
          'help' => 'Get creopling users. creople_user/123/creopling',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_user_creopling',
          'access callback' => '_user_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'uid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the user to get creopling users to',
            )
          ),
        ),
        'favourites' => array(
          'help' => 'Get favourite posts. creople_user/123/favourites',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_user_favourites',
          'access callback' => '_user_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'uid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The uid of the user to get favourite posts to',
            )
          ),
        ),
      ),
    ),
    'creople_node' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a node',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_retrieve',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to retrieve',
            ),
          ),
          'access callback' => '_node_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a node',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_create',
          'args' => array(
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to create',
              'type' => 'array',
            ),
          ),
          'access callback' => '_node_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a node',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to update',
            ),
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => '_node_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a node'),
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to delete',
            ),
          ),
          'access callback' => '_node_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all nodes',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'pagesize'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
      ),
      'targeted_actions' => array(
        'update' => array(
          'help' => 'Update a node',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to update',
            ),
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => '_node_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'attach_file' => array(
          'help' => 'Upload and attach file(s) to a node. POST multipart/form-data to node/123/attach_file',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_attach_file',
          'access callback' => '_node_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to attach a file to',
            ),
            array(
              'name' => 'field_name',
              'optional' => FALSE,
              'source' => array('data' => 'field_name'),
              'description' => 'The file field name',
              'type' => 'string',
            ),
            array(
              'name' => 'attach',
              'optional' => TRUE,
              'source' => array('data' => 'attach'),
              'description' => 'Attach the file(s) to the node. If FALSE, this clears ALL files attached, and attaches the files',
              'type' => 'int',
              'default value' => TRUE,
            ),
            array(
              'name' => 'field_values',
              'optional' => TRUE,
              'source' => array('data' => 'field_values'),
              'description' => 'The extra field values',
              'type' => 'array',
              'default value' => array(),
            ),
          ),
        ),
        'vote_up' => array(
          'help' => 'Vote Up. creople_node/123/vote_up',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_vote_up',
          'access callback' => '_creopleapi_vud_access_callback',
          'access arguments' => array('use vote up/down', 'node', 1, 'vote'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to vote up to',
            )
          ),
        ),
        'vote_down' => array(
          'help' => 'Vote Up. creople_node/123/vote_down',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_vote_down',
          'access callback' => '_creopleapi_vud_access_callback',
          'access arguments' => array('use vote up/down', 'node', -1, 'vote'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to vote down to',
            )
          ),
        ),
        'flag' => array(
          'help' => 'Vote Up. creople_node/123/flag',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_flag',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to flag to',
            ),
            array(
              'name' => 'status',
              'optional' => FALSE,
              'source' => array('data' => 'status'),
              'type' => 'string',
              'description' => 'flag / unflag',
            )
          ),
        ),
        'comment' => array(
          'help' => 'Create Comment. creople_node/123/comment',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_create_comment',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to flag to',
            ),
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'array',
              'description' => 'Comment Data',
            )
          ),
        ),
        'favourite' => array(
          'help' => 'Favourite. creople_node/123/favourite',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_favourite',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to favourite to',
            ),
            array(
              'name' => 'status',
              'optional' => FALSE,
              'source' => array('data' => 'status'),
              'type' => 'string',
              'description' => 'flag / unflag',
            )
          ),
        ),
      ),
      'relationships' => array(
        'files' => array(
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'help'   => 'This method returns files associated with a node.',
          'access callback' => '_node_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'callback' => '_creopleapi_node_load_node_files',
          'args'     => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node whose files we are getting',
            ),
            array(
              'name' => 'file_contents',
              'type' => 'int',
              'description'  => t('To return file contents or not.'),
              'source' => array('path' => 2),
              'optional' => TRUE,
              'default value' => TRUE,
            ),
            array(
              'name'         => 'image_styles',
              'type'         => 'int',
              'description'  => t('To return image styles or not.'),
              'source'       => array('path' => 3),
              'optional'     => TRUE,
              'default value' => FALSE,
            ),
          ),
        ),
        'comments' => array(
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'help'                    => 'This method returns the number of new comments on a given node.',
          'access callback'         => 'user_access',
          'access arguments'        => array('access comments'),
          'access arguments append' => FALSE,
          'callback'                => '_creopleapi_node_comments',
          'args'                    => array(
            array(
              'name'         => 'nid',
              'type'         => 'int',
              'description'  => t('The node id to load comments for.'),
              'source'       => array('path' => 0),
              'optional'     => FALSE,
            ),
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'pagesize'),
            ),
          ),
        ),
        'vote' => array(
          'help' => 'Get Vote Score. creople_node/123/vote',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_vote',
          'access callback' => '_node_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to get vote score to',
            )
          ),
        ),
      ),
      'actions' => array(
        'search' => array(
          'help' => 'Search Nodes',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_search',
          'access callback' => 'search_is_active',
          'args' => array(
            array(
              'name' => 'query',
              'optional' => FALSE,
              'type' => 'string',
              'description' => 'Search query',
              'source' => array('data' => 'query'),
            ),
          ),
        ),
        'flagged_posts' => array(
          'help' => 'List all flagged nodes',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_flagged',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('data' => 'page'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('data' => 'pagesize'),
            ),
          ),
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
        ),
        'most_popular' => array(
          'help' => 'List most popular posts',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_node_most_popular',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('data' => 'page'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('data' => 'pagesize'),
            ),
          ),
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
        ),
      )
    ),
    'creople_category' => array(
      'actions' => array(
        'list' => array(
          'help' => 'Get all categories',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_category_list',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(),
        ),
      ),
      'targeted_actions' => array(
        'node_list' => array(
          'help' => 'Get nodes of category',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_category_node_list',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'cid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The uid of the user to attach a file to',
            ),
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('data' => 'page'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('data' => 'pagesize'),
            ),
          ),
        ),
      )
    ),
    'creople_friend' => array(
      'operations' => array(
        'index' => array(
          'help' => 'Get all friends',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_list',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(),
        ),
      ),
      'relationships' => array(
        'pendings' => array(
          'help' => 'Pendings',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_pendings',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name'         => 'uid',
              'type'         => 'int',
              'description'  => t('The user id to get friends for.'),
              'source'       => array('path' => 0),
              'optional'     => FALSE,
            ),
          ),
        ),
        'requests' => array(
          'help' => 'Requests',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_requests',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name'         => 'uid',
              'type'         => 'int',
              'description'  => t('The user id to get friends for.'),
              'source'       => array('path' => 0),
              'optional'     => FALSE,
            ),
          ),
        ),
      ),
      'actions' => array(
        'invite' => array(
          'help' => 'Approve friend request',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_invite',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'array',
              'description' => 'Invite Data',
            ),
          ),
        ),
        'remove' => array(
          'help' => 'Approve friend request',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_remove',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'array',
              'description' => 'Relationship Data',
            ),
          ),
        ),
        'approve' => array(
          'help' => 'Approve friend request',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_approve',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'array',
              'description' => 'Relationship Data',
            ),
          ),
        ),
        'disapprove' => array(
          'help' => 'Disapprove friend request',
          'file' => array('type' => 'inc', 'module' => 'creopleapi', 'name' => 'creopleapi.resource'),
          'callback' => '_creopleapi_friend_disapprove',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'data',
              'optional' => FALSE,
              'source' => 'data',
              'type' => 'array',
              'description' => 'Relationship Data',
            ),
          ),
        ),
      )
    )
  );
  return $resources;
}
