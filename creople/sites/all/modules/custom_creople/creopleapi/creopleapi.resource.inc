<?php
function _creopleapi_user_login($username, $password) {
  $result = _user_resource_login($username, $password);
  if (is_object($result) && isset($result->user)) {
    $result->user = _creopleapi_user_retrieve($result->user->uid);
  }
  return $result;
}
function _creopleapi_user_logout() {
  return _user_resource_logout();
}
/**
 * Register an user.
 *
 * @param $data
 *   Fields to create an user.
 *
 * @return
 *   The modified user object.
 */
function _creopleapi_user_register($data) {
  global $language;

  try {

    $result = array('status'=>'no-valid', 'errors' => array());
    if (!isset($data['name']) || $data['name'] == '') {
      $result['errors']['name'] = 'Name is required.';
    }
    if (!isset($data['username']) || $data['username'] == '') {
      $result['errors']['username'] = 'Username is required.';
    }
    if (!isset($data['password']) || $data['password'] == '') {
      $result['errors']['password'] = 'Password is required.';
    }
    if (!isset($data['email']) || !valid_email_address($data['email'])) {
      $result['errors']['email'] = 'Email address is not valid.';
    }
    if (!isset($data['dob']) || $data['dob'] == '') {
      $result['errors']['dob'] = 'Date of birthday is required.';
    } else {
      $dob = new DateObject($data['dob'], $user->timezone);
    }

    if (count($result['errors']) > 0) {
      return services_error(t('Registration Failed'), 401);
    }

    //set up the user fields
    $fields = array(
      'name'    => $data['username'],
      'mail'    => $data['email'],
      'pass'    => $data['password'],
      'status'  => 1,
      'timezone'=> variable_get('date_default_timezone', ''),
      'init'    => $data['email'],
      'roles'   => array(
        DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      ),
      'field_date_of_birth' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $dob->format('Y-m-d 00:00:00'),
          ),
        ),
      ),
      'field_name' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $data['name'],
          ),
        ),
      ),
      'field_likes_dislikes' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => 1,
          ),
        ),
      ),
      'field_terms_of_use' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => 1,
          ),
        ),
      ),
    );

    //return $fields;
    //the first parameter is left blank so a new user is created
    $account = user_save('', $fields);

    // Send welcome email
    $mail = _user_mail_notify('register_no_approval_required', $account, $language);
    
    return _user_resource_login($data['username'], $data['password']);

  } catch (Exception $e) {
    if (isset($e->errorInfo[1]) && $e->errorInfo[1] == 1062) {
      $result['errors']['username'] = 'user already registered';
    }
    return services_error(t('Registration Failed'), 401, $result);
  }
}

/**
 * Get user details.
 *
 * @param $uid
 *   UID of the user to be loaded.
 *
 * @return
 *   A user object.
 *
 * @see user_load()
 */
function _creopleapi_user_retrieve($uid) {
  $account = _user_resource_retrieve($uid);
  if (isset($account->field_profile_picture[LANGUAGE_NONE])) {
    $account->field_profile_picture[LANGUAGE_NONE][0]['url'] = file_create_url($account->field_profile_picture[LANGUAGE_NONE][0]['uri']);
  } else {
    $account->field_profile_picture[LANGUAGE_NONE][0]['url'] = file_create_url('public://default_avatar.png');
  }
  return $account;
}

/**
 * Update an user.
 *
 * @param $uid
 *   The uid of the user to update to
 *
 * @param $data
 *   Fields to modify for this user.
 *
 * @return
 *   The modified user object.
 */
function _creopleapi_user_update($uid, $data) {
  global $user;
  try {
    $result = array();

    if (isset($data['name']) && $data['name'] == '') {
      $result['errors']['name'] = 'Name is required.';
    }
    if (isset($data['email']) && !valid_email_address($data['email'])) {
      $result['errors']['email'] = 'Email address is not valid.';
    }
    if (isset($result['errors']) && count($result['errors']) > 0) {
      $result['status'] = 'no-valid';
      return services_error(t('Failed to update'), 401);
    }

    //set up the user fields
    $fields = array();
    if (isset($data['name'])) {
      $fields['field_name'] = array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $data['name'],
          ),
        )
      );
    }
    if (isset($data['phone_number'])) {
      $fields['field_phone_number'] = array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $data['phone_number'],
          ),
        )
      );
    }
    if (isset($data['email'])) {
      $fields['mail'] = $data['email'];
      $fields['init'] = $data['email'];
    }
    $notifies = array();
    if (isset($data['notify_email']) && $data['notify_email'] == 1) {
      $notifies[] = array('value' => 'Email');
    }
    if (isset($data['notify_sms']) && $data['notify_sms'] == 1) {
      $notifies[] = array('value' => 'SMS');
    }
    if (isset($data['notify_push']) && $data['notify_push'] == 1) {
      $notifies[] = array('value' => 'Push');
    }
    $fields['field_notification'] = array();
    if (count($notifies) > 0) {
      $fields['field_notification'] = array(
        LANGUAGE_NONE => $notifies
      );
    }
    if (isset($data['likes'])) {
      $fields['field_likes'] = array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $data['likes'],
          ),
        )
      );
    }
    if (isset($data['dislikes'])) {
      $fields['field_dislikes'] = array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $data['dislikes'],
          ),
        )
      );
    }
    if (isset($data['password']) && $data['password'] != '') {
      $fields['pass'] = $data['password'];
    }
    if (isset($data['dob']) && $data['dob'] != '') {
      $dob = new DateObject($data['dob'], $user->timezone);
      $fields['field_date_of_birth'] = array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $dob->format('Y-m-d 00:00:00'),
          ),
        ),
      );
    }

    //return $fields;
    //the first parameter is left blank so a new user is created
    if (count($fields) > 0) {
      $account = user_load($uid);
      user_save($account, $fields);
      _user_resource_update_services_user($uid, time());
    }

    $result['status'] = 'success';
    return $result;

  } catch (Exception $e) {
    return services_error(t('Failed to update'), 401, $result);
  }
}

function _creopleapi_user_creopling($uid) {
  global $user;

  $creoplings = array();

  if ($user->uid != $uid) {
    return false;
  }

  $result = db_query("SELECT DISTINCT comment.nid AS comment_nid, comment.uid AS comment_uid, node_comment.uid AS node_comment_uid, users_comment.picture AS users_comment_picture, users_comment.uid AS users_comment_uid, users_comment.name AS users_comment_name, users_comment.mail AS users_comment_mail, comment.name AS comment_name, comment.homepage AS comment_homepage, comment.created AS comment_created, 'user' AS field_data_field_profile_picture_user_entity_type
    FROM 
    {comment} comment
    LEFT JOIN {users} users_comment ON comment.uid = users_comment.uid
    LEFT JOIN {node} node_comment ON comment.nid = node_comment.nid
    INNER JOIN {field_data_field_creople_me} field_data_field_creople_me ON comment.cid = field_data_field_creople_me.entity_id AND (field_data_field_creople_me.entity_type = 'comment' AND field_data_field_creople_me.deleted = '0')
    WHERE (( (node_comment.uid = '$uid') )AND(( (comment.status <> '0') AND (node_comment.status = '1') AND( (users_comment.uid <> '$uid') )AND (field_data_field_creople_me.field_creople_me_value = '1') )))
    ORDER BY comment_created DESC
    LIMIT 6 OFFSET 0");
  
  foreach ($result as $creopling_user) {
    $creopling = _creopleapi_user_retrieve($creopling_user->comment_uid);
    $creopling->creopled_time = $creopling_user->comment_created;
    $creopling->creopled_nid = $creopling_user->comment_nid;
    $creoplings[] = $creopling;
  }

  return $creoplings;
}

/**
 * Upload and attach file to user.
 *
 * @param $uid
 *   The uid of the user to attach a file to
 * @param $field_name
 *   The file field name
 * @param $attach
 *   Attach the file(s) to the user. If FALSE, this clears ALL files attached, and attaches the files
 * @param $field_values
 *   Fields to create an user.
 *
 * @return
 *   The modified user object.
 */
function _creopleapi_user_attach_file($uid) {
  $file = file_save_upload('profile', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array('png gif jpg jpeg'),
  ));
  if ($file) {
    $file = file_move($file, 'public://');
    if (!$file) {
      return services_error(t('Failed to write the uploaded file the site\'s file folder.'), 401);
    }
  } else {
    return services_error(t('No file was uploaded.'), 401);
  }
  $account = user_load($uid);

  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);

  $file = get_object_vars($file);
  $fields['field_profile_picture'] = array(
    LANGUAGE_NONE => array(
      0 => $file
    )
  );
  user_save($account, $fields);

  $file['url'] = file_create_url($file['uri']);

  return $file;

}

function _creopleapi_user_node_list($uid, $page, $pagesize) {

  $nodes = _node_resource_index($page, '*', array('uid' => $uid), $pagesize);

  for ($i=0; $i<count($nodes); $i++) {
    $node = _creopleapi_node_retrieve($nodes[$i]->nid);
    
    $nodes[$i] = $node;
  }

  return $nodes;
}

function _creopleapi_user_favourites($uid) {
  global $user;

  $favourites = array();

  if ($user->uid != $uid) {
    return false;
  }

  $result = db_query("SELECT node.created AS node_created, node.nid AS nid
    FROM 
    {node} node
    INNER JOIN {flagging} flagging_node ON node.nid = flagging_node.entity_id AND (flagging_node.fid = '4' AND flagging_node.uid = '$uid')
    WHERE (( (node.status = '1') AND (flagging_node.uid IS NOT NULL ) AND (node.type IN  ('posts')) ))
    ORDER BY node_created DESC");
  
  foreach ($result as $favourite_node) {
    $node = _creopleapi_node_retrieve($favourite_node->nid);
    $favourites[] = $node;
  }

  return $favourites;
}

/**
 * Returns the results of a node_load() for the specified node.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the node we want to return.
 * @return
 *   Node object or FALSE if not found.
 *
 * @see node_load()
 */
function _creopleapi_node_retrieve($nid) {
  $node = _node_resource_retrieve($nid);

  $node->body[LANGUAGE_NONE][0]['value'] = wordfilter_filter_process($node->body[LANGUAGE_NONE][0]['value']);
  $node->body[LANGUAGE_NONE][0]['safe_value'] = wordfilter_filter_process($node->body[LANGUAGE_NONE][0]['safe_value']);

  if (!empty($node->field_upload_media)) {
    $node->field_upload_media[LANGUAGE_NONE][0]['url'] = file_create_url($node->field_upload_media[LANGUAGE_NONE][0]['uri']);
  }

  if (!empty($node->field_upload_video)) {
    $node->field_upload_video[LANGUAGE_NONE][0]['url'] = file_create_url($node->field_upload_video[LANGUAGE_NONE][0]['uri']);
  }

  if (!empty($node->field_upload_audio)) {
    $node->field_upload_audio[LANGUAGE_NONE][0]['url'] = file_create_url($node->field_upload_audio[LANGUAGE_NONE][0]['uri']);
  }

  for ($i=0; $i<count($node->field_categories[LANGUAGE_NONE]); $i++) {
    $category = taxonomy_term_load($node->field_categories[LANGUAGE_NONE][$i]['tid']);
    $node->field_categories[LANGUAGE_NONE][$i]['name'] = $category->name;
  }
  
  $node_user = _creopleapi_user_retrieve($node->uid);    
  $node->user_name = $node_user->name;
  $node->user_picture_url = $node_user->field_profile_picture[LANGUAGE_NONE][0]['url'];
  $node->user_fullname = $node_user->field_name[LANGUAGE_NONE][0]['value'];

  $flag = flag_get_flag('report');
  $node->flag_report = $flag->is_flagged($nid);

  $flag = flag_get_flag('favourite');
  $node->flag_favourite = $flag->is_flagged($nid);
  $node->title = utf8_decode($node->title);
  $node->body['und'][0]['value'] = utf8_decode($node->body['und'][0]['value']);

  return $node;
}

/**
 * Return an array of optionally paged nids baed on a set of criteria.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $page_size
 *   Integer number of items to be returned.
 * @return
 *   An array of node objects.
 *
 */
function _creopleapi_node_index($page, $pagesize) {
  $nodes = _node_resource_index($page, '*', array(), $pagesize);

  for ($i=0; $i<count($nodes); $i++) {
    $node = _creopleapi_node_retrieve($nodes[$i]->nid);
    
    $nodes[$i]->user_name = $node->user_name;
    $nodes[$i]->user_picture_url = $node->user_picture_url;
    $nodes[$i]->user_fullname = $node->user_fullname;
    $nodes[$i]->body = $node->body[LANGUAGE_NONE][0]['value'];
  }

  return $nodes;
}

function _creopleapi_vud_access_callback($perm, $entity_type, $value, $tag, $args = array()) {
  return vud_access_callback($perm, $entity_type, $args[0], $value, $tag);
}

function _creople_contact_form_access($action) {
  return true;
}
function _creopleapi_vud_vote($type, $entity_id, $value, $tag, $widget, $token) {
  // If the user is anonymous we don't need to check for a token.
  if (!is_numeric($value) || !drupal_valid_token($token, "vote/$type/$entity_id/$value/$tag/$widget", TRUE)) {
    return MENU_ACCESS_DENIED;
  }

  $vote = array();
  $casted_vote_criteria = array(
    'entity_type' => $type,
    'entity_id' => $entity_id,
    'tag' => $tag,
  ) + votingapi_current_user_identifier();
  $casted_vote = votingapi_select_single_vote_value($casted_vote_criteria);

  // Sanity-check the incoming values.
  if ($value > 0) {
    $value = 1;
  }
  elseif ($value < 0) {
    $value = -1;
  }
  else {
    return FALSE;
  }

  $vote['value'] = $value;
  $vote['value_type'] = 'points';
  $tag = $tag ? $tag : variable_get('vud_tag', 'vote');
  $vote['tag'] = $tag;
  $vote['entity_id'] = $entity_id;
  $vote['entity_type'] = $type;
  $votes = array(0 => $vote);
  drupal_alter('vud_votes', $votes);

  // Do not allow to vote with the same value.
  if ($casted_vote != $votes[0]['value']) {
    votingapi_set_votes($votes);
  }

  $criteria = array(
    'entity_type' => $type,
    'entity_id' => $entity_id,
    'value_type' => 'points',
    'tag' => 'vote',
    'function' => ($value == 1) ? ('positives') : ('negatives')
  );

  return (int)votingapi_select_single_result_value($criteria);
}

function _creopleapi_node_vote($nid) {
  $criteria = array(
    'entity_type' => 'node',
    'entity_id' => $nid,
    'value_type' => 'points',
    'tag' => 'vote',
    'function' => 'positives'
  );
  $up_score = (int)votingapi_select_single_result_value($criteria);
  $criteria = array(
    'entity_type' => 'node',
    'entity_id' => $nid,
    'value_type' => 'points',
    'tag' => 'vote',
    'function' => 'negatives'
  );
  $down_score = (int)votingapi_select_single_result_value($criteria);

  return array(
    'up_score' => $up_score,
    'down_score' => $down_score
  );
}

function _creopleapi_node_vote_up($nid) {
  $token_up = drupal_get_token("vote/node/$nid/1/vote/upanddown");
  $vote_up_score = _creopleapi_vud_vote('node', $nid, 1, 'vote', 'upanddown', $token_up);

  return array('up_score' => $vote_up_score);
}

function _creopleapi_node_vote_down($nid) {
  $token_up = drupal_get_token("vote/node/$nid/-1/vote/upanddown");
  $vote_down_score = _creopleapi_vud_vote('node', $nid, -1, 'vote', 'upanddown', $token_up);

  return array('down_score' => $vote_down_score);
}

function _creopleapi_node_flag($nid, $status) {

  $flag = flag_get_flag('report');

  $result = $flag->flag($status, $nid);

  return $result;
}

function _creopleapi_node_create($data) {
  global $user;

  try {
    $errors = array();

    $validate_fields = array(
      'type' => 'Type',
      'title' => 'Title',
      'category_id' => 'Category',
      /*'city' => 'City',
      'province' => 'Province',
      'postal_code' => 'Postal Code',
      'country' => 'Country'*/
      'body' => 'Post'
    );

    foreach ($validate_fields as $key => $title) {
      if (!isset($data[$key]) || $data[$key] == '') {
        $errors[$key] = $title . ' is required field.';
      }
    }

    if (count($errors) > 0) {
      return services_error(t('Post Failed'), 403);
      //return array('error_type' => 'validation', 'errors' => $errors)
    }
    $buffer = $data['body'];
/*********************************************************/
   // $data['body'] = json_encode($data['body']);
   // $data['title'] = json_encode($data['title']);
/********************************************************/

    $node = new stdClass();
    $node->title = $data['title'];
    $node->type = $data['type'];
    node_object_prepare($node);

    $node->language = LANGUAGE_NONE;
    $node->uid = $user->uid; 
    $node->status = 1;
    $node->promote = 1;
    $node->comment = 2;


   /*
    drupal_mail('system', 'mail', 'lexkertis@gmail.com', language_default(), array(
	  'context' => array(
	    'subject' => 'Some subject',
	    'message' => $data['title'] . '   ' . $data['body'] . '  ' . json_decode($data['body']),
	  )
  ));*/
   //$data['body'] = $buffer;


    $node->body[LANGUAGE_NONE][0]['value'] = $data['body'];

    $node->field_categories[LANGUAGE_NONE][0]['tid'] = $data['category_id'];
    if (isset($data['make_anonymous']) && $data['make_anonymous'] == 1) {
      $node->uid = 0;
      $node->field_anonymous[LANGUAGE_NONE][0]['value'] = $data['make_anonymous'];
    }
    if (isset($data['smeared_on']) && $data['smeared_on'] != '') {
      $node->field_smeared_on[LANGUAGE_NONE][0]['value'] = $data['smeared_on'];
    }

    $location = array(
      'name' => $data['place_name'],
      'street' => $data['street'],
      'city' => $data['city'],
      'province' => $data['province'],
      'postal_code' => $data['postal_code'],
      'country' => $data['country'],
      'latitude' => $data['latitude'],
      'longitude' => $data['longitude'],
      'body' => 'Post'
    );

    location_save($location);
    $node->field_address[LANGUAGE_NONE][0] = $location;

    $node->locations = array($location);
    $node->location = $location;

    if (!empty($_FILES['files']['name']['picture'])) {
      $picture = _creopleapi_file_save('picture', array('png gif jpg jpeg'));
      $node->field_upload_media = array(
        LANGUAGE_NONE => array(
          0 => get_object_vars($picture)
        )
      );
    }

    if (!empty($_FILES['files']['name']['video'])) {
      $video = _creopleapi_file_save('video', array('avi mp4 3gp mov wmv'));
      $node->field_upload_video = array(
        LANGUAGE_NONE => array(
          0 => get_object_vars($video)
        )
      );
    }

    if (!empty($_FILES['files']['name']['audio'])) {
      $audio = _creopleapi_file_save('audio', array('mp3, wav, m4a'));
      $node->field_upload_audio = array(
        LANGUAGE_NONE => array(
          0 => get_object_vars($audio)
        )
      );
    }

    $node = node_submit($node); // Prepare node for saving
    node_save($node);

    return _creopleapi_node_retrieve($node->nid);

  } catch (Exception $e) {
    return services_error(t('Post Failed'), 401);
  }
}

function _creopleapi_file_save($field, $extensions) {
  $file = file_save_upload($field, array(
    'file_validate_extensions' => $extensions,
  ));
  if ($file) {
    $file = file_move($file, 'public://');
    if (!$file) {
      return services_error(t('Failed to write the uploaded file the site\'s file folder.'), 401);
    }
  } else {
    return services_error(t('No file was uploaded.'), 401);
  }

  $file->status = FILE_STATUS_PERMANENT;
  $file->display = 1;
  file_save($file);

  return $file;
}

function _creopleapi_node_update($nid, $data) {
  global $user;
  
  try {
    $errors = array();

    $validate_fields = array(
      'type' => 'Type',
      'title' => 'Title',
      'category_id' => 'Category',
      /*'street' => 'Street',
      'city' => 'City',
      'province' => 'Province',
      'postal_code' => 'Postal Code',
      'country' => 'Country',*/
      'body' => 'Post'
    );

    /*foreach ($validate_fields as $key => $title) {
      if (!isset($data[$key]) || $data[$key] == '') {
        $errors[$key] = $title . ' is required field.';
      }
    }*/

    if (count($errors) > 0) {
      return services_error(t('Post Update Failed'), 401);
    }
/*********************************************************/
    //$data['body'] = json_encode($data['body']);
    //$data['title'] = json_encode($data['title']);
/***********************************************************/

    $node = node_load($nid);
    $node->title = $data['title'];
    $node->type = $data['type'];

    $node->language = LANGUAGE_NONE;
    $node->uid = $user->uid; 
    $node->status = 1;
    $node->promote = 1;
    $node->comment = 2;

    $node->body[LANGUAGE_NONE][0]['value'] = $data['body'];
    $node->field_categories[LANGUAGE_NONE][0]['tid'] = $data['category_id'];
    if (isset($data['make_anonymous']) && $data['make_anonymous'] == 1) {
      $node->uid = 0;
      $node->field_anonymous[LANGUAGE_NONE][0]['value'] = $data['make_anonymous'];
    }
    if (isset($data['smeared_on']) && $data['smeared_on'] != '') {
      $node->field_smeared_on[LANGUAGE_NONE][0]['value'] = $data['smeared_on'];
    }

    $location = $node->locations[0];
    $location['street'] = $data['street'];
    $location['city'] = $data['city'];
    $location['province'] = $data['province'];
    $location['postal_code'] = $data['postal_code'];
    $location['country'] = $data['country'];
    $location['latitude'] = $data['latitude'];
    $location['longitude'] = $data['longitude'];

    location_save($location, FALSE);
    $node->field_address[LANGUAGE_NONE][0] = $location;

    $node->locations = array($location);
    $node->location = $location;

    if (!empty($_FILES['files']['name']['picture'])) {
      $picture = _creopleapi_file_save('picture', array('png gif jpg jpeg'));
      $node->field_upload_media = array(
        LANGUAGE_NONE => array(
          0 => get_object_vars($picture)
        )
      );
    }

    if (!empty($_FILES['files']['name']['video'])) {
      $video = _creopleapi_file_save('video', array('avi mp4 3gp mov wmv'));
      $node->field_upload_video = array(
        LANGUAGE_NONE => array(
          0 => get_object_vars($video)
        )
      );
    }

    if (!empty($_FILES['files']['name']['audio'])) {
      $audio = _creopleapi_file_save('audio', array('mp3, wav, m4a'));
      $node->field_upload_audio = array(
        LANGUAGE_NONE => array(
          0 => get_object_vars($audio)
        )
      );
    }

    $node = node_submit($node); // Prepare node for saving
    node_save($node);

    return _creopleapi_node_retrieve($node->nid);

  } catch (Exception $e) {
    return services_error(t('Post Update Failed'), 401, array('error_type' => 'no-valid', 'errors' => $errors));
  }
}

function _creopleapi_node_search($query) {
  global $pager_total;

  $result = search_data($query, 'node');
  
  $nids = array();
  foreach ($result['#results'] as $node) {
    $nids[] = $node['node']->nid;
  }
  
  $nodes = array();
  foreach ($nids as $nid) {
    $nodes[] = _creopleapi_node_retrieve($nid);
  }

  return array(
    'total_pages' => $pager_total[0],
    'nodes' => $nodes
  );
}

function _creopleapi_node_delete($nid) {
  node_delete($nid);
  return TRUE;
}

function _creopleapi_node_comments($nid, $page, $pagesize) {
  $comment_select = db_select('comment', 't')
    ->orderBy('created', 'DESC');

  $parameters = array('nid' => $nid);
  services_resource_build_index_query($comment_select, $page, '*', $parameters, $pagesize, 'comment');

  if (!user_access('administer comments')) {
    $comment_select->condition('status', COMMENT_PUBLISHED);
  }

  $results = services_resource_execute_index_query($comment_select);

  $comments = services_resource_build_index_list($results, 'comment', 'cid');

  for ($i=0; $i<count($comments); $i++) {
    $comment_user = _creopleapi_user_retrieve($comments[$i]->uid);
    $comment_detail = _comment_resource_retrieve($comments[$i]->cid);
    $comments[$i]->comment_body = utf8_decode($comment_detail->comment_body[LANGUAGE_NONE][0]['value']);
    $comments[$i]->user_picture_url = $comment_user->field_profile_picture[LANGUAGE_NONE][0]['url'];
    $comments[$i]->user_fullname = $comment_user->field_name[LANGUAGE_NONE][0]['value'];
  }

  return $comments;
}

function _creopleapi_node_create_comment($nid, $data) {

  try {

    $errors = array();
    if (!isset($data['subject']) || $data['subject'] == '') {
      $errors['subject'] = 'Subject is required.';
    }
    if (count($errors) > 0) {
      return array('error_type' => 'validation', 'errors' => $errors);
    }

    $comment_form = array();
    $comment_form['nid'] = $nid;
    $comment_form['comment_body'][LANGUAGE_NONE][0]['value'] = ($data['subject']);
    if (isset($data['creople_user']) && $data['creople_user'] == 1) {
      $comment_form['field_creople_me'][LANGUAGE_NONE] = 1;
    }

    return _comment_resource_create($comment_form);

  } catch (Exception $e) {
    return services_error(t('Comment Create Failed'), 401);
  }
}


function _creopleapi_user_send_contact_form($data) {
  try {

    $errors = array();
    if (!isset($data['name']) || $data['name'] == '') {
      $errors['name'] = 'Name field is required.';
    }
    if (!isset($data['email']) || $data['email'] == '') {
      $errors['email'] = 'Email field is required.';
    }
    if (!isset($data['message']) || $data['message'] == '') {
      $errors['message'] = 'Message field is required.';
    }
    if (isset($data['message']) && strlen($data['message']) > 250) {
      $errors['message'] = 'Message field is too length.';
    }
    if (count($errors) > 0) {
      return array('error_type' => 'validation', 'errors' => $errors);
    }

    global $user, $language;
	$values = array(
	  "subject" => "Make Creople Better Form",
	  "cid" => 1,
	  "copy" => "",
	  "submit" => "Submit",
	  "form_id" => "contact_site_form",
          "message" => $data['message']	   
	);
    $values['sender'] = user_load($user->uid);
    $values['sender']->name = $data['name'];
    $values['sender']->mail = $data['mail'];
    $values['category'] = contact_load($values['cid']);

    if (!$user->uid) {
      user_cookie_save(array_intersect_key($values, array_flip(array('name', 'mail'))));
    }

    // Get the to and from e-mail addresses.
    $to = $values['category']['recipients'];
    $from = $values['sender']->mail;

    // Send the e-mail to the recipients using the site default language.
    drupal_mail('contact', 'page_mail', $to, language_default(), $values, $from);

    return drupal_mail('contact', 'page_mail', $to, language_default(), $values, $from);

  } catch (Exception $e) {
    return services_error(t('Contact Form Send Failed'), 401);
  }
}



function _creopleapi_node_flagged($page, $pagesize) {
  
  $offset = $page * $pagesize;

  $flagged = array();

  $result = db_query("SELECT node.nid AS nid, node.title AS node_title, flag_counts_node.count AS flag_counts_node_count, node.status AS node_status
    FROM 
    {node} node
    INNER JOIN {flag_counts} flag_counts_node ON node.nid = flag_counts_node.entity_id AND (flag_counts_node.fid = '3' AND flag_counts_node.count > '0')
    WHERE (( (node.status = '0') AND (node.type IN  ('posts')) ))
    ORDER BY flag_counts_node_count DESC
    LIMIT $pagesize OFFSET $offset");

  while ($node = $result->fetchObject()) {
    $flagged[] = $node;
  }

  return $flagged;
}

function _creopleapi_node_most_popular($page, $pagesize) {
  
  $offset = $page * $pagesize;

  $most_popular = array();

  $result = db_query("SELECT node_comment_statistics.comment_count AS node_comment_statistics_comment_count, node.sticky AS node_sticky, node.created AS node_created, node.nid AS nid FROM {node} node INNER JOIN {node_comment_statistics} node_comment_statistics ON node.nid = node_comment_statistics.nid WHERE node.status = 1 ORDER BY node_comment_statistics_comment_count DESC, node_sticky DESC, node_created DESC LIMIT $pagesize OFFSET $offset");

  while ($node = $result->fetchObject()) {
    $most_popular[] = _creopleapi_node_retrieve($node->nid);
  }

  return $most_popular;
}

function _creopleapi_node_favourite($nid, $status = 'flag') {
  global $user;

  if (!$user) {
    return false;
  }

  $flag = flag_get_flag('favourite');

  $result = $flag->flag($status, $nid);

  return $result;
}

function _creopleapi_category_list() {
  return taxonomy_get_tree(4);
}

function _creopleapi_category_node_list($cid, $page, $pagesize) {

  $nids = taxonomy_select_nodes($cid, FALSE, FALSE);
  if (is_array($nids)) {
    $nids = array_slice($nids, $page * $pagesize, ($page + 1) * $pagesize);
  }

  $nodes = array();
  foreach ($nids as $nid) {
    $nodes[] = _creopleapi_node_retrieve($nid);
  }

  return $nodes;
}

function _creopleapi_friend_list() {
  global $user;

  $friends = array();

  $result = db_query("SELECT users.uid AS uid, users.picture AS users_picture, users.name AS users_name, users.mail AS users_mail, user_relationships.approved AS user_relationships_approved, user_relationships.requestee_id AS user_relationships_requestee_id, user_relationships.requester_id AS user_relationships_requester_id, user_relationships.rid AS user_relationships_rid, users.created AS users_created
    FROM users
    LEFT JOIN user_relationships user_relationships ON users.uid = user_relationships.requestee_id
    LEFT JOIN users users_user_relationships ON user_relationships.requester_id = users_user_relationships.uid
    WHERE (( (users_user_relationships.uid = '" . $user->uid . "' ) )AND(( (users.status <> '0') AND (user_relationships.approved = '1') )))
    ORDER BY users_created DESC");

  while ($friend = $result->fetchObject()) {
    $friend_user = _creopleapi_user_retrieve($friend->uid);
    $friend_user->user_relationships_rid = $friend->user_relationships_rid;
    $friends[] = $friend_user;
  }

  return $friends;
}

function _creopleapi_friend_pendings() {
  global $user;

  $friends = array();

  $result = db_query("SELECT users.uid AS uid, users.created AS users_created
    FROM users
    LEFT JOIN user_relationships user_relationships ON users.uid = user_relationships.requestee_id
    LEFT JOIN users users_user_relationships ON user_relationships.requester_id = users_user_relationships.uid
    WHERE (( (users_user_relationships.uid = '" . $user->uid . "' )) AND (((users.status <> '0') AND (user_relationships.approved = '0') )))
    ORDER BY users_created DESC");

  while ($friend = $result->fetchObject()) {
    $friends[] = _creopleapi_user_retrieve($friend->uid);
  }

  return $friends;
}

function _creopleapi_friend_requests($uid = null) {
  global $user;
  $user = user_load($uid);
  $friends = array();

  $result = db_query("SELECT users_user_relationships.name AS users_user_relationships_name, users_user_relationships.uid AS users_user_relationships_uid, users_user_relationships.picture AS users_user_relationships_picture, users_user_relationships.mail AS users_user_relationships_mail, user_relationships.requestee_id AS user_relationships_requestee_id, user_relationships.requester_id AS user_relationships_requester_id, user_relationships.rid AS user_relationships_rid, user_relationships.approved AS user_relationships_approved, users.created AS users_created
    FROM users
    LEFT JOIN user_relationships user_relationships ON users.uid = user_relationships.requestee_id
    LEFT JOIN users users_user_relationships ON user_relationships.requester_id = users_user_relationships.uid
    WHERE (( (user_relationships.requestee_id = '" . $user->uid . "' ) )AND(( (users.status <> '0') AND (user_relationships.approved = '0') )))
    ORDER BY users_created DESC");
  
  while ($friend = $result->fetchObject()) {
    $friend_user = _creopleapi_user_retrieve($friend->users_user_relationships_uid);
    $friend_user->user_relationships_rid = $friend->user_relationships_rid;
    $friends[] = $friend_user;
  }

  return $friends;
}

function _creopleapi_friend_invite($data) {
  global $user;
  $uid = $user->uid;

  if(custom_creople_opt_out_exist($uid, $data['email'])) {
    return array('status' => 'fail', 'msg' => 'Cannot send to the following recipient');
  }

  $account = user_load_by_mail($data['email']);
  if ($account->uid) {
    $requester = user_load($uid);
    $requestee = user_load($account->uid);
    $relationship = _creopleapi_friend_invite_request($requester, $requestee);
    
    if ($relationship === FALSE) {
      return array('status' => 'fail', 'msg' => user_relationships_get_message('unknown_error', $relationship));
    } else {
      return array('status' => 'success');
    }
  } else {
    if (_creopleapi_friend_invite_by_email($data['email'])) {
      return array('status' => 'success');
    } else {
      return array('status' => 'fail');
    }
  }
}

function _creopleapi_friend_invite_request($requester, $requestee) {
  $relationships = user_relationships_get_requestable_rtypes($requester, $requestee);
  $default_relationship = NULL;
  $rtids = array_keys($relationships);
  $default_relationship = $rtids[0];
  $default_relationship = isset($relationships[$default_relationship]) ? $default_relationship : NULL;
  
  $relationship = new stdClass();
  $relationship->requester = $requester;
  $relationship->requestee = $requestee;
  $relationship->type = user_relationships_type_load($default_relationship);
  
  $relationship = user_relationships_request_relationship($relationship);

  return $relationship;
}

function _creopleapi_friend_invite_by_email($email) {

  $invite = entity_create('invite', array('type' => 'invite_by_email'));
  $mail_subject = variable_get('invite_default_mail_subject', t('[invite:inviter:name] has sent you an invite!'));
  $mail_body = variable_get('invite_default_mail_body', '');
  $invite->field_invitation_email_address[LANGUAGE_NONE][0]['value'] = $email;
  $invite->field_invitation_email_subject[LANGUAGE_NONE][0]['value'] = $mail_subject;
  $invite->field_invitation_email_body[LANGUAGE_NONE][0]['value'] = $mail_body;

  if (invite_save($invite)) {
    return true;
  } else {
    return false;
  }
}

function _creopleapi_friend_remove($data) {
  global $user;

  $relationship = user_relationships_load($data['rid']);

  if (!user_relationships_ui_check_access('delete', NULL, $relationship)) {
    return array('status' => 'fail', 'msg' => 'Access Denied');
  }

  user_relationships_delete_relationship($relationship, $user);
  return array('status' => 'success');
}

function _creopleapi_friend_approve($data) {
  $relationships = user_relationships_load(array('rid' => $data['rid']), array('include_user_info' => TRUE));
  $relationship = $relationships[$data['rid']];

  if (!user_relationships_ui_check_access('approve', NULL, $relationship)) {
    return array('status' => 'fail', 'msg' => 'Access Denied');
  }
  $relationship->approved = TRUE;
  user_relationships_save_relationship($relationship, 'approve');

  return array('status' => 'success');
}

function _creopleapi_friend_disapprove($data) {
  global $user;

  $relationships = user_relationships_load(array('rid' => $data['rid']), array('include_user_info' => TRUE));
  $relationship = $relationships[$data['rid']];

  if (!user_relationships_ui_check_access('approve', NULL, $relationship)) {
    return array('status' => 'fail', 'msg' => 'Access Denied');
  }
  
  user_relationships_delete_relationship($relationship, $user, 'disapprove');

  return array('status' => 'success');
}
