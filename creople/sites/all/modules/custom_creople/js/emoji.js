(function ($) {
  Drupal.behaviors.emoji = {
    attach : function(context, settings) {
      twemoji.size = '16x16';
      twemoji.parse(document.body);
    }
  };
})(jQuery);
