
(function ($) {

	
	Drupal.behaviors.myCustomJS = {
		attach: function(context, settings) {
			$('a.mapclass', context).once('mycustombutton', function () {
		      // Apply the MyBehaviour effect to the elements only once.

		    });
		
		    /*
		     * =======================================================
		     */
		    //alert("Welcome");
		    // do things only if current page is a search page 

			if(window.location.href.indexOf("search/node") > -1) {
		       // For frontpage, searchpage, userpage & taxonomypage.
		       	//alert("ONE");
		       	var parentdivid = $("a.mapclass").first().closest(".node.node-posts.node-promoted.node-teaser.clearfix").attr("id");
					
				var link = $("a.mapclass").first().attr("href");
				var id = parentdivid.substr(parentdivid.indexOf("-")+1, parentdivid.length);
				var current_nids = $.parseJSON($("span#all_current_nids").last().text());
				//current_nids.splice(current_nids.indexOf(id), 1);
				link = link+"/"+current_nids;

				link = link+"?popup=1";
				var ratio = 0.50;
			    var width = Math.round(screen.width * ratio );
			    var height = Math.round(width / (screen.width / screen.height));
				//alert("OKAy");
			    var left = (screen.width/2)-(width/2);
				var top = (screen.height/2)-(height/2);
				ourpopup = window.open(link, '_Blank', 'toolbar=no, location=no, directories=no, toolbar=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height+', top='+top+', left='+left);									
				if(!ourpopup){
					if($('#is_load_map').html() == "1") {
						$('#is_load_map').html("1");													
							//$.colorbox({html:$('#search-map-buffer').html()});
						}
					  $('#is_load_map').html("2");
						console.log(link);
						//$.colorbox({html:'vjfgff gfghgghhjgfgfg'});
						/*$('#search-map-buffer').load(link, function(){							
						});*/					
				}
			    
				/*if(!ourpopup){
					$html = "<div><span>'In your browser pop-up windows are forbidden.\n\nAllow pop-up windows?</span><a onclick=\"ourpopup = window.open(link, '_Blank', 'toolbar=no, location=no, directories=no, toolbar=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height+', top='+top+', left='+left);\">Allow</a><a onclick=\"\">Decline</a></div>"
					if(confirm('In your browser pop-up windows are forbidden.\n\nAllow pop-up windows? ")){
						ourpopup = window.open(link, '_Blank', 'toolbar=no, location=no, directories=no, toolbar=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height+', top='+top+', left='+left);
					}
				}*/
			    //window.open(link, '_Blank', 'toolbar=no, location=no, directories=no, toolbar=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height+', top='+top+', left='+left);
			    
			    //if (window.focus) {ourpopup.focus()}
			   	//alert("END");
		    }

			/*
			 * #1. DISPLAYING A POPUP.
			 */
			// Hiding span element which shows json string for currently loaded nodes.
			$("span#all_current_nids").css({"display":"none", });
			//Added for maps
			//$("div.item-list").css({"display":"block", });
			
			$("a.mapclass").click(function(e){
				//$(this).click(function(e){
					e.preventDefault();
					
					var parentdivid = $(this).closest(".node.node-posts.node-promoted.node-teaser.clearfix").attr("id");
					
					var link = $(this).attr("href");
					/*
					// only for search page 
					if(window.location.href.indexOf("search") > -1) {
						// For searchpage.
						var id = parentdivid.substr(parentdivid.indexOf("-")+1, parentdivid.length);
						var current_nids = $.parseJSON($("span#all_current_nids").last().text());
						current_nids.splice(current_nids.indexOf(id), 1);
						link = link+"/"+id+"/"+current_nids;
					}else{

						//console.log("this is id of parentdiv :: "+parentdivid);
						if(parentdivid == undefined){
							

						}
					}
					*/
					
					if(parentdivid == undefined){	
						// for internal node pages.
						var parentdivid = $(this).closest(".node.node-posts.node-promoted.node-full.node-full.clearfix").attr("id");
					}
					var id = parentdivid.substr(parentdivid.indexOf("-")+1, parentdivid.length);
					link = link+"/"+id;
					
					//alert(" NODE ID: " + id + " Found !!");
					//var current_nids = $.parseJSON($("span#all_current_nids").text());

					//current_nids.splice(current_nids.indexOf(id), 1);

					link = link+"?popup=1";
					//alert("you just clicked me. and I stopeed your navigation.. :) ");
					// to open in good size for user
					var ratio = 0.50;
				    var width = Math.round(screen.width * ratio );
				    var height = Math.round(width / (screen.width / screen.height));

				    var left = (screen.width/2)-(width/2);
					var top = (screen.height/2)-(height/2);					
				    ourpopup = window.open(link, '_Blank', 'toolbar=no, location=no, directories=no, toolbar=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height+', top='+top+', left='+left);
				    if (window.focus) {ourpopup.focus()}
				    return false;
				//});
			});
			/*
			 * Adding a handle to each popup title link so that it can be referenced.
			 */
			$("#popup_contentDiv .field-content a").addClass("popup_close_refresh_parent");
			// On Click event of popup title link, current popup should close and parent window should redirect. 
			$("#popup_contentDiv .field-content a").click(function(e){
				
				var link = $(this).attr("href");
				var querystring = getUrlVars();
				if(querystring['popup'] == 1){
					e.preventDefault();
					window.opener.location.href=link;
					window.opener.focus();
				    self.close();
				}
			});
			/*
			 * End
			 */	
			/*
			 * #2. MAKING MAP FULLSCREEN.
			 */
			var querystring = getUrlVars();
			if(querystring['popup'] == 1){
				//console.log("popup window found. :) chaning view state of map to full screen");
				// Making map fullscreen.
				$("#openlayers-map").addClass("openlayers_map_fullscreen");
				// disabling toggle fullscreen button.
				$(".openlayers_behavior_fullscreen_buttonItemInactive.olButton").hide();
			}
			//console.log(window.location.pathname +" :: "+window.location.href);

			// Read a page's GET URL variables and return them as an associative array.
			function getUrlVars(){
			    var vars = [], hash;
			    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
			    for(var i = 0; i < hashes.length; i++)
			    {
			        hash = hashes[i].split('=');
			        vars.push(hash[0]);
			        vars[hash[0]] = hash[1];
			    }
			    return vars;
			}

			// Issue #7. 
			var iconlink = window.location.href.substr(0, window.location.href.indexOf("/user")) + "/sites/all/themes/creople/images/calendar_icon1.jpg";
			$("#edit-field-date-of-birth-und-0-value-datepicker-popup-0").after("<img id='customjs_calendar_icon' src='"+iconlink+"' />");
			// Adding Style to thaticon...  
			$("#customjs_calendar_icon").css({ "height": "24px", "margin": "2px 0 0 -6px", "padding": "2px", "position": "absolute", "cursor": "pointer", }).click(function(){
				$(this).toggle(function(){
					$("#ui-datepicker-div").toggle().css({"display": "block", });
				}, function(){
					$("#ui-datepicker-div").toggle().css({"display": "none", });
				});
				$("#edit-field-date-of-birth-und-0-value-datepicker-popup-0").focus();
			});
			// # ISSUE NO. 8
			var home = window.location.href.substr(0, window.location.href.indexOf('user/register'));
			//$("#edit-field-terms-of-use-und").after('<a href="'+home+"terms-of-service"+'">Click Here</a>');
			// ISSUE NO. 9
			$("#user-register-form #edit-submit").after('<a id="custom_cancel_exit_button" href="'+home+'">Cancel / Exit</a>');
			
			// JUGAAD APPENDING FOR SIGNUP PAGE.
			$("#user-register-form .captcha.form-wrapper").appendTo(".required-fields.group-profile.field-group-div");
			$("#user-register-form #edit-field-terms-of-use ").appendTo(".required-fields.group-profile.field-group-div");
			$("#user-register-form #edit-actions").appendTo(".required-fields.group-profile.field-group-div");
			$("#user-register-form .form-item-persistent-login").appendTo("#user-register-form #edit-field-terms-of-use");
			
			//If anonymous hide these fields
			$("#edit-field-anonymous-und").click(function () {
				if ($(this).prop('checked') === true) {
					console.log('Anonymous clicked');
	
					$('.form-item-field-send-email-und').hide();
					$('.form-item-field-copy-to-friend-und').hide();
				} else {
				   $('.form-item-field-send-email-und').show();
					$('.form-item-field-copy-to-friend-und').show();
				}
			});
		}
	};

	$(document).ready(function(){
	  $('body span.menu-toggle').on('click', function(){
	    $('#burger-menu-list').slideToggle();
	  });

	$(document).on('click', function(event) { 
	    if(!$(event.target).closest('#burger-menu-list').length && !$(event.target).closest('body span.menu-toggle').length) {
		if($('#burger-menu-list').is(":visible")) {
		    $('#burger-menu-list').hide();
                    return false;
		}
	    }        
	});

jQuery(document).on('touchstart', function (e) {
var container = jQuery("#burger-menu-list");

    if (!container.is(e.target) 
    && container.has(e.target).length === 0) 
        {
		if(container.is(":visible")) {
		    container.hide();
                    return false;
		}
            
        }
});
       // $('.anonim-img').height($('.anonim-img img').height() - 81 + 'px');
	$( window ).resize(function() {
          // $('.anonim-img').height($('.anonim-img img').height() - 81 + 'px');
	   if($(window).width() > 991) {
             $('#burger-menu-list').hide()
           }
	});
	  $('.post-desc-actions .link-more').click(function(){
	    $(this).parent().parent().find('.full-desc').show();
	    $(this).parent().parent().find('.short-desc').hide();
	    $(this).parent().find('.link-hide').show();
	    $(this).hide();
	  });
	  $('.post-desc-actions .link-hide').click(function(){
	    $(this).parent().parent().find('.full-desc').hide();
	    $(this).parent().parent().find('.short-desc').show();
	    $(this).parent().find('.link-more').show();
	    $(this).hide();
	  });
	});
})(jQuery);


