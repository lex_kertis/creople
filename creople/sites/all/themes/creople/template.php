<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 function creople_theme(&$existing, $type, $theme, $path) {
   $hooks['user_login_block'] = array(
     'template' => 'templates/user-login-block',
     'render element' => 'form',
   );
   return $hooks;
 }
function creople_preprocess_user_login_block(&$vars) {
  $vars['name'] = render($vars['form']['name']);
  $vars['pass'] = render($vars['form']['pass']);
  $vars['submit'] = render($vars['form']['actions']['submit']);
  $vars['rendered'] = drupal_render_children($vars['form']);
}

/**
 * Override or insert variables into the node template.
 */
function creople_preprocess_node(&$variables) {

  $variables['content']['links']['comment']['#links']['comment-add']['title'] = t('Jump In');

  if ($variables['field_anonymous'][0]['value'] == 1 || $variables['field_anonymous']['und'][0]['value'] == 1) {
    $variables['user_picture'] = theme('user_picture');
    $variables['submitted'] = 'Submitted Anonymously On ' . $variables['date'];
  }

  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }

}


function creople_block_view_user_login_alter(&$data, $block) {
  global $user;
  if (!$user->uid && !(arg(0) == 'user' && (arg(1) == 'login'))) {
    $block->subject = t('User login');
    $block->content = drupal_get_form('user_login_block');
  }
}

function phptemplate_my_comments($uid,$howmany) {
  $returnstr = "<span class='field-label'>My Recent Comments:</span><br><ul>";
  $i = 0;
  $result = db_query('SELECT n.nid, n.title, c.cid, c.subject FROM node n INNER JOIN comments c ON n.nid = c.nid WHERE c.uid = %d AND c.status = 0 ORDER BY c.timestamp DESC LIMIT 0,%d', $uid, $howmany);
  while ($r = db_fetch_object($result)) {
    $i++;
    $returnstr .= '<li>' . $i . ". <a href='/node/" . $r->nid . "#comment-" . $r->cid . "'>" . $r->subject . '</a>... <small><i>(on ' . $r->title . ')</i></small></li>';
  }
  $returnstr .= '</ul>';
  return $returnstr;
}

/**
 * Implements hook_menu_alter().
 */
function creople_menu_alter(&$items) {
  $items['cart/my']['title callback'] = 'creople_menu_item_title';
}
/**
 * Returns the title of the shopping cart menu item with an item count.
 */
function creople_menu_item_title() {
  global $user;
  // Default to a static title.
  $title = t('Cart Items (0)');
  // If the user actually has a cart order...
  if ($order = commerce_cart_order_load($user->uid)) {
    // Count the number of product line items on the order.
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $quantity = commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types());
    // If there are more than 0 product line items on the order...
    if ($quantity > 0) {
      // Use the dynamic menu item title.
      $title = format_plural($quantity, 'Cart Items (1)', 'Cart Items (@count)');
    }
  }
  return $title;
}


function creople_preprocess_search_results(&$variables){
  #drupal_set_message("MSG FROM PREPROCESS SEARCH RESULT");
  #dsm($variables);
  $all_nids = array();
  foreach ($variables['results'] as $result) {
    $all_nids[] = $result['node']->nid;
  }
  $variables['all_nids'] = $all_nids;
  if(count($all_nids)>0){
  $variables['search_results'] .= "<span id='all_current_nids'>".json_encode($all_nids)."</span>";
  }  
}
function creople_html_head_alter(&$head_elements) {
  // Hide Drupal generator meta tag.
  // Use this if you want to hide the Drupal 7 Generator meta tag.
  $generator = 'Leave blank or add random generator name';
  $head_elements['system_meta_generator']['#attributes']['content'] = $generator;
  if (isset($head_elements['system_meta_generator']['#attached']['drupal_add_http_header'][0][1])) {
    $head_elements['system_meta_generator']['#attached']['drupal_add_http_header'][0][1] = $generator;
  }	
}


function creople_node_preview($variables) {
  global $base_url;
  $node = $variables['node'];
  $elements = node_view($node, 'teaser');
  $full = drupal_render($elements);
  $output = '<div class="preview">';
  $output .= '<h3 class="post-preview" >' . t('Preview of your posting') . '</h3>';
  if($node->field_anonymous[LANGUAGE_NONE][0]['value']!=1){
    $output .= $full;
  }else {
    $output .= '<div id="node-" class="node node-posts node-promoted node-teaser node-preview clearfix " >';
    $output .= '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 no-padding">';
    $output .= '<div class="meta submitted clearfix">';
    $output .= '<div class="col-xs-6 col-sm-3 col-md-3 col-lg-12"><a href=""><img src="'.$base_url.'/sites/default/files/default_avatar.png" /></a></div>';
    $output .=  '<div class="col-xs-6 col-sm-9 col-md-12 col-lg-12 user-name"><span>Anonymous</span></div>';
    $output .= '</div></div>';
    $output .= '<div class=" col-xs-12 col-sm-12 col-md-9 col-lg-9 no-padding">';
    $output .= drupal_render($elements['field_categories']);
    $output .= '<div class="content clearfix post-content">';
    $output .= '<h2><a>'.$node->title.'</a></h2>';
    if(isset($elements['body'])) {
      $elements['body'][0]['#markup']=wordfilter_filter_process($elements['body'][0]['#markup']);
      $output .= drupal_render($elements['body']);
    }
    $output .= drupal_render($elements['field_address']);
    $output .= '<div class="bottom-box clearfix">';
    $output .= '<div class="date">'.date('D, m/d/Y - h:ia',$node->created).'</div>';
    $mPath = url('node/' . $node->nid, array('absolute' => TRUE));
	$im_uri = isset($content['field_upload_media']['#items'][0]['uri'])?$content['field_upload_media']['#items'][0]['uri']:null;
    $im_url = ($im_uri)?file_create_url($im_uri):'';
    unset($elements['links']['comment']['#links']['comment-add']);
    $output .= drupal_render($elements['links']);
    $output .= theme('sharethis', array('data_options' =>sharethis_get_options_array(), 'm_path' => $mPath,'st_image' => $im_url, 'm_title' => $node->title));
    $output .= drupal_render($elements['field_voting_field']);
    $output .= drupal_render($elements['field_upload_video']);
    $output .= drupal_render($elements['field_upload_media']);
    $output .= drupal_render($elements['field_upload_audio']);
      $output .= '</div></div></div></div>';
  }
  $output .= "</div>\n";
  return $output;
}
