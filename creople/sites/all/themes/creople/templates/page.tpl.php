<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *#block-system-main
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<?php 
/*
  global $user; 
  $path = drupal_get_path_alias($_GET["q"]);
  global $_COOKIE;
  if($user->uid == 0 && $path != 'tos-anonimus'&& $path != 'front' && $path != 'user/register' && $path != 'about-us' && $path != 'terms-of-service' && $path != 'faq' && $path != 'contact' ) {    
    if(!isset($_COOKIE['accept_tos'])) {
      drupal_goto('front');
    }
  }
  /* $uu = user_load(285);
    var_dump($uu);
    var_dump(user_save($uu));
    exit;
    */
?>
<!--
<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script><script type="text/javascript">// <![CDATA[
//FB.init({ appId : '894051824018610', });
FB.init({ 
    a2ppId : '894051824018610',
    status     : true,
    xfbml      : true,
    version    : 'v2.3' });
// ]]></script>-->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '894051824018610',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    jQuery(document).ready(function(){
        jQuery('.share_button5').click(function(e){
            console.log(jQuery(this).attr('m_image'));
            e.preventDefault();
            FB.ui( {
                app_id:'894051824018610',
                method: 'feed',
                name: jQuery(this).attr('m_title'),
                link: jQuery(this).attr('m_path'),
                picture: jQuery(this).attr('m_image'),
                //caption: 'Мне нравится этот блог!',
                description: jQuery(this).attr('m_desc'),
                message: ''
            });
        });
    });
</script>
<?php global $base_url; ?>

<?php  if(isset($_GET['popup']) && ($_GET['popup'] == 1)){ 
  //dsm($page);
  //var_dump($page['content']);
  print render($page['content']['system_main']); 
}else{  ?>

<base href="<?php  print $base_url ?>" />

<div id="page-wrapper"><div id="page">

  <div id="header" class="<?php print $secondary_menu ? 'with-secondary-menu': 'without-secondary-menu'; ?>"><div class="section clearfix">
    <?php print render($page['header']); ?>
    <div class="container">
	
      <div class="hidden-lg hidden-md col-xs-12 col-sm-12" style="height:0;">
        <span class="fa fa-bars menu-toggle pull-right"></span>
      </div>
	<ul id="burger-menu-list" style="display:none;">
		<li><a href="<?php print $base_url;?>/about-us">What is Creople</a></li>
		<li><a href="<?php print $base_url;?>/terms-of-service">Terms of Service </a></li>
		<li><a href="<?php print $base_url;?>/privacy-policy">Privacy Policy </a></li>
		<li><a href="<?php print $base_url;?>/faq">FAQ</a></li>
		<li><a href="mailto:contact@creople.com">Contact Us</a></li>
		<li><a href="<?php print $base_url;?>/contact">Make Creople Better</a></li>
		<li><a href="http://creoples.com">Shop Creople</a></li>
                <li>
                  <div class="burger-social"> 
                  <a class="facebook-burger" href="https://www.facebook.com/creoplecom-156967994349887/?ref=hl" target="_blank"></a>
                  <a class="twitter-burger" href="https://twitter.com/creoples" target="_blank"></a>
                  <a class="youtube-burger" href="https://www.youtube.com/channel/UCdD7bqTBb9xjyBLG_eVLlFw?guided_help_flow=3" target="_blank"></a>
                  </div>
                </li>
	</ul>
	  
	  <!--
	  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
      </button>
	  -->
      <div class="row">
        <div class="col-md-6 col-md-offset-2 col-xs-6 col-xs-offset-3 image-logo" >
          <?php if ($logo): ?>
            <a href="/front <?php //print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img class="img-relative" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>
        </div>
        <div class="col-md-4 col-xs-8 col-md-offset-0 col-xs-offset-2">
        <?php if ($site_name || $site_slogan): ?>
            <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } ?>>

              <?php if ($site_name): ?>
                <?php if ($title): ?>
                  <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
                    <strong>
                      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                    </strong>
                  </div>
                <?php else: /* Use h1 when the content title is empty */ ?>
                  <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                  </h1>
                <?php endif; ?>
              <?php endif; ?>

              <?php if ($site_slogan): ?>
                <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
                  <?php print $site_slogan; ?>
                </div>
              <?php endif; ?>

            </div> <!-- /#name-and-slogan -->
          <?php endif; ?>
        </div>
      </div>
    	<div class="row header-categories">
        <div id="block-menu-menu-creople-top-menu" class="collapse-1 navbar-collapse-1" >
        <?php
          $block = module_invoke('menu', 'block_view', 'menu-creople-top-menu');
          print render($block['content']);
        ?>
        </div>
    	</div>
    </div>
	
	
    <?php if ($secondary_menu): ?>
      <div id="secondary-menu" class="navigation">
        <?php print theme('links__system_secondary_menu', array(
          'links' => $secondary_menu,
          'attributes' => array(
            'id' => 'secondary-menu-links',
            'class' => array('links', 'inline', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#secondary-menu -->
    <?php endif; ?>

  </div></div> <!-- /.section, /#header -->

 

  <?php if ($page['featured']): ?>
    <div id="featured"><div class="section clearfix">
      <?php print render($page['featured']); ?>
    </div></div> <!-- /.section, /#featured -->
  <?php endif; ?>

  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix row">
   
    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
     <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>
    <?php if ($page['sidebar_first']): ?>
      <div id="" class="column sidebar col-xs-12 col-sm-12 col-md-3 col-lg-3 no-padding"><div class="section">
        <?php print render($page['sidebar_first']); ?>
      </div></div> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>

    <div id="middle-content" class="column col-xs-12 col-sm-12 col-md-9 col-lg-9"><div class="section ">
      <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
      <a id="main-content"></a>
      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
          <div class="clearfix"></div>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>

    </div></div> <!-- /.section, /#content -->

    <?php //if ($page['sidebar_second']): ?>
    <?php if (false): ?>
      <div id="" class="column sidebar col-xs-12 col-sm-12 col-md-2 col-lg-2 hidden-sm"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->

  <?php if ($page['triptych_first'] || $page['triptych_middle'] || $page['triptych_last']): ?>
    <div id="triptych-wrapper"><div id="triptych" class="clearfix">
      <?php print render($page['triptych_first']); ?>
      <?php print render($page['triptych_middle']); ?>
      <?php print render($page['triptych_last']); ?>
    </div></div> <!-- /#triptych, /#triptych-wrapper -->
  <?php endif; ?>

  <div id="footer-wrapper"><div class="section">

    <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
      <div id="footer-columns" class="clearfix">
        <?php print render($page['footer_firstcolumn']); ?>
        <?php print render($page['footer_secondcolumn']); ?>
        <?php print render($page['footer_thirdcolumn']); ?>
        <?php print render($page['footer_fourthcolumn']); ?>
      </div> <!-- /#footer-columns -->
    <?php endif; ?>

    <?php if ($page['footer']): ?>
      <div id="footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>

  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->


<?php  } ?>
