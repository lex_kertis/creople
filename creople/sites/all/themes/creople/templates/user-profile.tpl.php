<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>

<div class="profile"<?php print $attributes; ?>>
<?php
  global $user;
  global $base_url;
  echo "<h3>".$user_profile['field_name']['#object']->name."</h3>";
  if(arg(1) == $user->uid){
    drupal_goto('current-posts');
  }
  //Added the update user profile picture image to user page
  //If picture available show the picture else show the default image
  if($user_profile['user_picture']['#markup'])
    echo "<img src ='".$user_profile['user_picture']['#markup']."' height ='150px' width ='150px'/>";
  else
    echo "<img src ='".$base_url."/sites/default/files/default_avatar.png'/>";

  //User id and User name retrieved
  $userid = $user_profile['field_name']['#object']->uid;
  $username = $user_profile['field_name']['#object']->name;

  hide($user_profile['user_picture']);
  hide($user_profile['sms']);

  //Updating the language for Relationship actions
  $user_profile['user_relationships_ui']['#title'] = '';
  $user_profile['user_relationships_ui']['actions']['#title'] = '';

  //Changing the Create a Relationship message to Invite a Friend
  //$user_profile['user_relationships_ui']['actions']['#markup'] = '<div class="item-list"/><ul/><li class="first last"/><a href= "'.$base_url.'/relationship/'.$userid.'/request?destination=user/'.$userid.'">Send a friend invite to <em class="placeholder">'.$username.'</em></a>';
  print render($user_profile);
  echo "<h3>Likes</h3>";
  //var_dump($user_profile);
  if(!empty($user_profile['field_likes']['#items'])){
    foreach($user_profile['field_likes']['#items'] as $value){
      print $value['value'];
    }
  }

echo "<h3>Dis-likes</h3>";
if(!empty($user_profile['field_dislikes']['#items'])){
  foreach($user_profile['field_dislikes']['#items'] as $value){
    echo $value['value']."<br />\n";
  }
}
?>
</div>
