<?php global $base_url; ?><div id="user-login-block-container">
  <div id="user-login-block-form-fields">
    <?php print $name; // Display username field ?>
    <?php print $pass; // Display Password field ?>
  </div>
  <div class="clearfix">
  <div class="links">  
   <?php print $rendered; // Display hidden elements (required for successful login) ?>
   <a id="user-remind-password" href="<?php print $base_url;?>/user/password">Forgot Password?</a>
  </div>
  <div class="submit"><?php print $submit; // Display submit button ?></div>
</div>
</div>