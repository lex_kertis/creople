﻿<?php



/**

 * @file

 * Bartik's theme implementation to display a node.

 *

 * Available variables:

 * - $title: the (sanitized) title of the node.

 * - $content: An array of node items. Use render($content) to print them all,

 *   or print a subset such as render($content['field_example']). Use

 *   hide($content['field_example']) to temporarily suppress the printing of a

 *   given element.

 * - $user_picture: The node author's picture from user-picture.tpl.php.

 * - $date: Formatted creation date. Preprocess functions can reformat it by

 *   calling format_date() with the desired parameters on the $created variable.

 * - $name: Themed username of node author output from theme_username().

 * - $node_url: Direct URL of the current node.

 * - $display_submitted: Whether submission information should be displayed.

 * - $submitted: Submission information created from $name and $date during

 *   template_preprocess_node().

 * - $classes: String of classes that can be used to style contextually through

 *   CSS. It can be manipulated through the variable $classes_array from

 *   preprocess functions. The default values can be one or more of the

 *   following:

 *   - node: The current template type; for example, "theming hook".

 *   - node-[type]: The current node type. For example, if the node is a

 *     "Blog entry" it would result in "node-blog". Note that the machine

 *     name will often be in a short form of the human readable label.

 *   - node-teaser: Nodes in teaser form.

 *   - node-preview: Nodes in preview mode.

 *   The following are controlled through the node publishing options.

 *   - node-promoted: Nodes promoted to the front page.

 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser

 *     listings.

 *   - node-unpublished: Unpublished nodes visible only to administrators.

 * - $title_prefix (array): An array containing additional output populated by

 *   modules, intended to be displayed in front of the main title tag that

 *   appears in the template.

 * - $title_suffix (array): An array containing additional output populated by

 *   modules, intended to be displayed after the main title tag that appears in

 *   the template.

 *

 * Other variables:

 * - $node: Full node object. Contains data that may not be safe.

 * - $type: Node type; for example, story, page, blog, etc.

 * - $comment_count: Number of comments attached to the node.

 * - $uid: User ID of the node author.

 * - $created: Time the node was published formatted in Unix timestamp.

 * - $classes_array: Array of html class attribute values. It is flattened

 *   into a string within the variable $classes.

 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in

 *   teaser listings.

 * - $id: Position of the node. Increments each time it's output.

 *

 * Node status variables:

 * - $view_mode: View mode; for example, "full", "teaser".

 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').

 * - $page: Flag for the full page state.

 * - $promote: Flag for front page promotion state.

 * - $sticky: Flags for sticky post setting.

 * - $status: Flag for published status.

 * - $comment: State of comment settings for the node.

 * - $readmore: Flags true if the teaser content of the node cannot hold the

 *   main body content.

 * - $is_front: Flags true when presented in the front page.

 * - $logged_in: Flags true when the current user is a logged-in member.

 * - $is_admin: Flags true when the current user is an administrator.

 *

 * Field variables: for each field instance attached to the node a corresponding

 * variable is defined; for example, $node->body becomes $body. When needing to

 * access a field's raw values, developers/themers are strongly encouraged to

 * use these variables. Otherwise they will have to explicitly specify the

 * desired field language; for example, $node->body['en'], thus overriding any

 * language negotiation rule that was previously applied.

 *

 * @see template_preprocess()

 * @see template_preprocess_node()

 * @see template_process()

 */
global $user;
global $base_url;
$n=node_load($node->nid);
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix  row"<?php print $attributes; ?>>

<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 no-padding">
  <?php print render($title_suffix); ?>



  <?php if ($display_submitted): ?>

    <div class="meta submitted clearfix">
	
<?php
if($n->field_anonymous[LANGUAGE_NONE][0]['value']!=1){

print '<div class="author-picture">'.$user_picture.'</div>'; 
$u = user_load($uid);
print '<div class="user-name author-name">'.$u->name.'</div>'; 
}
else {
print '<div class="author-picture"><a href=""><img src="'.$base_url.'/sites/default/files/default_avatar.png" /></a></div>';
print '<div class="user-name author-name"><span>Anonymous</span></div>';
}
?>
    </div>

  <?php endif; ?>
</div>


  <div class=" col-xs-12 col-sm-12 col-md-9 col-lg-9 no-padding"<?php print $content_attributes; ?>>
  <?php print render($content['field_categories']); ?>
  <div class="content clearfix post-content">
  <?php print render($title_prefix); ?>

  <?php if (!$page): ?>

    <h2<?php print $title_attributes; ?>>

      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>

    </h2>

  <?php endif; ?>
    <?php

      // We hide the comments and links now so that we can render them later.

      hide($content['comments']);
      hide($content['sharethis']);
      hide($content['links']);
	  hide($content['field_voting_field']);
	  hide($content['field_upload_video']);
	  hide($content['field_upload_media']);
	  hide($content['field_upload_audio']);
	  
	 

	  $content['body'][0]['#markup']=wordfilter_filter_process($content['body'][0]['#markup']);
	   
	  print render($content);
	  if ($teaser || !empty($content['comments']['comment_form'])) {

      unset($content['links']['comment']['#links']['comment-add']);

    }
 
    ?>
<div class="bottom-box clearfix">
	<div class="date">
<?php print $date;?>
	</div>
  

<?php 
$links = render($content['links']);
//.print_r($content['links']);
if(arg(0)!="node"){
$content['links']['#links']['node-readmore']['href']="node/".$node->nid;
$content['links']['#links']['node-readmore']['query'] = array('jumpin'=>1);
}
print render($content['links']);
   $mPath = url('node/' . $node->nid, array('absolute' => TRUE));
$mTitle = $node->title;
 $data_options = sharethis_get_options_array();
 print theme('sharethis', array('data_options' => $data_options, 'm_path' => $mPath, 'm_title' => $mTitle));
 if($user->uid!=0 && $user->uid)
 print render($content['field_voting_field']);
 ?>
<div class="uploaded_media">
<? 
 print render($content['field_upload_video']);
 print render($content['field_upload_media']);
 print render($content['field_upload_audio']);
 ?>
  </div>
  </div>
</div>
</div>
  <?php

    // Remove the "Add new comment" link on the teaser page or if the comment

    // form is being displayed on the same page.

    

    // Only display the wrapper div if there are links.
  ?>
  
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
<?php

  if($_GET['jumpin']==1){
    print render($content['comments']['comment_form']); 
 }
 
 ?>
  <?php 
    if( !is_null($content['comments']['comments'])){
      print render(array_reverse($content['comments']['comments'])); 
    }
      ?>
</div>

</div>

