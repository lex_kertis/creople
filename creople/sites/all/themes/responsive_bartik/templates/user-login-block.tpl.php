<button class="mobile-login-button"  onclick="(jQuery)(this).parent().find('#user-login-block-container').slideToggle();return false;">Log in</button>
<div id="user-login-block-container" style="display:none;">
  <div id="user-login-block-form-fields">
    <?php print $name; // Display username field ?>
    <?php print $pass; // Display Password field ?>
  </div>
  <div>
  <div class="links">
   <?php print $rendered; // Display hidden elements (required for successful login) ?>
   
  </div>
  <div class="submit"><?php print $submit; // Display submit button ?></div>
  <div class="threebuttons">
  <ul>
  <li><a href="/user/password">Forgot your Password?</a></li>
  <li><a href="/user/password">Forgot your username?</a></li>
  <li><a href="/user/register">Create an account</a></li>
   </div>
</div>
</div>