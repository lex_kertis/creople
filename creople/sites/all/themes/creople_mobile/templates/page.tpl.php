<?php $path = drupal_get_path_alias($_GET["q"]);?>
<?php $img_name = 'default_logo';?>
<?php //mvar_dump($path);exit;?>

<?php if($path == 'user/password' || $path == 'user/register' || $path == 'user'):?>
  <script src="/<?=path_to_theme()?>/js/user-register.js"></script>
  <header class="main-header">
    <?php //print render($page['header']); ?>
    <section class="header-edit-block clearfix">
      <a class="edit-cancel"  href="/">Cancel</a><div class="page-title"><?=($path == 'user/password')?'password reset':'User Sign Up'?></div>
    </section>
  </header>
  <section class="main-content" role="content">
    <article class="text-page-block">
      <?php print render($page['content']['system_main']); ?>
      <?php print render($page['help']); ?>
      <?php print render($page['content']); ?>

    </article>
  </section>
<?php elseif($path == 'contact'):?>
  <header class="main-header">
    <section class="header-edit-block clearfix">
      <a class="edit-cancel" href="javascript:history.back()">Cancel</a><div class="page-title">Comment</div><a class="edit-ok">Save</a>
    </section>
  </header>

  <section class="main-content" role="content">
    <article class="comment-form-top">
      <h2>Help us make Creople better</h2>
      <div>What would like to see on the app/site? Any new category or topic?</div>
      <div>How can we make Creople better. We love to hear from you.</div>
    </article>


    <article class="edit-item clearfix">
      <label for="edit-name">Name</label>
      <input placeholder="Your name..." type="text" id="edit-name" name="name" value="" size="60" maxlength="255" class="form-text required">
    </article>
    <article class="edit-item clearfix">
      <label for="edit-mail">E-mail</label>
      <input placeholder="Your E-mail..." type="text" id="edit-mail" name="mail" value="" size="60" maxlength="255" class="form-text required">
    </article>
    <article class="edit-item clearfix">
      <label for="edit-message">Comments (max 250 charecters)</label>
      <textarea placeholder="Your comments..." id="edit-message" name="message" cols="60" rows="5" class="form-textarea required"></textarea>
    </article>


  </section>
  <footer class="main-footer">
  </footer>


<?php elseif(arg(0) == 'taxonomy' || $path == 'my-favourites' || $path == 'most-popular' || $path == 'current-posts'):?>


  <header class="main-header">
    <div class="header-top">
      <?php
      global $user;
      $u = user_load($user->uid);
      $uri = isset($u->field_profile_picture['und'][0]['uri'])?$u->field_profile_picture['und'][0]['uri']:null;
      ?>
      <?php if($user->uid > 0):?>
        <a class="edit-profile" href="/user/<?=$user->uid?>/edit"><img class="user-pic" src="<?=($uri)?image_style_url('current_user_pix', $uri):'/sites/default/files/styles/current_user_pix/public/pictures/sampledp.png' ?>"></a>
      <?php else:?>
        <a class="edit-profile colorbox-inline" href="?width=280&height=100&inline=true#popup-div-user-edit"><img class="user-pic" src="<?=($uri)?image_style_url('current_user_pix', $uri):'/sites/default/files/styles/current_user_pix/public/pictures/sampledp.png' ?>"></a>
      <?php endif;?>
      <div style="display: none"><div class="popup-div" id="popup-div-user-edit">Only registered user can edit profile</div></div>

      <?php
        $elements = drupal_get_form("search_block_form");
        $form = drupal_render($elements);
      ?>

      <?php if($path == 'my-favourites') {
        $block = module_invoke('views', 'block_view', 'taxonomy_block-block_1');
        $block_id = "type-1";
        $img_name = 'my_favorites';
      } else {
        if($path == 'most-popular') {
          $block = module_invoke('views', 'block_view', 'taxonomy_block-block_3');
          $block_id = "type-1";
          $img_name = 'most_popular';
        } else {
          if(arg(0) == 'taxonomy') {
            $block = module_invoke('views', 'block_view', 'taxonomy_block-block');
            $block_id = "type-1";
            $img_name = 'rottern_customers';
            switch(arg(2)) {
                case 3:
                  $img_name = 'rottern_customers';
                break;
              case 4:
                $img_name = 'bullies_unmasked';
                break;
              case 5:
                $img_name = 'bosses';
                break;
              case 6:
                $img_name = 'teacher';
                break;
              case 7:
                $img_name = 'random_wtf';
                break;

            }
          } else {
            if($path == 'current-posts') {
              $img_name = 'current_posts';
              $block_id = 'current-posts';
              $block = $page;
            }
          }
        }

      }?>
      <div id="search-form"><?php echo $form;?></div>
      <?php if($user->uid > 0) {?><a href="/node/add/posts" class="add-post"></a><?php } else{?>
        <a href="?width=280&height=100&inline=true#popup-div" class="add-post colorbox-inline"></a>
      <?php }?>
      <div style="display: none"><div class="popup-div" id="popup-div">Only registered user can make a post</div></div>
    </div>
    <div class="header-logo">
      <div class="underline"></div>
      <a class="go-home" href="javascript:history.back()">Home</a>
      <div class="logo-image"></div>
      <div class="circle-logo"><img src="/<?=path_to_theme()?>/css/images/menu_<?=$img_name?>.png"></div>
      <?php ///var_dump($img_name);?>
    </div>
  </header>

  <section class="main-content" role="content">
    <?php if($path == "my-favourites" || $path == 'current-posts'):?>
        <style>
          #current-posts, #type-1 {
            margin-top: 20px;
          }
          .views-row-1 .content-item {
            border:none;
          }
        </style>
      <a class="content-item fixed" href="#" onclick="return:false;">
        <div class="more-articles"><?=($path != 'current-posts')?'My Favorites':'My Posts'?></div>
      </a>
    <?php endif;?>
    <div id="<?=$block_id?>"><?php echo render($block['content']); ?></div>
  </section>
  <script>
    function spu_createCookie(name, value, hours)
    {
      if (hours)
      {
        var date = new Date();
        date.setTime(date.getTime()+(hours*60*60*1000));
        var expires = "; expires="+date.toGMTString();
      }
      else
      {
        var expires = "";
      }

      document.cookie = name+"="+value+expires+"; path=/";
    }
  </script>
  <footer class="main-footer">
    <section class="main-footer-inner clearfix">
      <article class="footer-user-navigation">
        <button type="button" class="user-navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <nav id="user-navigtion" class="user-navigation" role="navigation" style="display:none;">
          <a class="menu-item" href="javascript:history.back()">home</a>
          <?php if($user->uid > 0):?>
            <a class="menu-item" href="/current-posts">my posts</a>
            <a class="menu-item" href="/my-favourites">my favorites</a>
          <?php endif;?>
          <a class="menu-item" href="/most-popular">most popular</a>
          <a class="menu-item" href="/taxonomy/term/rotten-customers">rotten customers</a>
          <a class="menu-item" href="/taxonomy/term/bullies-unmasked">bullies unmasked</a>
          <a class="menu-item" href="/taxonomy/term/bosses-co-workers">bosses/co-worcers</a>
          <a class="menu-item" href="/taxonomy/term/random-wtf">random/wtf?</a>
          <?php if($user->uid > 0):?>
            <a class="menu-item" href="/user/logout">logout</a>
          <?php else:?>
            <a class="menu-item" href="#" onclick="spu_createCookie('was_accept_tos', 0, -1); location.reload(); return false;" >logout</a>
          <?php endif;?>

        </nav>
      </article>

      <article class="footer-friends">
          <?php if($user->uid > 0):?>
             <a href="/user/<?=$user->uid?>/friends/flaggedphone" class="friends"><img src="/<?=path_to_theme()?>/css/images/icon_friends.png"></a>
             <div class="friends-news-counter">20</div>
          <?php else:?>
            <a href="?width=280&height=100&inline=true#popup-div-see-friends" class="friends colorbox-inline"><img src="/<?=path_to_theme()?>/css/images/icon_friends.png"></a>
          <?php endif?>
      </article>
      <div style="display: none"><div class="popup-div" id="popup-div-see-friends">Only registered user can invite friends</div></div>

      <?php if($user->uid > 0):?>
        <article class="footer-creopied-by">
          <a href="/node/712" class="creopied-by"><img src="/<?=path_to_theme()?>/css/images/icon_creopled_by.png"></a>
        </article>
        <?php else:?>
        <article class="footer-creopied-by">
          <a href="#" onclick="return false;" class="creopied-by"><img src="/<?=path_to_theme()?>/css/images/icon_creopled_by.png"></a>
        </article>
      <?php endif;?>

      <article class="footer-shopping-cart">
        <a class="shopping-cart" target="_blank" href="http://creoples.com"><img src="/<?=path_to_theme()?>/css/images/shopping_cart.png"></a>
      </article>
      <article class="footer-navigation">
        <button type="button" class="navbar-toggle">
          <img src="/<?=path_to_theme()?>/css/images/info-menu.png">
        </button>
        <nav id="main-navigtion" role="navigation" class='footer-main-navigation' style="display:none;">
          <a class="menu-item" href="<?php print $base_url;?>/about-us">What is Creople</a>
          <a class="menu-item" href="<?php print $base_url;?>/terms-of-service">Terms of Service</a>
          <a class="menu-item" href="<?php print $base_url;?>/privacy-policy">Privacy Policy</a>
          <a class="menu-item" href="<?php print $base_url;?>/faq">FAQ</a>
          <a class="menu-item" href="mailto:contact@creople.com">Contact Us</a>
          <a class="menu-item" href="<?php print $base_url;?>/contact">Make Creople Better</a>
          <!--<a class="menu-item" href="http://creoples.com" target="_blank">Shop Creople</a>-->
        </nav>
      </article>
      <div class="clearfix"></div>
  </footer>


<?php else:?>

  <?php if($path == 'node/add/posts'):?>
    <script src="/<?=path_to_theme()?>/js/post-edit.js"></script>

    <header class="main-header">
      <section class="header-edit-block clearfix">
        <a class="edit-cancel" href="javascript:history.back()">Cancel</a><div class="page-title">Make a post</div><a class="edit-ok"></a>
      </section>
    </header>

    <section id="edit-post-form" class="main-content" role="content">
      <article class="edit-item clearfix post-make-anonimus">
        <div>
          <input type="checkbox" id="edit-field-anonymous-und" name="field_anonymous[und]" value="1" class="form-checkbox">
          <label class="option" for="edit-field-anonymous-und">Anonymous <span></span></label>
        </div>
      </article>
      <article class="edit-item clearfix post-add-category">
        <label for="edit-field-categories-und">Category</label>
        <select id="edit-field-categories-und" name="field_categories[und]" class="form-select required">
          <option value="_none">Select a category</option>
          <option value="3">Rotten Customers</option>
          <option value="4">Bullies Unmasked</option>
          <option value="5">Bosses / Co-Workers</option>
          <option value="6">Lousy Teachers / Profs</option>
          <option value="7">Random / WTF?</option>
        </select>
      </article>
      <article class="edit-item clearfix post-smeared">
        <label for="edit-field-smeared-on-und-select">Smeared on </label>
        <select class="select-or-other-select form-select" id="edit-field-smeared-on-und-select" name="field_smeared_on[und][select]">
          <option value="_none">None</option>
          <option value="Yelp®">Yelp®</option>
          <option value="Google®">Google®</option>
          <option value="Yahoo®">Yahoo®</option>
          <option value="Angie's List®">Angie's List®</option>
          <option value="select_or_other">Other</option>
        </select>
      </article>
      <article class="edit-item clearfix post-title">
        <input class="maxlength form-text required maxlength-processed" type="text" id="edit-title" name="title" value="" size="60" maxlength="140" placeholder="Who or what is it about?">
      </article>
      <article class="edit-item clearfix post-text">
        <textarea class="text-full form-textarea" id="edit-body-und-0-value" name="body[und][0][value]" cols="60" rows="5" placeholder="Your creople story..."></textarea>
      </article>
      <article class="edit-item clearfix">
        <label>Attachment (Max size: 12 Mb)</label>
        <section class="attachment">
          <article id="video" class="inactive">
            <span></span>
            <div>
              <!--<input type="file" id="edit-field-upload-video-und-0-upload--3" name="files[field_upload_video_und_0]" size="22" class="form-file">
              <input type="submit" id="edit-field-upload-video-und-0-upload-button--3" name="field_upload_video_und_0_upload_button" value="Upload" class="form-submit ajax-processed">-->
            </div>
          </article>
          <article id="picture" class="inactive">
            <span></span>
            <div>
              <input type="file" id="edit-field-upload-media-und-0-upload" name="files[field_upload_media_und_0]" size="22" class="form-file">
              <input type="submit" id="edit-field-upload-media-und-0-upload-button" name="field_upload_media_und_0_upload_button" value="Upload" class="form-submit ajax-processed">
            </div>
          </article>
          <article id="audio" class="inactive">
            <span></span>
            <div>
              <input type="file" id="edit-field-upload-audio-und-0-upload" name="files[field_upload_audio_und_0]" size="22" class="form-file">
              <input type="submit" id="edit-field-upload-audio-und-0-upload-button" name="field_upload_audio_und_0_upload_button" value="Upload" class="form-submit ajax-processed">
            </div>
          </article>
          <article id="none" class="active">
            <span>None</span>
          </article>
          <div class="uploaded-files"></div>
        </section>
      </article>
      <article class="edit-item clearfix post-name-place">
        <div>
          <input type="text" id="name-of-place" name="name-of-place" value="" size="17" class="form-text" placeholder="Name of Place">
        </div>
        <a href="javascript:void(0);" class="map-mark" onclick="getaddr()"></a>

      </article>
      <article class="edit-item clearfix post-street">
        <input type="text" id="edit-field-address-und-0-street" name="field_address[und][0][street]" value="" size="64" maxlength="255" class="form-text" placeholder="Street">
      </article>
      <article class="edit-item clearfix post-city">
        <input type="text" id="edit-field-address-und-0-city" name="field_address[und][0][city]" value="" size="64" maxlength="255" class="form-text" placeholder="City">
      </article>
      <article class="edit-item clearfix post-postal-code">
        <input type="text" id="edit-field-address-und-0-postal-code" name="field_address[und][0][postal_code]" value="" size="16" maxlength="16" class="form-text" placeholder="ZIP Code">
      </article>
      <article class="edit-item clearfix post-state">
        <label for="edit-field-address-und-0-province">State/Province </label>
        <input class="location_auto_province form-text form-autocomplete" type="text" id="edit-field-address-und-0-province" name="field_address[und][0][province]" value="" size="64" maxlength="64" autocomplete="OFF" aria-autocomplete="list" placeholder="Enter State">
      </article>
      <article class="edit-item clearfix post-country">
        <label for="edit-field-address-und-0-country">Country </label>
        <select class="location_auto_country form-select ajax-processed location-processed" id="edit-field-address-und-0-country" name="field_address[und][0][country]">
          <option value="">Select</option><option value="xx">NOT LISTED</option><option value="af">Afghanistan</option><option value="ax">Aland Islands</option><option value="al">Albania</option><option value="dz">Algeria</option><option value="as">American Samoa</option><option value="ad">Andorra</option><option value="ao">Angola</option><option value="ai">Anguilla</option><option value="aq">Antarctica</option><option value="ag">Antigua and Barbuda</option><option value="ar">Argentina</option><option value="am">Armenia</option><option value="aw">Aruba</option><option value="au">Australia</option><option value="at">Austria</option><option value="az">Azerbaijan</option><option value="bs">Bahamas</option><option value="bh">Bahrain</option><option value="bd">Bangladesh</option><option value="bb">Barbados</option><option value="by">Belarus</option><option value="be">Belgium</option><option value="bz">Belize</option><option value="bj">Benin</option><option value="bm">Bermuda</option><option value="bt">Bhutan</option><option value="bo">Bolivia</option><option value="ba">Bosnia and Herzegovina</option><option value="bw">Botswana</option><option value="bv">Bouvet Island</option><option value="br">Brazil</option><option value="io">British Indian Ocean Territory</option><option value="vg">British Virgin Islands</option><option value="bn">Brunei</option><option value="bg">Bulgaria</option><option value="bf">Burkina Faso</option><option value="bi">Burundi</option><option value="kh">Cambodia</option><option value="cm">Cameroon</option><option value="ca">Canada</option><option value="cv">Cape Verde</option><option value="bq">Caribbean Netherlands</option><option value="ky">Cayman Islands</option><option value="cf">Central African Republic</option><option value="td">Chad</option><option value="cl">Chile</option><option value="cn">China</option><option value="cx">Christmas Island</option><option value="cc">Cocos (Keeling) Islands</option><option value="co">Colombia</option><option value="km">Comoros</option><option value="cg">Congo (Brazzaville)</option><option value="cd">Congo (Kinshasa)</option><option value="ck">Cook Islands</option><option value="cr">Costa Rica</option><option value="hr">Croatia</option><option value="cu">Cuba</option><option value="cw">Curaçao</option><option value="cy">Cyprus</option><option value="cz">Czech Republic</option><option value="dk">Denmark</option><option value="dj">Djibouti</option><option value="dm">Dominica</option><option value="do">Dominican Republic</option><option value="ec">Ecuador</option><option value="eg">Egypt</option><option value="sv">El Salvador</option><option value="gq">Equatorial Guinea</option><option value="er">Eritrea</option><option value="ee">Estonia</option><option value="et">Ethiopia</option><option value="fk">Falkland Islands</option><option value="fo">Faroe Islands</option><option value="fj">Fiji</option><option value="fi">Finland</option><option value="fr">France</option><option value="gf">French Guiana</option><option value="pf">French Polynesia</option><option value="tf">French Southern Territories</option><option value="ga">Gabon</option><option value="gm">Gambia</option><option value="ge">Georgia</option><option value="de">Germany</option><option value="gh">Ghana</option><option value="gi">Gibraltar</option><option value="gr">Greece</option><option value="gl">Greenland</option><option value="gd">Grenada</option><option value="gp">Guadeloupe</option><option value="gu">Guam</option><option value="gt">Guatemala</option><option value="gg">Guernsey</option><option value="gn">Guinea</option><option value="gw">Guinea-Bissau</option><option value="gy">Guyana</option><option value="ht">Haiti</option><option value="hm">Heard Island and McDonald Islands</option><option value="hn">Honduras</option><option value="hk">Hong Kong S.A.R., China</option><option value="hu">Hungary</option><option value="is">Iceland</option><option value="in">India</option><option value="id">Indonesia</option><option value="ir">Iran</option><option value="iq">Iraq</option><option value="ie">Ireland</option><option value="im">Isle of Man</option><option value="il">Israel</option><option value="it">Italy</option><option value="ci">Ivory Coast</option><option value="jm">Jamaica</option><option value="jp">Japan</option><option value="je">Jersey</option><option value="jo">Jordan</option><option value="kz">Kazakhstan</option><option value="ke">Kenya</option><option value="ki">Kiribati</option><option value="kw">Kuwait</option><option value="kg">Kyrgyzstan</option><option value="la">Laos</option><option value="lv">Latvia</option><option value="lb">Lebanon</option><option value="ls">Lesotho</option><option value="lr">Liberia</option><option value="ly">Libya</option><option value="li">Liechtenstein</option><option value="lt">Lithuania</option><option value="lu">Luxembourg</option><option value="mo">Macao S.A.R., China</option><option value="mk">Macedonia</option><option value="mg">Madagascar</option><option value="mw">Malawi</option><option value="my">Malaysia</option><option value="mv">Maldives</option><option value="ml">Mali</option><option value="mt">Malta</option><option value="mh">Marshall Islands</option><option value="mq">Martinique</option><option value="mr">Mauritania</option><option value="mu">Mauritius</option><option value="yt">Mayotte</option><option value="mx">Mexico</option><option value="fm">Micronesia</option><option value="md">Moldova</option><option value="mc">Monaco</option><option value="mn">Mongolia</option><option value="me">Montenegro</option><option value="ms">Montserrat</option><option value="ma">Morocco</option><option value="mz">Mozambique</option><option value="mm">Myanmar</option><option value="na">Namibia</option><option value="nr">Nauru</option><option value="np">Nepal</option><option value="nl">Netherlands</option><option value="an">Netherlands Antilles</option><option value="nc">New Caledonia</option><option value="nz">New Zealand</option><option value="ni">Nicaragua</option><option value="ne">Niger</option><option value="ng">Nigeria</option><option value="nu">Niue</option><option value="nf">Norfolk Island</option><option value="mp">Northern Mariana Islands</option><option value="kp">North Korea</option><option value="no">Norway</option><option value="om">Oman</option><option value="pk">Pakistan</option><option value="pw">Palau</option><option value="ps">Palestinian Territory</option><option value="pa">Panama</option><option value="pg">Papua New Guinea</option><option value="py">Paraguay</option><option value="pe">Peru</option><option value="ph">Philippines</option><option value="pn">Pitcairn</option><option value="pl">Poland</option><option value="pt">Portugal</option><option value="pr">Puerto Rico</option><option value="qa">Qatar</option><option value="re">Reunion</option><option value="ro">Romania</option><option value="ru">Russia</option><option value="rw">Rwanda</option><option value="bl">Saint Barthélemy</option><option value="sh">Saint Helena</option><option value="kn">Saint Kitts and Nevis</option><option value="lc">Saint Lucia</option><option value="mf">Saint Martin (French part)</option><option value="pm">Saint Pierre and Miquelon</option><option value="vc">Saint Vincent and the Grenadines</option><option value="ws">Samoa</option><option value="sm">San Marino</option><option value="st">Sao Tome and Principe</option><option value="sa">Saudi Arabia</option><option value="sn">Senegal</option><option value="rs">Serbia</option><option value="sc">Seychelles</option><option value="sl">Sierra Leone</option><option value="sg">Singapore</option><option value="sx">Sint Maarten</option><option value="sk">Slovakia</option><option value="si">Slovenia</option><option value="sb">Solomon Islands</option><option value="so">Somalia</option><option value="za">South Africa</option><option value="gs">South Georgia and the South Sandwich Islands</option><option value="kr">South Korea</option><option value="ss">South Sudan</option><option value="es">Spain</option><option value="lk">Sri Lanka</option><option value="sd">Sudan</option><option value="sr">Suriname</option><option value="sj">Svalbard and Jan Mayen</option><option value="sz">Swaziland</option><option value="se">Sweden</option><option value="ch">Switzerland</option><option value="sy">Syria</option><option value="tw">Taiwan</option><option value="tj">Tajikistan</option><option value="tz">Tanzania</option><option value="th">Thailand</option><option value="tl">Timor-Leste</option><option value="tg">Togo</option><option value="tk">Tokelau</option><option value="to">Tonga</option><option value="tt">Trinidad and Tobago</option><option value="tn">Tunisia</option><option value="tr">Turkey</option><option value="tm">Turkmenistan</option><option value="tc">Turks and Caicos Islands</option><option value="tv">Tuvalu</option><option value="vi">U.S. Virgin Islands</option><option value="ug">Uganda</option><option value="ua">Ukraine</option><option value="ae">United Arab Emirates</option><option value="gb">United Kingdom</option><option value="us" selected="selected">United States</option><option value="um">United States Minor Outlying Islands</option><option value="uy">Uruguay</option><option value="uz">Uzbekistan</option><option value="vu">Vanuatu</option><option value="va">Vatican</option><option value="ve">Venezuela</option><option value="vn">Vietnam</option><option value="wf">Wallis and Futuna</option><option value="eh">Western Sahara</option><option value="ye">Yemen</option><option value="zm">Zambia</option><option value="zw">Zimbabwe</option></select>
      </article>
    </section>
    <?php
     /* global $user;
      module_load_include('inc', 'node', 'node.pages');
      $node = (object) array('uid' => $user->uid, 'name' => (isset($user->name) ? $user->name : ''), 'type' => 'posts', 'language' => LANGUAGE_NONE);

      $elements = drupal_get_form("posts_node_form", $node);
      $form = drupal_render($elements);
      print $form;*/

    ?>
    <?php print render($page['content']); ?>
    <footer class="main-footer">
    </footer>
  <?php else:?>
   <?php if(arg(0) == 'forward' && !arg(1)):?>
        <?php if($_SESSION['popup'] == true):?>
           <script>alert('This post has been shared with your friends')</script>
           <?php $_SESSION['popup'] =false;?>
        <?php endif;?>
        <style>
          #block-search-form {
            display: none;;
          }
          #edit-actions {
            margin-top: 15px;;
            margin-bottom: 25px;;
          }
        </style>
      <header class="main-header">
        <section class="header-edit-block clearfix">
          <a class="edit-cancel" href="javascript:history.back()">Cancel</a><div class="page-title">Share by e-mail</div>
        </section>
      </header>
      <section class="main-content" role="content">
        <?php print render($page['content']); ?>
      </section>

    <?php else:?>
        <?php if(strripos($path,'user/' . $user->uid . '/friends') !== false ):?>
        <style>
          #block-search-form {
            display: none;;
          }
          thead {
            display: none;
          }

        </style>

        <header class="main-header">
          <section class="header-edit-block clearfix">
            <a class="edit-cancel" href="javascript:history.back()">Cancel</a><div class="page-title">Friends</div><a class="edit-ok" href="/invite/add/invite_by_email">Invite</a>
          </section>
        </header>

        <section class="main-content clearfix" role="content">
          <section class="friends-tab">
            <section class="friends-tab-name clearfix">
              <?php $first_tab = (strripos($path,'user/' . $user->uid . '/friends/flaggedphone') === false && strripos($path,'user/' . $user->uid . '/friends/pending') === false)?'active-th':'inactive-th'?>
              <span class="<?=$first_tab?>"  class="tab-label"><a href="/user/<?=$user->uid?>/friends">My friends</a></span>
              <span class="<?=(strripos($path,'user/' . $user->uid . '/friends/flaggedphone') !== false )?'active-th':'inactive-th'?>"  class="tab-label"><a href="/user/<?=$user->uid?>/friends/flaggedphone">Invitations</a></span>
              <span class="<?=(strripos($path,'user/' . $user->uid . '/friends/pending') !== false )?'active-th':'inactive-th'?>"  class="tab-label"><a href="/user/<?=$user->uid?>/friends/pending">Pending</a></span>
            </section>

            <article class="friend-tab-item active-frti">
              <section class="friends-list">
                <?php print render($page['content']); ?>
              </section>
            </article>

          </section>

        </section>
        <footer class="main-footer">
        </footer>
        <?php else:?>
          <?php if($path == 'invite/add/invite_by_email'):?>
          <header class="main-header">
            <section class="header-edit-block clearfix">
              <a class="edit-cancel" href="javascript:history.back()">Back</a><div class="page-title">Invite Friends</div><a class="edit-ok"></a>
            </section>
          </header>

          <section class="main-content clearfix" role="content">
            <article class="edit-item clearfix">
              <img class="mail-icon" src="/<?=path_to_theme()?>/css/images/mail.png"><label for="edit-field-invitation-email-address-und-0-value">Invite your friends to join Creople</label>
              <div style="clear: both;"></div>
              <?php print render($page['content']); ?>
            </article>

          </section>
          <footer class="main-footer">
          </footer>
      <?php else:?>
      <?php if($path == 'user/' . $user->uid . '/edit'):?>

            <?php drupal_add_js("/" . path_to_theme() . "/js/user-edit.js");?>
            <header class="main-header">
              <section class="header-edit-block clearfix">
                <a class="edit-cancel" href="/">Cancel</a><div class="page-title">Edit profile</div><a class="edit-ok"></a>
              </section>
            </header>

            <section class="main-content" role="content">
              <?php print render($page['content']['system_main']); ?>
            </section>

            <section id="edit-user-form" class="main-content" role="content">
              <article class="edit-item clearfix">
                <div class="user-avatar-div">
                  <a class="user-pic" href="#"><img src="/<?=path_to_theme()?>/css/images/default_avatar.png"></a>
                </div>
                <div class="user-name-username">
                  <section class="user-login">
                    <input class="username form-text required" type="text" id="edit-name" name="name" value="tl01" >
                  </section>

                  <section class="user-name">
                    <input class="text-full form-text required" type="text" id="edit-field-name-und-0-value" name="field_name[und][0][value]" value="Alex">
                  </section>
                </div>

              </article>
              <article class="edit-item clearfix my-likes">
                <label for="edit-field-likes-und-0-value">My Likes </label>
                <textarea class="text-full form-textarea" id="edit-field-likes-und-0-value" name="field_likes[und][0][value]" cols="60" rows="1" placeholder="I like..."></textarea>
              </article>
              <article class="edit-item clearfix my-dislikes">
                <label for="edit-field-dislikes-und-0-value">My Dislikes </label>
                <textarea class="text-full form-textarea" id="edit-field-dislikes-und-0-value" name="field_dislikes[und][0][value]" cols="60" rows="1" placeholder="I dislike..."></textarea>
              </article>
              <article class="edit-item clearfix email-address">
                <label for="edit-mail">E-mail address <span class="form-required" title="This field is required.">*</span></label>
                <input type="text" id="edit-mail" name="mail" value="dsacsd@inbox.ru" size="60" maxlength="254" class="form-text required">
              </article>
              <article class="edit-item clearfix email-notificate">
                <div>
                  <input type="checkbox" id="edit-field-notification-und-email" name="field_notification[und][Email]" value="Email" checked="checked" class="form-checkbox">
                  <label class="option" for="edit-field-notification-und-email">E-mail notification<span></span></label>
                </div>
              </article>

              <article class="edit-item clearfix phone-number">
                <label for="edit-field-phone-number-und-0-value">Phone number </label>
                <input type="text" id="edit-field-phone-number-und-0-value" name="field_phone_number[und][0][value]" value="" placeholder="My phone number..." size="17" class="form-text">
              </article>
              <article class="edit-item clearfix day-birth">
                <label for="edit-field-date-of-birth-und-0-value-datepicker-popup-0">Date of Birth (MM/DD/YYYY)</label>
                <input class="date-clear form-text hasDatepicker date-popup-init" type="text" id="edit-field-date-of-birth-und-0-value-datepicker-popup-0" name="field_date_of_birth[und][0][value][date]" value="" size="20" maxlength="30" placeholder="My Date of Birth">
                <img id="customjs_calendar_icon" class="date-picker-img" src="./images/calendar_icon.png">
              </article>
              <article class="edit-item clearfix">
                <a class="resetpassword" href="./resetpassword.html">reset password</a>
              </article>
              <article class="edit-item clearfix passwords-form">
                <div>

                </div>
              </article>

            </section>
            <footer class="main-footer">
            </footer>
            <footer class="main-footer">
            </footer>
            <?php else:?>
<!--
<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script><script type="text/javascript">// <![CDATA[
//FB.init({ appId : '894051824018610', });
FB.init({ 
    a2ppId : '894051824018610',
    status     : true,
    xfbml      : true,
    version    : 'v2.3' });
// ]]></script>-->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '894051824018610',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    jQuery(document).ready(function(){
        jQuery('.share_button5').click(function(e){
            console.log(jQuery(this).attr('m_image'));
            e.preventDefault();
            FB.ui( {
                app_id:'894051824018610',
                method: 'feed',
                name: jQuery(this).attr('m_title'),
                link: jQuery(this).attr('m_path'),
                picture: jQuery(this).attr('m_image'),
                //caption: 'Мне нравится этот блог!',
                description: jQuery(this).attr('m_desc'),
                message: ''
            });
        });
    });
</script>
<?php global $base_url; ?>

<?php  if(isset($_GET['popup']) && ($_GET['popup'] == 1)){ 
  //dsm($page);
  //var_dump($page['content']);
  print render($page['content']['system_main']); 
}else{  ?>
 <?php if(arg(0) != 'search'):?>

    <header class="main-header">
      <section class="header-edit-block clearfix">
        <a class="edit-cancel"  <?=(arg(0) != 'map')? 'href="javascript:history.back()"' :'href="#" onclick="window.close();"'?> ><?= ($path == 'select-friends')?'Cancel':'Back'?></a><div class="page-title"><?=drupal_get_title()?></div>
      </section>
    </header>
<?php endif;?>
<?php if(arg(0) == 'search'):?>
              <header class="main-header">
                <div class="header-top">
                  <?php
                  global $user;
                  $u = user_load($user->uid);
                  $uri = isset($u->field_profile_picture['und'][0]['uri'])?$u->field_profile_picture['und'][0]['uri']:null;
                  ?>
                  <?php if($user->uid > 0):?>
                    <a class="edit-profile" href="/user/<?=$user->uid?>/edit"><img class="user-pic" src="<?=($uri)?image_style_url('current_user_pix', $uri):'/sites/default/files/styles/current_user_pix/public/pictures/sampledp.png' ?>"></a>
                  <?php else:?>
                    <a class="edit-profile colorbox-inline" href="?width=280&height=100&inline=true#popup-div-user-edit"><img class="user-pic" src="<?=($uri)?image_style_url('current_user_pix', $uri):'/sites/default/files/styles/current_user_pix/public/pictures/sampledp.png' ?>"></a>
                  <?php endif;?>
                  <div style="display: none"><div class="popup-div" id="popup-div-user-edit">Only registered user can edit profile</div></div>
                  <?php
                  $elements = drupal_get_form("search_block_form");
                  $form = drupal_render($elements);
                  ?>
                  <div id="search-form"><?php echo $form;?></div>
                  <?php if($user->uid > 0) {?><a href="/node/add/posts" class="add-post"></a><?php } else{?>
                    <a href="?width=280&height=100&inline=true#popup-div" class="add-post colorbox-inline"></a>
                  <?php }?>
                  <div style="display: none"><div class="popup-div" id="popup-div">Only registered user can make a post</div></div>
                </div>
                <div class="header-logo">
                  <div class="underline"></div>
                  <a class="go-home" href="javascript:history.back()">Home</a>
                  <div class="logo-image"></div>
                  <div class="circle-logo"><img src="/<?=path_to_theme()?>/css/images/search_big.png"></div>
                </div>
                <div class="map-it-block">
                  <div class="title"><span>map it</span></div>
                  <div class="show-results clearfix">
                    <label>Show results on map</label>
                    <a class="map-it-button" href="#" target="_blank"><img src="/<?=path_to_theme()?>/css/images/map_it.png"></a>
                  </div>
                </div>
              </header>
<?php endif;?>
<section class="main-content" role="content">

  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix row">
     <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>

    <div id="middle-content" class="column col-xs-12 col-sm-12 col-md-9 col-lg-9"><div class="section ">
      <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
      <a id="main-content"></a>
      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
          <div class="clearfix"></div>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>

    </div></div> <!-- /.section, /#content -->

  </div></div> <!-- /#main, /#main-wrapper -->
  </section>

<?php  } ?>
          <?php endif;?>
        <?php endif;?>
      <?php endif;?>
    <?php endif;?>
  <?php endif;?>
<?php endif;?>
