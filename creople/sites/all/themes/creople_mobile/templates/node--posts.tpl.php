﻿<?php if($view_mode == 'teaser' || $view_mode == 'preview'):?>
  <?php
  global $user;
  global $base_url;
  $n=node_load($node->nid);
  $title = utf8_decode($node->title);
  $content['body'][0]['#markup'] = utf8_decode($content['body'][0]['#markup']);
  ?>
  <?php //krumo($node);exit;?>
  <?php $u = user_load($node->uid);?>
  <a class="content-item" href="<?=drupal_get_path_alias('node/' . $node->nid)?>">
    <header class="article-header">
      <h2 class="article-title"><?=$title?></h2>
      <div class="article-image">
        <?php if(isset($content['field_upload_media']['#object']->field_upload_media['und'][0]['uri'])):?>
          <div class="field-type-file custom">
              <img src="<?=image_style_url('postimage', $content['field_upload_media']['#object']->field_upload_media['und'][0]['uri'])?>" />
          </div>
        <?php endif;?>
        <?
        print render($content['field_upload_video']);
        print render($content['field_upload_audio']);
        ?>
      </div>
      <div class="article-info">
        <?if(count($node->field_smeared_on) > 0):?><div class="smeared-on">Smeared on: <?=($node->field_smeared_on['und'][0]['value'])?></div> <?php endif;?>
        <div class="article-author"><?=$u->name?></div>
      </div>
    </header>
    <div class="article-body">
      <?php echo $content['body'][0]['#markup']; ?>
    </div>
    <footer class="article-footer">
      <div class="article-date">2016-03-17 17:28:43</div>
    </footer>
  </a>
<?php endif;?>
<?php if($view_mode == 'full'):?>
  <style>
    #edit-comment-body-und-0-format, #edit-back-to-forum {
        display: none!important;
    }
    </style>
  <?php
    global $user;
    global $base_url;
    $n=node_load($node->nid);
  $title = utf8_decode($node->title);
  $content['body'][0]['#markup'] = utf8_decode($content['body'][0]['#markup']);
  ?>
  <?php
  $img_name = 'rottern_customers';
$tax = $n->field_categories['und'][0]['tid'];

  switch($tax) {
    case 3:
      $img_name = 'rottern_customers';
      break;
    case 4:
      $img_name = 'bullies_unmasked';
      break;
    case 5:
      $img_name = 'bosses';
      break;
    case 6:
      $img_name = 'teacher';
      break;
    case 7:
      $img_name = 'random_wtf';
      break;

  }
//var_dump(path_to_theme() . "/css/images/menu_" . $img_name . ".png");exit;
  ?>
  <?php if($_GET['jumpin'] != 1):?>
      <header class="main-header">
        <section class="header-edit-block clearfix">
          <a class="edit-cancel" href="javascript:history.back()">Back</a>
          <div class="post-category-title"><?=strip_tags(render($content['field_categories']))?></div>
          <div class="post-category-image">
            <img src="/<?=path_to_theme()?>/css/images/menu_<?=$img_name?>.png">
          </div>
        </section>
      </header>
  <?php else:?>
      <header class="main-header">
        <section class="header-edit-block clearfix">
          <a class="edit-cancel" href="javascript:history.back()">Back</a><div class="page-title">Jump in</div><a class="edit-ok" href="/node/<?=$node->nid?>?jumpin=1">Refresh</a>
        </section>
      </header>
  <?php endif;?>
  <section class="main-content" role="content">
    <article class="content-item post-view-item">
      <header class="article-header clearfix">
        <h2 class="article-title"><?=$title?></h2>
        <div class="article-image">
          <?php if(isset($content['field_upload_media']['#object']->field_upload_media['und'][0]['uri'])):?>
            <div class="field-type-file custom">
              <a class="colorbox-inline" href="?width=90%20&height=90%20&inline=true#popup-div-images">
                <img src="<?=image_style_url('postimage', $content['field_upload_media']['#object']->field_upload_media['und'][0]['uri'])?>" />
              </a>
              <div style="display: none"><div class="popup-div" id="popup-div-images"><img src="<?=image_style_url('large', $content['field_upload_media']['#object']->field_upload_media['und'][0]['uri'])?>"/></div></div>
            </div>
          <?php endif;?>
          <?
          print render($content['field_upload_video']);
          print render($content['field_upload_audio']);
          ?>
        </div>
        <div class="article-info">
          <div style="display: inline-block;vertical-align: middle">
            <div class="article-date"><?php print $date;?></div>
            <?php
            if($n->field_anonymous[LANGUAGE_NONE][0]['value']!=1){
              //print '<div class="author-picture">'.$user_picture.'</div>';
              $u = user_load($node->uid);
              if($u->field_profile_picture['und'][0]['filename']){
                if(user_is_logged_in()){
                  $user_picture = "<a class=\"author-avatar\" href='".$base_url."/user/".$uid."'><img class=\"author-avatar\" src='" . image_style_url('postuserphoto', $u->field_profile_picture['und'][0]['uri']) . "'/></a>";
                }
                else {
                  $user_picture = "<a class=\"author-link\" href=\"#\"><img class=\"author-avatar\" src='" . image_style_url('postuserphoto', $u->field_profile_picture['und'][0]['uri']) . "' /></a>";
                }
              } else {
                if(user_is_logged_in()){
                  $user_picture = $user_picture = "<a class=\"author-avatar\" href='".$base_url."/user/".$uid."'><img class=\"author-avatar\"  src='".$base_url."/sites/default/files/default_avatar.png'/></a>";
                } else {
                  $user_picture = $user_picture = "<a class=\"author-link\" href=\"#\"><img  class=\"author-avatar\"  src='".$base_url."/sites/default/files/default_avatar.png'/></a>";
                }
              }

              print $user_picture;
              print '<div class="article-author">'.$u->name.'</div>';
            }
            else {
              //print '<div class="author-picture"><a href=""><img src="'.$base_url.'/sites/default/files/default_avatar.png" /></a></div>';
              print '<a class="author-link" href="#"><img  class="author-avatar"  src="'.$base_url.'/sites/default/files/default_avatar.png" /></a>';
              print '<div class="article-author">Anonymous</div>';
            }
            ?>
          </div>

          <?php if(($user->uid == $n->uid) && $user->uid > 0):?>
            <div class="edit-post" style="display: inline-block;vertical-align: middle">
              <?php if($_GET['jumpin'] != 1):?><a href="/node/<?=$n->nid?>/edit"><img src="/<?=path_to_theme()?>/css/images/icon_post.png"></a><?php endif;?>
            </div>
          <?php endif;?>
        </div>
      </header>
      <div class="article-body">
        <?php echo $content['body'][0]['#markup']; ?>
      </div>
      <?php if($_GET['jumpin'] != 1):?>
        <footer class="article-footer">
          <section class="post-view-footer">
            <article class="favorive-block">
              <?php print render($content['links']);?>
              <!--<a class="favorite unfiled" href="#"></a>-->
            </article>
            <article class="map-link">
              <?php //print render($content['links']);?>
              <a href="http://creople.com/map/<?=$node->nid?>" target="_blank"><img src="/<?=path_to_theme()?>/css/images/map_it.png"></a>
            </article>
            <div id="cheat-links" style="display:none;"><?php print render($content['links']);?></div>
            <article class="post-address">
              <?php $location = $content['field_address'][0]['#location']?>
              <?php echo ($location['street'] != "")?$location['street'] . '<br/>':"" ?>
              <?php echo ($location['city'] != "")?$location['city']:""; ?>, <?php echo $location['province_name']; ?><br />
              <?php echo ($location['country_name'] != "")?$location['country_name'] . '<br/>':''; ?>
              <?php echo $location['postal_code']; ?>
            </article>
          </section>
        </footer>
      <?php endif;?>

      <?php if($_GET['jumpin']==1):?>
        <article class="jump-in-add-comment"><?=render($content['comments']['comment_form'])?></article>
        <?php

        if( !is_null($content['comments']['comments']) && $user->uid!=0 && $user->uid):?>
          <section class="jump-in-comments"><?=utf8_decode(render(array_reverse($content['comments']['comments'])))?></section>
        <?php endif; ?>
      <?php endif;?>

    </article>

  </section>
  <?php
  $mPath = url('node/' . $node->nid, array('absolute' => TRUE));
  $mTitle = $node->title;
  global $user;


  $im_uri = isset($content['field_upload_media']['#items'][0]['uri'])?$content['field_upload_media']['#items'][0]['uri']:null;
  $im_url = ($im_uri)?image_style_url('large', $im_uri):'';
  $data_options = sharethis_get_options_array();
  //krumo($content['links']);
  ?>

  <link rel="image_src" href="<?=$im_url?>" />

  <?php if($_GET['jumpin'] != 1):?>
    <footer class="main-footer">
      <section class="main-footer-inner clearfix">
        <article class="score-block">
          <?php print render($content['field_voting_field']);?>
        </article>
        <article class="flag-block">
          <?php if($user->uid > 0):?>
            <?php print render($content['links']);?>
          <?php else:?>
            <a href="#" onclick="return false;" class="flag-action"><img src="/<?=path_to_theme()?>/css/images/icon_flag_white.png"></a>
          <?php endif;?>
        </article>
        <article class="jump-in-block">
          <?php if($user->uid > 0 || 1):?>
            <a href="/node/<?=$node->nid?>?jumpin=1" class="jump-in-action"><img src="/<?=path_to_theme()?>/css/images/icon_jump_in.png"></a>
          <?php else:?>
            <a href="#" class="jump-in-action" onclick="return false;" ><img src="/<?=path_to_theme()?>/css/images/icon_jump_in.png"></a>
          <?php endif;?>

        </article>
        <article class="share-block">
          <button class="share-action"><img src="/<?=path_to_theme()?>/css/images/icon_share.png"></button>
          <div id="share-menu" class="share-menu" style="display:none;">
            <!--<a class="share-facebook" href="#"></a>
            <a class="share-twitter" href="#"></a>-->
            <a href="https://www.facebook.com/sharer/sharer.php?u=&t=" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent('http://creople.com/node/<?=$node->nid?>') + '&t=' + encodeURIComponent('http://creople.com/node/<?=$node->nid?>')); return false;"><img src="/<?=path_to_theme()?>/css/images/icon_facebook_small.png"></a>
            <a href="https://twitter.com/intent/tweet?source=&text=:%20" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent('<?=$node->title?>') + ':%20'  + encodeURIComponent('http://creople.com/node/<?=$node->nid?>')); return false;"><img src="/<?=path_to_theme()?>/css/images/icon_twitter.png"></a>
            <a class="share-email" href="/forward?path=node/<?=$node->nid?>"></a>
            <?php if($user->uid > 1):?><a class="share-friend" href="/select-friends?<?=$node->nid?>"></a><?php endif;?>
          </div>
        </article>
        <div class="clearfix"></div>
    </footer>
  <?php endif;?>

<?php endif;?>
<?php if(isset($_SESSION['popup_alert']) && 0):?>
<div class="popup_message"><?php print $_SESSION['text_popup'];?></div>
<?php unset($_SESSION['popup_alert']); unset($_SESSION['text_popup']);?>
<script>
setTimeout(function() {
    jQuery('.popup_message').fadeOut('slow');
    }, 3500);
</script>
<?php endif; ?>
