<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *#block-system-main
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<?php
/*
  global $user;
  $path = drupal_get_path_alias($_GET["q"]);
  global $_COOKIE;
  if($user->uid == 0 && $path != 'tos-anonimus'&& $path != 'front' && $path != 'user/register' && $path != 'about-us' && $path != 'terms-of-service' && $path != 'faq' && $path != 'contact' ) {
    if(!isset($_COOKIE['accept_tos'])) {
      drupal_goto('front');
    }
  }
  /* $uu = user_load(285);
    var_dump($uu);
    var_dump(user_save($uu));
    exit;
    */
?>
<!--
<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script><script type="text/javascript">// <![CDATA[
//FB.init({ appId : '894051824018610', });
FB.init({
    a2ppId : '894051824018610',
    status     : true,
    xfbml      : true,
    version    : 'v2.3' });
// ]]></script>-->
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '894051824018610',
            xfbml      : true,
            version    : 'v2.4'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    jQuery(document).ready(function(){
        jQuery('.share_button5').click(function(e){
            console.log(jQuery(this).attr('m_image'));
            e.preventDefault();
            FB.ui( {
                app_id:'894051824018610',
                method: 'feed',
                name: jQuery(this).attr('m_title'),
                link: jQuery(this).attr('m_path'),
                picture: jQuery(this).attr('m_image'),
                //caption: 'Мне нравится этот блог!',
                description: jQuery(this).attr('m_desc'),
                message: ''
            });
        });
    });
</script>
<?php global $base_url; ?>

<?php  if(isset($_GET['popup']) && ($_GET['popup'] == 1)){
    //dsm($page);
    //var_dump($page['content']);
    print render($page['content']['system_main']);
    //krumo($_COOKIE);
}else{  ?>

    <base href="<?php  print $base_url ?>" />
            <?php if($user->uid == 0 && !$_COOKIE['was_accept_tos']):?>
                <header class="login-header">
                    <a href="/" title="Home" rel="home">
                        <img src="/<?=path_to_theme()?>/css/images/creoplelogobig.png" class="logo">
                    </a>
                    <button type="button" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <nav id="main-navigtion" class="login-navigation" role="navigation" style="display:none;">
                        <a class="menu-item" href="<?php print $base_url;?>/about-us">What is Creople</a>
                        <a class="menu-item" href="<?php print $base_url;?>/terms-of-service">Terms of Service</a>
                        <a class="menu-item" href="<?php print $base_url;?>/privacy-policy">Privacy Policy</a>
                        <a class="menu-item" href="<?php print $base_url;?>/faq">FAQ</a>
                        <a class="menu-item" href="mailto:contact@creople.com">Contact Us</a>
                        <a class="menu-item" href="<?php print $base_url;?>/contact">Make Creople Better</a>
                        <a class="menu-item" href="http://creoples.com" target="_blank">Shop Creople</a>
                        <section class="burger-social menu-item">
                            <a target="_blank" class="facebook-burger" href="https://www.facebook.com/creoplecom-156967994349887/?ref=hl"></a>
                            <a target="_blank" class="twitter-burger" href="https://twitter.com/creoples"></a>
                            <a target="_blank" class="youtube-burger" href="https://www.youtube.com/channel/UCdD7bqTBb9xjyBLG_eVLlFw?guided_help_flow=3"></a>
                        </section>
                    </nav>
                </header>
                <section class="site-slogan"><span class="anomin-front-text-1">Fight back!</span></section>
                <section class="content" role="content">

                    <article class="sign-in">
                        <img class="img-responsive" src="/<?=path_to_theme()?>/css/images/HomepageMainGraphic.png">
                        <span class="anomin-front-text-2">Dedicated to exposing the injustices you experience every day</span>
                        <!--
                        <form name="signin" id="user-login-form">
                            <input type="text" name="user-name" class="form-input-text" placeholder="User Name">
                            <inputы type="password" name="user-pass" class="form-input-text" placeholder="Password">
                            <input type="submit" value="sign in" class="form-submit">
                            <div class="remember-me">
                                <input type="checkbox" name="rememberme" id="rememberme">
                                <label for="rememberme">Remember me <span></span></label>
                            </div>
                        </form>
                        -->
                        <?php
                            $elements = drupal_get_form("user_login");
                            $form = drupal_render($elements);
                            echo $form;
                        ?>
                        <a href="/user/password" class="forgot-password">Forgot password</a>
                        <div class="singup-row">
                            <a href="/user/register">sign up now</a><span>or</span><a href="/tos-anonimus" >browse as guest</a>
                        </div>
                    </article>
                </section>
                <footer></footer>
            <?php else:?>

        <header class="main-header">
            <div class="header-top">
                <?php
                global $user;
                $u = user_load($user->uid);
                $uri = isset($u->field_profile_picture['und'][0]['uri'])?$u->field_profile_picture['und'][0]['uri']:null;
                ?>
                <?php if($user->uid > 0):?>
                    <a class="edit-profile" href="/user/<?=$user->uid?>/edit"><img class="user-pic" src="<?=($uri)?image_style_url('current_user_pix', $uri):'/sites/default/files/styles/current_user_pix/public/pictures/sampledp.png' ?>"></a>
                <?php else:?>
                <a class="edit-profile colorbox-inline" href="?width=280&height=100&inline=true#popup-div-user-edit"><img class="user-pic" src="<?=($uri)?image_style_url('current_user_pix', $uri):'/sites/default/files/styles/current_user_pix/public/pictures/sampledp.png' ?>"></a>
                <?php endif;?>
                <div style="display: none"><div class="popup-div" id="popup-div-user-edit">Only registered user can edit profile</div></div>
                <?php
                    $elements = drupal_get_form("search_block_form");
                    $form = drupal_render($elements);
                ?>
                <div id="search-form"><?php echo $form;?></div>
<!---
                <form action="" method="post" class="search-top">
                    <input type="search" name="" placeholder="Search" class="input" />
                    <input type="submit" name="" value="" class="submit" />
                </form>
                -->
                <?php if($user->uid > 0) {?><a href="/node/add/posts" class="add-post"></a><?php } else{?>
                    <a href="?width=280&height=100&inline=true#popup-div" class="add-post colorbox-inline"></a>
                <?php }?>
                <div style="display: none"><div class="popup-div" id="popup-div">Only registered user can make a post</div></div>
            </div>
            <div class="header-logo">
                <div class="underline"></div>
                <div class="logo-image"></div>
                <div class="circle-logo"><img src="/<?=path_to_theme()?>/css/images/icon_home.png"></div>
            </div>
        </header>
        <?php $block = module_invoke('views', 'block_view', 'mobilemainpage-block'); echo render($block['content']); ?>
<script>
    function spu_createCookie(name, value, hours)
    {
        if (hours)
        {
            var date = new Date();
            date.setTime(date.getTime()+(hours*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else
        {
            var expires = "";
        }

        document.cookie = name+"="+value+expires+"; path=/";
    }
</script>
        <footer class="main-footer">
            <section class="main-footer-inner clearfix">
                <article class="footer-user-navigation">
                    <button type="button" class="user-navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <nav id="user-navigtion" class="user-navigation" role="navigation" style="display:none;">
                        <a class="menu-item" href="/">home</a>
                        <?php if($user->uid > 0):?>
                            <a class="menu-item" href="/current-posts">my posts</a>
                            <a class="menu-item" href="/my-favourites">my favorites</a>
                        <?php endif;?>
                        <a class="menu-item" href="/most-popular">most popular</a>
                        <a class="menu-item" href="/taxonomy/term/rotten-customers">rotten customers</a>
                        <a class="menu-item" href="/taxonomy/term/bullies-unmasked">bullies unmasked</a>
                        <a class="menu-item" href="/taxonomy/term/bosses-co-workers">bosses/co-worcers</a>
                        <a class="menu-item" href="/taxonomy/term/random-wtf">random/wtf?</a>
                        <?php if($user->uid > 0):?>
                            <a class="menu-item" href="/user/logout">logout</a>
                        <?php else:?>
                            <a class="menu-item" href="#" onclick="spu_createCookie('was_accept_tos', 0, -1); location.reload(); return false;" >logout</a>
                        <?php endif;?>

                    </nav>
                </article>
                <article class="footer-friends">
                    <?php if($user->uid > 0):?>
                        <a href="/user/<?=$user->uid?>/friends/flaggedphone" class="friends"><img src="/<?=path_to_theme()?>/css/images/icon_friends.png"></a>
                        <div class="friends-news-counter">20</div>
                    <?php else:?>
                        <a href="?width=280&height=100&inline=true#popup-div-see-friends" class="friends colorbox-inline"><img src="/<?=path_to_theme()?>/css/images/icon_friends.png"></a>
                    <?php endif?>
                </article>
                <div style="display: none"><div class="popup-div" id="popup-div-see-friends">Only registered user can invite friends</div></div>

                <?php if($user->uid > 0):?>
                    <article class="footer-creopied-by">
                        <a href="/node/712" class="creopied-by"><img src="/<?=path_to_theme()?>/css/images/icon_creopled_by.png"></a>
                    </article>
                <?php else:?>
                    <article class="footer-creopied-by">
                        <a href="#" onclick="return false;" class="creopied-by"><img src="/<?=path_to_theme()?>/css/images/icon_creopled_by.png"></a>
                    </article>
                <?php endif;?>

                <article class="footer-shopping-cart">
                    <a class="shopping-cart" target="_blank" href="http://creoples.com"><img src="/<?=path_to_theme()?>/css/images/shopping_cart.png"></a>
                </article>
                <article class="footer-navigation">
                    <button type="button" class="navbar-toggle">
                        <img src="/<?=path_to_theme()?>/css/images/info-menu.png">
                    </button>
                    <nav id="main-navigtion" role="navigation" class='footer-main-navigation' style="display:none;">
                        <a class="menu-item" href="<?php print $base_url;?>/about-us">What is Creople</a>
                        <a class="menu-item" href="<?php print $base_url;?>/terms-of-service">Terms of Service</a>
                        <a class="menu-item" href="<?php print $base_url;?>/privacy-policy">Privacy Policy</a>
                        <a class="menu-item" href="<?php print $base_url;?>/faq">FAQ</a>
                        <a class="menu-item" href="mailto:contact@creople.com">Contact Us</a>
                        <a class="menu-item" href="<?php print $base_url;?>/contact">Make Creople Better</a>
                        <!--<a class="menu-item" href="http://creoples.com" target="_blank">Shop Creople</a>-->
                    </nav>
                </article>
                <div class="clearfix"></div>
        </footer>

    <?php endif; ?>



<?php  } ?>
