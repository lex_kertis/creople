<?php /** * @file * Default simple view template to display a list of rows. * * @ingroup views_templates */ ?> 
<?php if (!empty($title)): ?> <h3><?php print $title; ?></h3> <?php endif; ?> <?php foreach ($rows as $id => $row): ?> 
<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"'; } ?>> <?php print $row; ?> </div> 

 <?php if ($id%2==0): ?>
<div class="hidden-lg hidden-md">
<?php
$block = block_load('simpleads', 'content_ads');
print render(_block_get_renderable_array(_block_render_blocks(array($block))));
?>
</div>
  <?php endif; ?>
  
<?php endforeach; ?>