<!DOCTYPE html>
<html>
  <head>
	  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/<?=path_to_theme()?>/css/images/save_for_desktop_ico.png" />
	  <meta name="apple-mobile-web-app-title" content="Creople">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<?php print $head; ?>
		<title><?php print $head_title; ?></title>
		<?php //print $styles; ?>
		<?php print $scripts; ?>
	  <link type="text/css" rel="stylesheet" href="http://creople.com/sites/all/modules/colorbox/styles/default/colorbox_style.css" media="all" />
	  <link type="text/css" rel="stylesheet" href="http://creople.com/sites/all/modules/colorbox_node/colorbox_node.css" media="all" />

	  <link type="text/css" rel="stylesheet" href="/<?=path_to_theme()?>/css/style.css" media="all" />
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	  <script src="/<?=path_to_theme()?>/js/main.js"></script>
	</head>
	<body class="<?php print $classes; ?>" <?php print $attributes;?>>
	<?php $block = module_invoke('views', 'block_view', 'friends-block_2');?>
	<div id="friends-count" style="display: none"><?php echo render($block['content']); ?></div>
		<?php print $page_top; ?>
		<?php print $page; ?>
		<?php print $page_bottom; ?>
<?php if(isset($_SESSION['popup_alert'])):?>
<div class="popup_message"><?php print $_SESSION['text_popup'];?></div>
<?php unset($_SESSION['popup_alert']); unset($_SESSION['text_popup']);?>
<script>
setTimeout(function() {
    jQuery('.popup_message').fadeOut('slow');
    }, 3500);
</script>
<?php endif; ?>
	</body>
</html>
