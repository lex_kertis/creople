<style>
    .content {
        margin: 0;
        padding: 0;
    }
    #block-search-form {
        display: none;
    }
</style>

<?php $path = drupal_get_path_alias($_GET["q"]);?>
<?php if($path == 'node/' . $node->nid .'/edit'):?>

    <script src="/<?=path_to_theme()?>/js/post-edit.js"></script>

    <header class="main-header">
        <section class="header-edit-block clearfix">
            <a class="edit-cancel" href="javascript:history.back()">Cancel</a><div class="page-title">Edit a post</div><a class="edit-ok"></a>
        </section>
    </header>

    <section id="edit-post-form" class="main-content" role="content">
        <article class="edit-item clearfix post-make-anonimus">
            <div>
                <input type="checkbox" id="edit-field-anonymous-und" name="field_anonymous[und]" value="1" class="form-checkbox">
                <label class="option" for="edit-field-anonymous-und">Anonymous <span></span></label>
            </div>
        </article>
        <article class="edit-item clearfix post-add-category">
            <label for="edit-field-categories-und">Category</label>
            <select id="edit-field-categories-und" name="field_categories[und]" class="form-select required">
                <option value="_none">Select a category</option>
                <option value="3">Rotten Customers</option>
                <option value="4">Bullies Unmasked</option>
                <option value="5">Bosses / Co-Workers</option>
                <option value="6">Lousy Teachers / Profs</option>
                <option value="7">Random / WTF?</option>
            </select>
        </article>
        <article class="edit-item clearfix post-smeared">
            <label for="edit-field-smeared-on-und-select">Smeared on </label>
            <select class="select-or-other-select form-select" id="edit-field-smeared-on-und-select" name="field_smeared_on[und][select]">
                <option value="_none">None</option>
                <option value="Yelp®">Yelp®</option>
                <option value="Google®">Google®</option>
                <option value="Yahoo®">Yahoo®</option>
                <option value="Angie's List®">Angie's List®</option>
                <option value="select_or_other">Other</option>
            </select>
        </article>
        <article class="edit-item clearfix post-title">
            <input class="maxlength form-text required maxlength-processed" type="text" id="edit-title" name="title" value="" size="60" maxlength="140" placeholder="Who or what is it about?">
        </article>
        <article class="edit-item clearfix post-text">
            <textarea class="text-full form-textarea" id="edit-body-und-0-value" name="body[und][0][value]" cols="60" rows="5" placeholder="Your creople story..."></textarea>
        </article>
        <article class="edit-item clearfix">
            <label>Attachment (Max size: 12 Mb)</label>
            <section class="attachment">
                <article id="video" class="inactive">
                    <span></span>
                    <div>
                        <!--<input type="file" id="edit-field-upload-video-und-0-upload--3" name="files[field_upload_video_und_0]" size="22" class="form-file">
                        <input type="submit" id="edit-field-upload-video-und-0-upload-button--3" name="field_upload_video_und_0_upload_button" value="Upload" class="form-submit ajax-processed">-->
                    </div>
                </article>
                <article id="picture" class="inactive">
                    <span></span>
                    <div>
                        <input type="file" id="edit-field-upload-media-und-0-upload" name="files[field_upload_media_und_0]" size="22" class="form-file">
                        <input type="submit" id="edit-field-upload-media-und-0-upload-button" name="field_upload_media_und_0_upload_button" value="Upload" class="form-submit ajax-processed">
                    </div>
                </article>
                <article id="audio" class="inactive">
                    <span></span>
                    <div>
                        <input type="file" id="edit-field-upload-audio-und-0-upload" name="files[field_upload_audio_und_0]" size="22" class="form-file">
                        <input type="submit" id="edit-field-upload-audio-und-0-upload-button" name="field_upload_audio_und_0_upload_button" value="Upload" class="form-submit ajax-processed">
                    </div>
                </article>
                <article id="none" class="active">
                    <span>None</span>
                </article>
                <div class="uploaded-files">
                    <?php if(isset($node->field_upload_media['und'][0]['uri'])):?>
                        <div class="field-type-file custom">
                            <img src="<?=image_style_url('postimage', $node->field_upload_media['und'][0]['uri'])?>" />
                        </div>
                    <?php endif;?>
                    <?
                    print render($node->field_upload_video);
                    print render($node->field_upload_audio);
                    ?>
                </div>
            </section>
        </article>
        <article class="edit-item clearfix post-name-place">
            <div>
                <input type="text" id="name-of-place" name="name-of-place" value="" size="17" class="form-text" placeholder="Name of Place">
            </div>
            <a href="javascript:void(0);" class="map-mark" onclick="getaddr()"></a>

        </article>
        <article class="edit-item clearfix post-street">
            <input type="text" id="edit-field-address-und-0-street" name="field_address[und][0][street]" value="" size="64" maxlength="255" class="form-text" placeholder="Street">
        </article>
        <article class="edit-item clearfix post-city">
            <input type="text" id="edit-field-address-und-0-city" name="field_address[und][0][city]" value="" size="64" maxlength="255" class="form-text" placeholder="City">
        </article>
        <article class="edit-item clearfix post-postal-code">
            <input type="text" id="edit-field-address-und-0-postal-code" name="field_address[und][0][postal_code]" value="" size="16" maxlength="16" class="form-text" placeholder="ZIP Code">
        </article>
        <article class="edit-item clearfix post-state">
            <label for="edit-field-address-und-0-province">State/Province </label>
            <input class="location_auto_province form-text form-autocomplete" type="text" id="edit-field-address-und-0-province" name="field_address[und][0][province]" value="" size="64" maxlength="64" autocomplete="OFF" aria-autocomplete="list" placeholder="Enter State">
        </article>
        <article class="edit-item clearfix post-country">
            <label for="edit-field-address-und-0-country">Country </label>
            <select class="location_auto_country form-select ajax-processed location-processed" id="edit-field-address-und-0-country" name="field_address[und][0][country]">
                <option value="">Select</option><option value="xx">NOT LISTED</option><option value="af">Afghanistan</option><option value="ax">Aland Islands</option><option value="al">Albania</option><option value="dz">Algeria</option><option value="as">American Samoa</option><option value="ad">Andorra</option><option value="ao">Angola</option><option value="ai">Anguilla</option><option value="aq">Antarctica</option><option value="ag">Antigua and Barbuda</option><option value="ar">Argentina</option><option value="am">Armenia</option><option value="aw">Aruba</option><option value="au">Australia</option><option value="at">Austria</option><option value="az">Azerbaijan</option><option value="bs">Bahamas</option><option value="bh">Bahrain</option><option value="bd">Bangladesh</option><option value="bb">Barbados</option><option value="by">Belarus</option><option value="be">Belgium</option><option value="bz">Belize</option><option value="bj">Benin</option><option value="bm">Bermuda</option><option value="bt">Bhutan</option><option value="bo">Bolivia</option><option value="ba">Bosnia and Herzegovina</option><option value="bw">Botswana</option><option value="bv">Bouvet Island</option><option value="br">Brazil</option><option value="io">British Indian Ocean Territory</option><option value="vg">British Virgin Islands</option><option value="bn">Brunei</option><option value="bg">Bulgaria</option><option value="bf">Burkina Faso</option><option value="bi">Burundi</option><option value="kh">Cambodia</option><option value="cm">Cameroon</option><option value="ca">Canada</option><option value="cv">Cape Verde</option><option value="bq">Caribbean Netherlands</option><option value="ky">Cayman Islands</option><option value="cf">Central African Republic</option><option value="td">Chad</option><option value="cl">Chile</option><option value="cn">China</option><option value="cx">Christmas Island</option><option value="cc">Cocos (Keeling) Islands</option><option value="co">Colombia</option><option value="km">Comoros</option><option value="cg">Congo (Brazzaville)</option><option value="cd">Congo (Kinshasa)</option><option value="ck">Cook Islands</option><option value="cr">Costa Rica</option><option value="hr">Croatia</option><option value="cu">Cuba</option><option value="cw">Curaçao</option><option value="cy">Cyprus</option><option value="cz">Czech Republic</option><option value="dk">Denmark</option><option value="dj">Djibouti</option><option value="dm">Dominica</option><option value="do">Dominican Republic</option><option value="ec">Ecuador</option><option value="eg">Egypt</option><option value="sv">El Salvador</option><option value="gq">Equatorial Guinea</option><option value="er">Eritrea</option><option value="ee">Estonia</option><option value="et">Ethiopia</option><option value="fk">Falkland Islands</option><option value="fo">Faroe Islands</option><option value="fj">Fiji</option><option value="fi">Finland</option><option value="fr">France</option><option value="gf">French Guiana</option><option value="pf">French Polynesia</option><option value="tf">French Southern Territories</option><option value="ga">Gabon</option><option value="gm">Gambia</option><option value="ge">Georgia</option><option value="de">Germany</option><option value="gh">Ghana</option><option value="gi">Gibraltar</option><option value="gr">Greece</option><option value="gl">Greenland</option><option value="gd">Grenada</option><option value="gp">Guadeloupe</option><option value="gu">Guam</option><option value="gt">Guatemala</option><option value="gg">Guernsey</option><option value="gn">Guinea</option><option value="gw">Guinea-Bissau</option><option value="gy">Guyana</option><option value="ht">Haiti</option><option value="hm">Heard Island and McDonald Islands</option><option value="hn">Honduras</option><option value="hk">Hong Kong S.A.R., China</option><option value="hu">Hungary</option><option value="is">Iceland</option><option value="in">India</option><option value="id">Indonesia</option><option value="ir">Iran</option><option value="iq">Iraq</option><option value="ie">Ireland</option><option value="im">Isle of Man</option><option value="il">Israel</option><option value="it">Italy</option><option value="ci">Ivory Coast</option><option value="jm">Jamaica</option><option value="jp">Japan</option><option value="je">Jersey</option><option value="jo">Jordan</option><option value="kz">Kazakhstan</option><option value="ke">Kenya</option><option value="ki">Kiribati</option><option value="kw">Kuwait</option><option value="kg">Kyrgyzstan</option><option value="la">Laos</option><option value="lv">Latvia</option><option value="lb">Lebanon</option><option value="ls">Lesotho</option><option value="lr">Liberia</option><option value="ly">Libya</option><option value="li">Liechtenstein</option><option value="lt">Lithuania</option><option value="lu">Luxembourg</option><option value="mo">Macao S.A.R., China</option><option value="mk">Macedonia</option><option value="mg">Madagascar</option><option value="mw">Malawi</option><option value="my">Malaysia</option><option value="mv">Maldives</option><option value="ml">Mali</option><option value="mt">Malta</option><option value="mh">Marshall Islands</option><option value="mq">Martinique</option><option value="mr">Mauritania</option><option value="mu">Mauritius</option><option value="yt">Mayotte</option><option value="mx">Mexico</option><option value="fm">Micronesia</option><option value="md">Moldova</option><option value="mc">Monaco</option><option value="mn">Mongolia</option><option value="me">Montenegro</option><option value="ms">Montserrat</option><option value="ma">Morocco</option><option value="mz">Mozambique</option><option value="mm">Myanmar</option><option value="na">Namibia</option><option value="nr">Nauru</option><option value="np">Nepal</option><option value="nl">Netherlands</option><option value="an">Netherlands Antilles</option><option value="nc">New Caledonia</option><option value="nz">New Zealand</option><option value="ni">Nicaragua</option><option value="ne">Niger</option><option value="ng">Nigeria</option><option value="nu">Niue</option><option value="nf">Norfolk Island</option><option value="mp">Northern Mariana Islands</option><option value="kp">North Korea</option><option value="no">Norway</option><option value="om">Oman</option><option value="pk">Pakistan</option><option value="pw">Palau</option><option value="ps">Palestinian Territory</option><option value="pa">Panama</option><option value="pg">Papua New Guinea</option><option value="py">Paraguay</option><option value="pe">Peru</option><option value="ph">Philippines</option><option value="pn">Pitcairn</option><option value="pl">Poland</option><option value="pt">Portugal</option><option value="pr">Puerto Rico</option><option value="qa">Qatar</option><option value="re">Reunion</option><option value="ro">Romania</option><option value="ru">Russia</option><option value="rw">Rwanda</option><option value="bl">Saint Barthélemy</option><option value="sh">Saint Helena</option><option value="kn">Saint Kitts and Nevis</option><option value="lc">Saint Lucia</option><option value="mf">Saint Martin (French part)</option><option value="pm">Saint Pierre and Miquelon</option><option value="vc">Saint Vincent and the Grenadines</option><option value="ws">Samoa</option><option value="sm">San Marino</option><option value="st">Sao Tome and Principe</option><option value="sa">Saudi Arabia</option><option value="sn">Senegal</option><option value="rs">Serbia</option><option value="sc">Seychelles</option><option value="sl">Sierra Leone</option><option value="sg">Singapore</option><option value="sx">Sint Maarten</option><option value="sk">Slovakia</option><option value="si">Slovenia</option><option value="sb">Solomon Islands</option><option value="so">Somalia</option><option value="za">South Africa</option><option value="gs">South Georgia and the South Sandwich Islands</option><option value="kr">South Korea</option><option value="ss">South Sudan</option><option value="es">Spain</option><option value="lk">Sri Lanka</option><option value="sd">Sudan</option><option value="sr">Suriname</option><option value="sj">Svalbard and Jan Mayen</option><option value="sz">Swaziland</option><option value="se">Sweden</option><option value="ch">Switzerland</option><option value="sy">Syria</option><option value="tw">Taiwan</option><option value="tj">Tajikistan</option><option value="tz">Tanzania</option><option value="th">Thailand</option><option value="tl">Timor-Leste</option><option value="tg">Togo</option><option value="tk">Tokelau</option><option value="to">Tonga</option><option value="tt">Trinidad and Tobago</option><option value="tn">Tunisia</option><option value="tr">Turkey</option><option value="tm">Turkmenistan</option><option value="tc">Turks and Caicos Islands</option><option value="tv">Tuvalu</option><option value="vi">U.S. Virgin Islands</option><option value="ug">Uganda</option><option value="ua">Ukraine</option><option value="ae">United Arab Emirates</option><option value="gb">United Kingdom</option><option value="us" selected="selected">United States</option><option value="um">United States Minor Outlying Islands</option><option value="uy">Uruguay</option><option value="uz">Uzbekistan</option><option value="vu">Vanuatu</option><option value="va">Vatican</option><option value="ve">Venezuela</option><option value="vn">Vietnam</option><option value="wf">Wallis and Futuna</option><option value="eh">Western Sahara</option><option value="ye">Yemen</option><option value="zm">Zambia</option><option value="zw">Zimbabwe</option></select>
        </article>
    </section>
    <?php
    /* global $user;
     module_load_include('inc', 'node', 'node.pages');
     $node = (object) array('uid' => $user->uid, 'name' => (isset($user->name) ? $user->name : ''), 'type' => 'posts', 'language' => LANGUAGE_NONE);

     $elements = drupal_get_form("posts_node_form", $node);
     $form = drupal_render($elements);
     print $form;*/

    ?>
    <?php print render($page['content']); ?>
    <footer class="main-footer">
    </footer>
<?php else:?>
<?php if($_SESSION['shared'] == true):?>
	<script>
	 jQuery(document).ready(function(){
	   alert('This post has been shared with your friends');
	 jQuery('.shared-click').trigger('click');;
	});
	</script>
<a id= "shared-click" href="?width=280&height=100&inline=true#popup-div" class="colorbox-inline" style="display:none"></a>
<div style="display: none"><div class="popup-div" id="popup-div">This post has been shared with your friends</div></div>
<?php $_SESSION['shared'] = false;?>

<?php endif;?>

<?php print render($page['content']); ?>
<?php endif;?>
