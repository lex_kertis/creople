<style>
  .tabs,.search-form {
    display: none;;
  }
  section.main-content{
    padding-top: 0!important;
  }
</style>
<script>
  $(document).ready(function(){
    $('.map-it-button').attr('href', $('#map-search').attr('href'))
  });
</script>

<?php if ($search_results): ?>
<script>

</script>
<?php $src = '/map/';?>
<?php foreach($all_nids as $n):?>
 <?php $src .= $n . ',';?>
<?php endforeach;?>
    <?php $src =  substr($src, 0, -1);?>
<!--<span style="display:none;" id="is_load_map">1</span>-->
  <a id="map-search" href="http://creople.com<?=$src?>" target="_blank" style="display: none;"></a>
<!--<div id="search-map-buffer" style="height:0; overflow:hidden;"><iframe width="100%" height="500px" src="<?=$src?>?popup=1" ></iframe></div>-->
  <ul id="search-result-ul" class="search-results <?php print $module; ?>-results">
    <li><h2 class="category-title"><span>search result</span></h2></li>
    <?php print $search_results; ?>
  </ul>
  <?php print $pager; ?>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
  <?php print search_help('search#noresults', drupal_help_arg()); ?>
<?php endif; ?>

<footer class="main-footer">
  <section class="main-footer-inner clearfix">
    <article class="footer-user-navigation">
      <button type="button" class="user-navbar-toggle">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <nav id="user-navigtion" class="user-navigation" role="navigation" style="display:none;">
        <a class="menu-item" href="/">home</a>
        <a class="menu-item" href="#">my posts</a>
        <a class="menu-item" href="/my-favourites">my favorites</a>
        <a class="menu-item" href="/most-popular">most popular</a>
        <a class="menu-item" href="/taxonomy/term/rotten-customers">rotten customers</a>
        <a class="menu-item" href="/taxonomy/term/bullies-unmasked">bullies unmasked</a>
        <a class="menu-item" href="/taxonomy/term/bosses-co-workers">bosses/co-worcers</a>
        <a class="menu-item" href="/taxonomy/term/random-wtf">random/wtf?</a>
        <a class="menu-item" href="/user/logout">logout</a>
      </nav>
    </article>
    <article class="footer-friends">
      <a href="/user/<?=$user->uid?>/friends/flagged" class="friends"><img src="/<?=path_to_theme()?>/css/images/icon_friends.png"></a>
      <div class="friends-news-counter">20</div>
    </article>
    <article class="footer-creopied-by">
      <a href="/node/712" class="creopied-by"><img src="/<?=path_to_theme()?>/css/images/icon_creopled_by.png"></a>
    </article>
    <article class="footer-shopping-cart">
      <a class="shopping-cart" target="_blank" href="http://creoples.com"><img src="/<?=path_to_theme()?>/css/images/shopping_cart.png"></a>
    </article>
    <article class="footer-navigation">
      <button type="button" class="navbar-toggle">
        <img src="/<?=path_to_theme()?>/css/images/info-menu.png">
      </button>
      <nav id="main-navigtion" role="navigation" class='footer-main-navigation' style="display:none;">
        <a class="menu-item" href="<?php print $base_url;?>/about-us">What is Creople</a>
        <a class="menu-item" href="<?php print $base_url;?>/terms-of-service">Terms of Service</a>
        <a class="menu-item" href="<?php print $base_url;?>/privacy-policy">Privacy Policy</a>
        <a class="menu-item" href="<?php print $base_url;?>/faq">FAQ</a>
        <a class="menu-item" href="mailto:contact@creople.com">Contact Us</a>
        <a class="menu-item" href="<?php print $base_url;?>/contact">Make Creople Better</a>
        <a class="menu-item" href="http://creoples.com" target="_blank">Shop Creople</a>
      </nav>
    </article>
    <div class="clearfix"></div>
</footer>
