<?php
  $u = user_load($account->uid);
  if (isset($u->field_profile_picture['und'][0]['filename'])) {
    $picture = file_create_url('sites/default/files/'.$u->field_profile_picture['und'][0]['filename']);
    print $picture;
  }
