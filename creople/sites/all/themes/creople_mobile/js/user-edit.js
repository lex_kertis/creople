function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {
            continue;
        }
        var img=document.createElement('img');
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
        var block=document.createElement('div');
        block.className='image-preview';
        $(block).prepend(img);
        $('.image-widget').prepend(block);
    }
}
jQuery(function($){


    $(document).ready(function(){

        $('#user-profile-form .group-picture .image-widget-data input[type="file"]').attr('accept', 'image/*');
        $('#user-profile-form .group-picture .image-widget-data input[type="file"]').attr('capture', 'camera');
        $('#user-profile-form .group-picture .image-widget-data input[type="file"]').attr('onchange', 'showMyImage(this)');


        $('#edit-user-form .user-login').html('').append($('#edit-account .form-item-name').remove() );
        $('#edit-user-form .user-name').html('').append($('.field-name-field-name').remove() );
        $('#edit-user-form .my-likes').html('').append($('.field-name-field-likes').remove() );
        $('#edit-user-form .my-dislikes').html('').append($('.field-name-field-dislikes').remove() );
        $('#edit-user-form .email-address').html('').append($('.form-item-mail').remove() );
        $('#edit-user-form .email-notificate').html('').append($('.form-item-field-notification-und').remove() );
        $('#edit-user-form .phone-number').html('').append($('.field-name-field-phone-number').remove() );
        $('#edit-user-form .day-birth').html('').append($('.field-name-field-date-of-birth').remove() );
        $('#edit-user-form .user-avatar-div').html('').append($('.group-picture').remove() );
        $('#edit-user-form .passwords-form').html('').append($('#edit-account').remove() );


        $('#user-profile-form').prepend($('#edit-user-form').remove() );

        /* if($('#user-profile-form .group-picture .image-widget img').length < 1) {

         var image=document.createElement('img');
         image.src='htpp://creople.com/sites/all/themes/creople_mobile/css/images/default_avatar.png"';

         $('#user-profile-form .group-picture .image-widget').prepend($(image));
         }*/

        // $('#edit-user-form .user-name').append($('.field-name-field-name').remove() );

        $('#edit-user-form .my-likes textarea').attr('placeholder', 'I like...');
        $('#edit-user-form .my-dislikes textarea').attr('placeholder', 'I dislike...');


        if($('#user-profile-form .field-name-field-date-of-birth input').attr('type') == 'text' && $('#user-profile-form .field-name-field-date-of-birth input').val().indexOf('/') >= 0) {

            console.log($('#user-profile-form .field-name-field-date-of-birth input').val() + "  " + $('#user-profile-form .field-name-field-date-of-birth input').attr('type'));
            var from = $('#user-profile-form .field-name-field-date-of-birth input').val().split("/");
            $('#user-profile-form .field-name-field-date-of-birth input').prop('type', 'date');
            $('#user-profile-form .field-name-field-date-of-birth input').val(from[2] + '-' + from[0] + '-' + from[1]);

        }

        $('#user-profile-form #edit-actions input#edit-submit').on('click', function(){

                var from = $('#user-profile-form .field-name-field-date-of-birth input').val().split("-");
                $('#user-profile-form .field-name-field-date-of-birth input').prop('type', 'text');
                $('#user-profile-form .field-name-field-date-of-birth input').val(from[1] + '/' + from[2] + '/' + from[0]);
                console.log($('#user-profile-form .field-name-field-date-of-birth input').val());
        });



    });

});
