jQuery(function($){


	$(document).ready(function(){
	  $('.navbar-toggle').on('click', function(){
	    $('#main-navigtion').toggle();
	  });
	  $('.user-navbar-toggle').on('click', function(){
	    $('#user-navigtion').toggle();
	  });
	   $('.share-action').on('click', function(){
	    $('#share-menu').toggle();
	  });

	  var headerHeight = $('.main-header').height() + 22;
	  $('.main-content').css("padding-top",headerHeight + "px");
	  var footerHeight = $('.main-footer').height();
	  $('.main-content').css("padding-bottom",footerHeight + "px");

	 /* $(".attachment article").on('click', function(){
	  	$('.attachment article').removeClass("active");
	  	$('.attachment article').addClass("inactive");
	  	$(this).removeClass("inactive");
	  	$(this).addClass("active");
	  });
*/

	  $(".friends-tab-name span").on("click",function(){
	  	$(".friends-tab-name span").removeClass("active-th");
	  	$('.friends-tab-name span').addClass("inactive-th");
	  	$('.friend-tab-item.active-frti').removeClass("active-frti");
	  	$('.friend-tab-item').addClass("inactive-frti");

	  	$(this).removeClass("inactive-th");
	  	$(this).addClass("active-th");
	  	var fortab = $(this).attr("for");
	  	$('.friends-tab').find('#' + fortab).addClass("active-frti").removeClass("inactive-frti");
	  });

	  $("a.favorite").on("click",function(){
		  $('li.flag-favourite a').trigger('click');
		  $(this).toggleClass("unfiled");
		  $(this).toggleClass("filed");

	  });

		$('.jump-in-add-comment .form-type-textarea label').remove();
		$('.jump-in-add-comment .form-type-textarea textarea').attr('placeholder', 'Jump in here');

		$('#invite-form .form-type-textfield label').remove();
		$('#invite-form .form-type-textfield .description').remove();
		$('#invite-form .form-type-textfield input').attr('placeholder', 'Type your friend\'s Email');
		if($('#friends-count .view-footer').length) {
			$('.friends-news-counter').html($('#friends-count .view-footer').html().replace(/\s+/g, ''));
		} else {
			$('.friends-news-counter').remove();
		}
	});
	
});


// addres on makepost page
function getaddr(){
	if (navigator.geolocation) {
 		var timeoutVal = 10 * 1000 * 1000;
 		navigator.geolocation.getCurrentPosition(
  			changePosition,
  			displayError,
  			{ enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
 		);
	}
	else {
 		alert("Geolocation is not supported by your browser");
	}
}
function displayError(error) {
 	var errors = {
  		1: 'Please turn on browser location services in your device settings',
  		2: "can't get location",
  		3: 'connection timeout'
 	};
 	alert("Error: " + errors[error.code]);
}
function changePosition(position) {
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	jQuery.ajax({
		type: "GET",
  		url: 'http://maps.googleapis.com/maps/api/geocode/json?',
  		data: "latlng="+latitude+","+longitude+"&language=en",
  		//data:"latlng=30.192476,-98.085541&language=en", //test coords for East Mercer Street, 78620, Texas, US
  		dataType:"json",
  		success: function(data){
console.log(data);
    		var getStreet;
    		var getCity;
    		var getZip;
    		var getState;    		
    		var getCountry;
                var getBuilding;
    		for (var i = 0; i < data.results[0].address_components.length; i++) {
    			var addr = data.results[0].address_components[i];
        		
        		if (addr.types[0] == 'route'){
          			getStreet = addr.long_name;
          		}
        		if (addr.types[0] == 'street_number'){
          			getBuilding = addr.long_name;
          		}
          		if (addr.types[0] == 'locality'){
          			getCity = addr.short_name;
          		}
          		if (addr.types[0] == 'postal_code'){
          			getZip = addr.short_name;
          		}
          		if (addr.types[0] == 'administrative_area_level_1'){
          			getState = addr.long_name;
          		}
          		if (addr.types[0] == 'country'){
          			getCountry = addr.short_name.toLowerCase();
          		}
    		}
    		autoPasteAddr(getStreet, getCity, getZip, getState, getCountry, getBuilding);	
  		}
	});
}
function autoPasteAddr(street, city, zipCode, state, country, getBuilding){
	jQuery(function($){
		$('#edit-field-address-und-0-street').val(street + ', ' + getBuilding);
		$('#edit-field-address-und-0-city').val(city);
		$('#edit-field-address-und-0-postal-code').val(zipCode);
		$('#edit-field-address-und-0-province').val(state);
		$('#edit-field-address-und-0-country').attr('data-select', country);

                if(country == 'us') {
var html_state = '<select id="state-cheat"><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option></select>';
          		var elem_div = document.createElement('div');
          		$(elem_div).html(html_state);
			  if($('#state-cheat').length === 0) {
			    $('.form-item-field-address-und-0-province input[type="text"]').after(elem_div).hide();
                            $('#state-cheat').val(state);
			    $('body').on('change', '#state-cheat', function(){
				      $('.form-item-field-address-und-0-province input[type="text"]').val($(this).val());
			    });
			  }
		} else {
			$('#state-cheat').parent().remove();
			$('.form-item-field-address-und-0-province input[type="text"]').show();
		}

		$('#edit-field-address-und-0-country  option').each(function(){
			if($(this).val() != country){
				$(this).removeAttr('selected');
			}
			if(($(this).val() == country)){
				$(this).attr('selected' , 'selected');
			}
		});
	});
}
