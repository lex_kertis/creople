$(document).ready(function(){
  $('.post-desc-actions .link-more').click(function(){
    $(this).parent().parent().find('.full-desc').show();
    $(this).parent().parent().find('.short-desc').hide();
    $(this).parent().find('.link-hide').show();
    $(this).hide();
  });
  $('.post-desc-actions .link-hide').click(function(){
    $(this).parent().parent().find('.full-desc').hide();
    $(this).parent().parent().find('.short-desc').show();
    $(this).parent().find('.link-more').show();
    $(this).hide();
  });
});
