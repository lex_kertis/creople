jQuery(function($){
    $(document).ready(function(){
        $('#user-register-form .field-name-field-date-of-birth input').attr('type', 'date');

        $('#user-register-form .field-name-field-name input').attr('placeholder', 'Type your name here...');
        $('#user-register-form .form-item-mail input').attr('placeholder', 'Type your email here...');
        $('#user-register-form .form-item-name input').attr('placeholder', 'Type your username here...');
        $('#user-register-form .password-parent input').attr('placeholder', 'Type your password here...');
        $('#user-register-form .confirm-parent input').attr('placeholder', 'Repeat your password here...');


        $('#user-register-form input#edit-submit').on('click', function(){
            var from = "";
            if($('#user-register-form .field-name-field-date-of-birth input').val()) {
		from = $('#user-register-form .field-name-field-date-of-birth input').val().split("-");
	   	 var d = from[1] + '/' + from[2] + '/' + from[0];	    
            	$('#user-register-form .field-name-field-date-of-birth input').attr('type', 'text');
	    	$('#user-register-form .field-name-field-date-of-birth input').val(d);
            } else {
		$('#user-register-form .field-name-field-date-of-birth input').attr('type', 'text');
            }
            
            
//return false;
            //$('#user-profile-form .field-name-field-phone-number input').attr('type', 'text');
        });

    });

});
