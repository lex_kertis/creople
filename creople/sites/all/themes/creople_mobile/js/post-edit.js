function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match('image.*')) {
            continue;
        }

        var img=document.createElement('img');
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
        var block=document.createElement('div');
        block.className='image-preview';
        $(block).prepend(img);
        $('.uploaded-files').html('').prepend(block);
    }
}

function showMyVideo(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        //var imageType = /video.*/;
        if (!file.type.match('video.*')) {
            continue;
        }
        var img=document.createElement('video');
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
        var block=document.createElement('div');
        block.className='image-preview';
        $(block).prepend(img);
        $('.uploaded-files').html('').prepend(block);
    }
}

function showMyAudio(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        //var imageType = /video.*/;
        if (!file.type.match('audio.*')) {
            continue;
        }
        var img=document.createElement('source');
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
                aImg.type = file.type;
            };
        })(img);
        console.log(file);
        reader.readAsDataURL(file);
        var block=document.createElement('div');
        block.className='image-preview';
        var audio=document.createElement('div');
        audio.className='audio-name';
        $(audio).html(file.name);

        $(block).prepend(audio);
        $('.uploaded-files').html('').prepend(block);
    }
}
jQuery(function($){
    $(document).ready(function(){
        $('#edit-post-form .post-make-anonimus').html('').append($('#posts-node-form .field-name-field-anonymous').remove() );
        $('#edit-post-form .post-add-category').html('').append($('#posts-node-form .field-name-field-categories').remove() );
        $('#edit-post-form .post-smeared').html('').append($('#posts-node-form .field-name-field-smeared-on').remove() );
        $('#edit-post-form .post-title').html('').append($('#posts-node-form .form-item-title').remove() );
        $('#edit-post-form .post-text').html('').append($('#posts-node-form .field-name-body').remove() );

        $('#edit-post-form .post-street').html('').append($('#posts-node-form .form-item-field-address-und-0-street').remove() );
        $('#edit-post-form .post-city').html('').append($('#posts-node-form .form-item-field-address-und-0-city').remove() );
        $('#edit-post-form .post-postal-code').html('').append($('#posts-node-form .form-item-field-address-und-0-postal-code').remove() );
        $('#edit-post-form .post-state').html('').append($('#posts-node-form .form-item-field-address-und-0-province').remove() );
        $('#edit-post-form .post-country').html('').append($('#posts-node-form .form-item-field-address-und-0-country').remove() );
        $('#edit-post-form .post-name-place div').html('').append($('#posts-node-form .form-item-field-address-und-0-name').remove() );

        $('#edit-post-form #video div').html('').append($('#posts-node-form #edit-field-upload-video').remove() );
        $('#edit-post-form #picture div').html('').append($('#posts-node-form #edit-field-upload-media').remove() );
        $('#edit-post-form #audio div').html('').append($('#posts-node-form #edit-field-upload-audio').remove() );
        var html_state = '<select id="state-cheat"><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option></select>';
        if($('#edit-field-address-und-0-country').val() == 'us') {
          var elem_div = document.createElement('div');
          $(elem_div).html(html_state);

          if($('#state-cheat').length === 0) {
            $('.form-item-field-address-und-0-province input[type="text"]').after(elem_div).hide();
		  $('body').on('change', '#state-cheat', function(){
		      $('.form-item-field-address-und-0-province input[type="text"]').val($(this).val());
		   });
          }
          
        }
        
	$('body').on('change', '#edit-field-address-und-0-country', function(){
	   if($('#edit-field-address-und-0-country').val() == 'us') {
          	var elem_div = document.createElement('div');
          	$(elem_div).html(html_state);

		  if($('#state-cheat').length === 0) {
		    $('.form-item-field-address-und-0-province input[type="text"]').after(elem_div).hide();
		    $('body').on('change', '#state-cheat', function(){
			      $('.form-item-field-address-und-0-province input[type="text"]').val($(this).val());
		    });
		  }
           } else {
             $('#state-cheat').parent().remove();
	     $('.form-item-field-address-und-0-province input[type="text"]').show();
           }
	});




        $('#posts-node-form').prepend($('#edit-post-form').remove() );
        $(".attachment article").on('click', function(){
            $('.attachment article.active').removeClass("active").addClass("inactive");
            $(this).removeClass("inactive");
            $(this).addClass("active");
        });

        $('#posts-node-form .post-title input').attr('placeholder', 'Who or what is it about?');
        $('#posts-node-form .post-text textarea').attr('placeholder', 'Your creople story...');
        $('#posts-node-form .post-street input').attr('placeholder', 'Street');
        $('#posts-node-form .post-city input').attr('placeholder', 'City');
        $('#posts-node-form .post-postal-code input').attr('placeholder', 'ZIP Code');
        $('#posts-node-form .post-state input').attr('placeholder', 'Enter State');
        $('#posts-node-form .post-name-place input').attr('placeholder', 'Name of Place');

	$('.attachment article').on('click', function(){
          switch($(this).attr('id')){
            case 'video':
		$('#edit-field-buffer-file input[type="file"]').attr('accept', 'video/*');
		$('#edit-field-buffer-file input[type="file"]').attr('capture', 'camera');
                $('#edit-field-buffer-file input[type="file"]').focus().trigger('click');
            break;
            case 'picture':
		$('#edit-field-buffer-file input[type="file"]').attr('accept', 'image/*');
		$('#edit-field-buffer-file input[type="file"]').attr('capture', 'camera');
                $('#edit-field-buffer-file input[type="file"]').focus().trigger('click');
            break;
            case 'audio':
		$('#edit-field-buffer-file input[type="file"]').attr('accept', 'audio/*');
		$('#edit-field-buffer-file input[type="file"]').attr('capture', 'microphone');
                $('#edit-field-buffer-file input[type="file"]').focus().trigger('click');
            break;
            case 'default':
                $('#edit-field-buffer-file input[type="file"]').reset();
            break;
          }
	  
	});
	$('body').on('change blur', '#edit-field-buffer-file input[type="file"]', function() {
          if(this.files[0].type.match('image.*')) {
		showMyImage(this);
          }
          if(this.files[0].type.match('video.*')) {
		showMyVideo(this);
          }
          if(this.files[0].type.match('audio.*')) {
		showMyAudio(this);
          }
        });

/*
        $('.attachment article').each(function(){
            if($(this).find('#edit-field-upload-media-und-0-remove-button').length > 0 ||$(this).find('#edit-field-upload-audio-und-0-remove-button').length > 0 ||$(this).find('#edit-field-upload-video-und-0-remove-button').length > 0) {
                $('.attachment article.active').removeClass("active").addClass("inactive");
                $(this).removeClass("inactive");
                $(this).addClass("active");
            }

        });

        $('.attachment article span').on('click', function(){
            if($(this).html() == 'None') {

                $('#edit-field-chechbox-for-delete input').prop('checked', true);


                $('.attachment article').each(function(){

                    if($(this).find('.file-size').length > 0) {
                        $(this).find('input[type="submit"]').focus().trigger('click');
                    }

                });
                $('.attachment article.active').removeClass("active").addClass("inactive");
                $(this).parent().removeClass("inactive");
                $(this).parent().addClass("active");
                $('.uploaded-files').html('');
                $('#edit-post-form #video div input[type="file"]').innerHTML = $('#edit-post-form #video div input[type="file"]').innerHTML;
                $('#edit-post-form #picture div input[type="file"]').innerHTML = $('#edit-post-form #picture div input[type="file"]').innerHTML;
                $('#edit-post-form #audio div input[type="file"]').innerHTML = $('#edit-post-form #audio div input[type="file"]').innerHTML;
                return true;
            } else {
                $('#edit-field-chechbox-for-delete input').prop('checked', false);
            }


            if($(this).parent().hasClass('active')) {
                return false;
            }

            $(this).parent().find('input[type="file"]').focus().trigger('click');
            $('.uploaded-files').html('');
            $('#edit-post-form #video div input[type="file"]').innerHTML = $('#edit-post-form #video div input[type="file"]').innerHTML;
            $('#edit-post-form #picture div input[type="file"]').innerHTML = $('#edit-post-form #picture div input[type="file"]').innerHTML;
            $('#edit-post-form #audio div input[type="file"]').innerHTML = $('#edit-post-form #audio div input[type="file"]').innerHTML;
            return false;

        });
        $('#edit-post-form #picture div input[type="file"]').attr('onchange', 'showMyImage(this)');
        $('#edit-post-form #video div input[type="file"]').attr('onchange', 'showMyVideo(this)');
        $('#edit-post-form #audio div input[type="file"]').attr('onchange', 'showMyAudio(this)');

        $('#posts-node-form .field-name-field-upload-media input').attr('accept', 'image/* video/*').hide();
        $('#posts-node-form .field-name-field-upload-media input').attr('capture', 'library');
        $('#posts-node-form .field-name-field-upload-video input').attr('accept', 'image/* video/*').hide();
        $('#posts-node-form .field-name-field-upload-media input').attr('capture', 'camera');
        $('#posts-node-form .field-name-field-upload-audio input').attr('accept', 'audio/*').hide();

        //$('#posts-node-form .field-name-field-upload-audio input').attr('capture', 'microphone');
        //$('#posts-node-form .field-name-field-upload-audio input').attr('capture', '');

*/


        $('#edit-field-categories-und').on('change', function(){
            if($(this).val() == 3) {
                $('#posts-node-form .post-smeared').show();
            } else {
                $('#posts-node-form .post-smeared').hide();
            }
            //
        });
        $('#posts-node-form  #edit-preview').attr('value', 'Post');
        $('#posts-node-form .post-smeared .form-item-field-smeared-on-und-other').addClass('hide');

        $('#edit-field-smeared-on-und-select').on('change', function(){
            if($(this).val() == 'select_or_other') {
                $('#posts-node-form .post-smeared .form-item-field-smeared-on-und-other').removeClass('hide');
            } else {
                $('#posts-node-form .post-smeared .form-item-field-smeared-on-und-other').addClass('hide');
            }
        });


    });

});
