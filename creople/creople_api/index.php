<?php require_once("includes/header.php"); ?>

<?php if (!empty($_GET['API'])) : ?>
    <?php require_once('API/API_' . $_GET['API']); ?>
<?php else : ?>
    <div class="jumbotron">
        <p style="width: 100%"><img src="images/img-logo@2x.png" />
        <h2>Creople API Documentation</h2>
        <p class="lead">Here you will find all API documentation for the Creople Mobile App</p>
        <br>
        <br>
        <p style="width: 100%"><img src="images/iphone5-image.png" />
    </div>
<?php endif; ?>

<?php require_once("includes/footer.php"); ?>