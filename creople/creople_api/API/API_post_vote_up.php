<?php require_once("includes/header.php"); ?>

<h1>Post - Vote Up</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node/[nid]/vote_up.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user vote up on node.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        None
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>up_score</code></td>
                <td><code class="sample">Int</code> - Current Vote Up Score</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node/[nid]/vote_up.json - POST</pre>
        <h5>Response</h5>
        <pre>
{
    "up_score": 2
}
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>