<?php require_once("includes/header.php"); ?>

<h1>User - Profile Picture</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_user/[uid]/attach_file.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when users try to update their profile picture.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>files[profile]</code> <span class="required">*</span></td>
                <td>Profile Image</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns Image Object
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
        The Content-Type of request HEADER should be "<b><u>multipart/form-data</u></b>".
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/5/attach_file.json - POST</pre>
        <h5>Response - 200</h5>
        <pre>
{
    "uid": "213",
    "status": 1,
    "filename": "james1.jpg",
    "uri": "public://james1.jpg",
    "filemime": "image/jpeg",
    "filesize": 28047,
    "source": "profile",
    "destination": "temporary://james1.jpg",
    "timestamp": 1433873679,
    "type": "image",
    "metadata": {
        "width": 300,
        "height": 356
    },
    "fid": "233",
    "url": "http://local.creople.com/sites/default/files/james1.jpg"
}
        </pre>
        <hr>
        <h5>Response - 406 Not Acceptable</h5>
        <pre>
[
    "There is no user with ID 214."
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>