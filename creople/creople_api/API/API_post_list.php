<?php require_once("includes/header.php"); ?>

<h1>Post - List</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node.json</strong></code> - <code class="sample">GET</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user get the list of node.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>page</code></td>
                <td>Page number of results to return (in pages of 20).</td>
            </tr>
            <tr>
                <td><code>pagesize</code></td>
                <td>Integer number of items to be returned.</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns array of Node object.
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>nid</code></td>
                <td><code class="sample">Int</code> - Node ID</td>
            </tr>
            <tr>
                <td><code>title</code></td>
                <td><code class="sample">String</code> - Title</td>
            </tr>
            <tr>
                <td><code>created</code></td>
                <td><code class="sample">Timestamp</code> - Created Date</td>
            </tr>
            <tr>
                <td><code>status</code></td>
                <td><code class="sample">Int</code> - Status</td>
            </tr>
            <tr>
                <td><code>user_name</code></td>
                <td><code class="sample">String</code> - Writer's username</td>
            </tr>
            <tr>
                <td><code>user_fullname</code></td>
                <td><code class="sample">String</code> - Writer's Full Name</td>
            </tr>
            <tr>
                <td><code>user_picture_url</code></td>
                <td><code class="sample">String</code> - Writer's Profile Image</td>
            </tr>
            <tr>
                <td><code>body</code></td>
                <td><code class="sample">String</code> - Node Description</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node.json - GET</pre>
        <h5>Response</h5>
        <pre>
[
    {
        "nid": "260",
        "vid": "260",
        "type": "posts",
        "language": "und",
        "title": "Sally Sitsonme",
        "uid": "0",
        "status": "1",
        "created": "1425953091",
        "changed": "1425953091",
        "comment": "2",
        "promote": "1",
        "sticky": "0",
        "tnid": "0",
        "translate": "0",
        "uri": "http://local.creople.com/mobileapi/node/260",
        "body": "Sally u took my boyfriend and now you can suck his dick as i would never",
        user_picture_url": "http://www.creople.com/sites/default/files/james.jpg",
        "user_name": "somone",
        "user_fullname": "James",
    },
    ...
    {
        ...
    }
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>