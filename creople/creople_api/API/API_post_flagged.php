<?php require_once("includes/header.php"); ?>

<h1>Post - Flagged Posts</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node/flagged_posts.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called while getting flagged posts.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>page</code></td>
                <td>The zero-based index of the page to get, defaults to 0.</td>
            </tr>
            <tr>
                <td><code>pagesize</code></td>
                <td>Number of records to get per page. defaults to 20</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
        None
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>nid</code></td>
                <td><code class="sample">Int</code> - Post ID</td>
            </tr>
            <tr>
                <td><code>node_title</code></td>
                <td><code class="sample">String</code> - Post Title</td>
            </tr>
            <tr>
                <td><code>flag_counts_node_count</code></td>
                <td><code class="sample">Int</code> - Flagged Count</td>
            </tr>
            <tr>
                <td><code>node_status</code></td>
                <td><code class="sample">Int</code> - Status</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node/flagged_posts.json - POST</pre>
        <h5>Response</h5>
        <pre>
[
    {
        "nid": "42",
        "node_title": ".",
        "flag_counts_node_count": "2",
        "node_status": "0"
    },
    {
        "nid": "66",
        "node_title": "test test",
        "flag_counts_node_count": "2",
        "node_status": "0"
    },
    {
        "nid": "106",
        "node_title": "34343242",
        "flag_counts_node_count": "2",
        "node_status": "0"
    },
    {
        "nid": "136",
        "node_title": "Who cares... I selected anonymous!!!!!",
        "flag_counts_node_count": "2",
        "node_status": "0"
    },
    {
        "nid": "144",
        "node_title": "mr mean customer",
        "flag_counts_node_count": "2",
        "node_status": "0"
    },
    {
        "nid": "196",
        "node_title": "flag this",
        "flag_counts_node_count": "2",
        "node_status": "0"
    },
    {
        "nid": "259",
        "node_title": "Jimmy John",
        "flag_counts_node_count": "2",
        "node_status": "0"
    }
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>