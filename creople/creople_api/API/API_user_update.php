<?php require_once("includes/header.php"); ?>

<h1>User - Update</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_user/[uid].json</strong></code> - <code class="sample">PUT</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when users try to update their details.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>name</code> <span class="required">*</span></td>
                <td>Full Name</td>
            </tr>
            <tr>
                <td><code>email</code> <span class="required">*</span></td>
                <td>Email</td>
            </tr>
            <tr>
                <td><code>password</code></td>
                <td>Password</td>
            </tr>
            <tr>
                <td><code>dob</code></td>
                <td>Birthday</td>
            </tr>
            <tr>
                <td><code>phone_number</code></td>
                <td>Phone Number</td>
            </tr>
            <tr>
                <td><code>notify_email</code> <span class="required">*</span></td>
                <td>Notify Email - Enable:1, Disable:0</td>
            </tr>
            <tr>
                <td><code>notify_sms</code> <span class="required">*</span></td>
                <td>Notify SMS - Enable:1, Disable:0</td>
            </tr>
            <tr>
                <td><code>likes</code> <span class="required">*</span></td>
                <td>User's Likes</td>
            </tr>
            <tr>
                <td><code>dislikes</code> <span class="required">*</span></td>
                <td>User's Dislikes</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>status</code></td>
                <td><code class="sample">string</code> - "success"</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 401 Unauthorized : Failed to Update</h3></div>
    <div class="panel-body">
        Returns Validation Fields.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
        
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/5.json - PUT</pre>
        <h5>Response - 200</h5>
        <pre>
{
    "status": "success"
}
        </pre>
        <pre>
{
    "status": "no-valid",
    "errors": {
        "name": "Name is required.",
        "email": "Email address is not valid."
    }
}
        </pre>
        <hr>
        <h5>Response - 406 Not Acceptable</h5>
        <pre>
[
    "There is no user with ID 214."
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>