<?php require_once("includes/header.php"); ?>

<h1>Post - Create Comment</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node/[nid]/comment.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user post a new comment to node.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>subject</code> <span class="required">*</span></td>
                <td>Comment Body</td>
            </tr>
            <tr>
                <td><code>creople_user</code></td>
                <td>To creople user</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns created comment.
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>cid</code></td>
                <td><code class="sample">Int</code> - Comment ID</td>
            </tr>
            <tr>
                <td><code>uri</code></td>
                <td><code class="sample">String</code> - Comment URI</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node/205/comment.json - POST</pre>
        <h5>Response</h5>
        <pre>
{
    "cid": "188",
    "uri": "http://local.creople.com/mobileapi/comment/188"
}
        </pre>
        <hr>
        <h5>Validation Error</h5>
        <pre>
{
    "error_type": "validation",
    "errors":{
        "subject": "Subject is required."
    }
}
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>