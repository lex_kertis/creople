<?php require_once("includes/header.php"); ?>

<h1>User - Retrieve</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_user/[uid].json</strong></code> - <code class="sample">GET</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when try to get user's details.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        None
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>uid</code></td>
                <td><code class="sample">Int</code> - User ID</td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td><code class="sample">String</code> - Username</td>
            </tr>
            <tr>
                <td><code>created</code></td>
                <td><code class="sample">Timestamp</code> - Created Date</td>
            </tr>
            <tr>
                <td><code>status</code></td>
                <td><code class="sample">Int</code> - Status</td>
            </tr>
            <tr>
                <td><code>data</code></td>
                <td><code class="sample">Object</code> - User's other information</td>
            </tr>
            <tr>
                <td><code>roles</code></td>
                <td><code class="sample">Object</code> - Roles</td>
            </tr>
            <tr>
                <td><code>field_notification</code></td>
                <td><code class="sample">Object</code> - Notification</td>
            </tr>
            <tr>
                <td><code>field_name</code></td>
                <td><code class="sample">Object</code> - Full Name</td>
            </tr>
            <tr>
                <td><code>field_date_of_birth</code></td>
                <td><code class="sample">Object</code> - Birthday</td>
            </tr>
            <tr>
                <td><code>field_phone_number</code></td>
                <td><code class="sample">Object</code> - Phone Number</td>
            </tr>
            <tr>
                <td><code>field_likes</code></td>
                <td><code class="sample">Object</code> - Likes</td>
            </tr>
            <tr>
                <td><code>field_dislikes</code></td>
                <td><code class="sample">Object</code> - Dislikes</td>
            </tr>
            <tr>
                <td><code>field_likes_dislikes</code></td>
                <td><code class="sample">Object</code> - Likes and Dislikes</td>
            </tr>
            <tr>
                <td><code>field_profile_picture</code></td>
                <td><code class="sample">Object</code> - Profile Picture</td>
            </tr>
            <tr>
                <td><code>sms_user</code></td>
                <td><code class="sample">Object</code> - SMS User info</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
        
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/5.json - GET</pre>
        <h5>Response</h5>
        <pre>
{
    "uid": "5",
    "name": "Harvey Specter",
    "created": "1410860144",
    "status": "1",
    "timezone": "Asia/Shanghai",
    "data": {
        "overlay": 1,
        "flag_friend_request_notify": 1,
        "flag_friend_approved_notify": 1,
        "contact": 0,
        "notification_received": false,
        "mimemail_textonly": 0,
        "user_relationships_ui_auto_approve":[],
        "user_relationship_mailer_send_mail": true
    },
    "roles": {
        "2": "authenticated user",
        "3": "administrator"
    },
    "field_notification": {
        "und": [
            {
                "value": "Email"
            }
        ]
    },
    "field_date_of_birth": {
        "und":[
            {
                "value": "2015-05-09 00:00:00", 
                "timezone": "Asia/Singapore", 
                "timezone_db": "Asia/Singapore"
            }
        ]
    },
    "field_name": {
        "und":[
            {
                "value": "James",
                "format": null,
                "safe_value": "James"
            }
        ]
    },
    "field_likes": {
        "und":[
            {
                "value": "Sport",
                "format": null,
                "safe_value": "Sport"
            }
        ]
    },
    "field_dislikes": {
        "und":[
            {
                "value": "Disturb",
                "format": null,
                "safe_value": "Disturb"
            }
        ]
    },
    "field_likes_dislikes":{
        "und": [
            {
                "value": "0"
            }
        ]
    },
    "field_phone_number": {
        "und": [
            {
                "value": "555-555-5555"
            }
        ]
    },
    "field_profile_picture":{
        "und": [
            {
                "fid": "229",
                "uid": "213",
                "filename": "james2.jpg",
                "uri": "public://james2_4.jpg",
                "filemime": "image/jpeg",
                "filesize": "5036",
                "status": "1",
                "timestamp": "1433872251",
                "type": "image",
                "field_file_image_alt_text":[],
                "field_file_image_title_text":[],
                "rdf_mapping":[],
                "metadata":{"height": 100, "width": 100},
                "alt": null,
                "title": null,
                "width": "100",
                "height": "100"
            }
        ]
    },
    "sms_user": {
        "uid": 5,
        "number": "",
        "status": 0,
        "code": 0,
        "gateway": "",
        "sleep_enabled": 0,
        "sleep_start_time": 0,
        "sleep_end_time": 0,
        "0": {"uid": 5, "number": "", "status": 0, "code": 0, "gateway": "",…}
    },
}
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>