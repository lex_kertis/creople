<?php require_once("includes/header.php"); ?>

<h1>Post - Comment List</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node/[nid]/comments.json</strong></code> - <code class="sample">GET</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user get the list of comment.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>page</code></td>
                <td>Page number of results to return (in pages of 20). Default: 0</td>
            </tr>
            <tr>
                <td><code>pagesize</code></td>
                <td>Integer number of items to be returned. Default: 20</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns array of Comment object.
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>cid</code></td>
                <td><code class="sample">Int</code> - Comment ID</td>
            </tr>
            <tr>
                <td><code>nid</code></td>
                <td><code class="sample">Int</code> - Node ID</td>
            </tr>
            <tr>
                <td><code>uid</code></td>
                <td><code class="sample">Int</code> - User ID</td>
            </tr>
            <tr>
                <td><code>status</code></td>
                <td><code class="sample">Int</code> - Status</td>
            </tr>
            <tr>
                <td><code>created</code></td>
                <td><code class="sample">Timestamp</code> - Created Date</td>
            </tr>
            <tr>
                <td><code>changed</code></td>
                <td><code class="sample">Timestamp</code> - Changed Date</td>
            </tr>
            <tr>
                <td><code>subject</code></td>
                <td><code class="sample">String</code> - Comment Body</td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td><code class="sample">String</code> - Username</td>
            </tr>
            <tr>
                <td><code>user_picture_url</code></td>
                <td><code class="sample">String</code> - User Profile Image</td>
            </tr>
            <tr>
                <td><code>user_fullname</code></td>
                <td><code class="sample">String</code> - User Fullname</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node/205/comments.json - GET</pre>
        <h5>Response</h5>
        <pre>
[
    {
        "cid": "187",
        "pid": "0",
        "nid": "205",
        "uid": "213",
        "subject": "This is cool.",
        "hostname": "127.0.0.1",
        "created": "1434140508",
        "changed": "1434140507",
        "status": "1",
        "thread": "02/",
        "name": "jointgap",
        "mail": "",
        "homepage": "",
        "language": "und",
        "uri": "http://local.creople.com/mobileapi/comment/187",
        "user_picture_url": "http://local.creople.com/sites/default/files/james2_4.jpg",
        "user_fullname": "James Stewart"
    },
    ...
    {
        ...
    }
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>