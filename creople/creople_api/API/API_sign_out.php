<?php require_once("includes/header.php"); ?>

<h1>Sign Out</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_user/logout.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user log out to site.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Status</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>200</code></td>
                <td>Success</td>
            </tr>
            <tr>
                <td><code>401</code></td>
                <td>CSRF validation failed</td>
            </tr>
            <tr>
                <td><code>406</code></td>
                <td>User is not logged in.</td>
            </tr>
            </tbody>
        </table>
        <h5>Response Status</h5>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request1</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/logout.json - 200 OK</pre>
        <h5>Response1</h5>
        <pre>
[
    true
]
        </pre>
        <h5>Request2</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/logout.json - 406</pre>
        <h5>Response2</h5>
        <pre>
[
    "User is not logged in."
]
        </pre>
        <h5>Request3</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/logout.json - 401</pre>
        <h5>Response3</h5>
        <pre>
[
    "CSRF validation failed"
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>