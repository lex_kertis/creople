<?php require_once("includes/header.php"); ?>

<h1>Forgot Password</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>user/request_new_password.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when users try to update their details.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>name</code> <span class="required">*</span></td>
                <td>A valid user name or e-mail address</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns TRUE / FALSE
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/user/request_new_password.json - POST</pre>
        <h5>Response - 200</h5>
        <pre>
[
    TRUE
]        
        </pre>
        <pre>
[
    FALSE
]
        </pre>
        <hr>
        <h5>Response - 406 Not Acceptable</h5>
        <pre>
[
    "Sorry, <em class=\"placeholder\">aaa</em> is not recognized as a user name or an e-mail address."
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>