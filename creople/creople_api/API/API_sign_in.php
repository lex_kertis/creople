<?php require_once("includes/header.php"); ?>

<h1>Sign In</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_user/login.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user log in to site.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>username</code> <span class="required">*</span></td>
                <td>User Name</td>
            </tr>
            <tr>
                <td><code>password</code> <span class="required">*</span></td>
                <td>Password</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters</h3></div>
    <div class="panel-body">
        <h3>Status: 200 OK</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>sessid</code></td>
                <td><code class="sample">String</code> - Session ID</td>
            </tr>
            <tr>
                <td><code>session_name</code></td>
                <td><code class="sample">String</code> - Session Name</td>
            </tr>
            <tr>
                <td><code>token</code></td>
                <td><code class="sample">String</code> - Token</td>
            </tr>
            <tr>
                <td><code>user</code></td>
                <td><code class="sample">Object</code> - User's information</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
        <h5>After user login successfully, you need to set the for all request <i>HEADER</i> with following values:</h5>
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Value</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>Cookie</code></td>
                <td><code class="sample">session_name=sessid</code></td>
            </tr>
            <tr>
                <td><code>X-CSRF-Token</code></td>
                <td><code class="sample">token</code></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/login.json</pre>
        <h5>Response</h5>
        <pre>
{
    "sessid": "e8d1hMgvYXW0Lj1htAGfrht7IKnS0SEWZtD9Ioerr4M",
    "session_name": "SESS37c44696d9ad42bc281d62a4a8a16b12",
    "token": "jrEf2fN8gruZSNhAF455-_L8z8HO8pM_Y8BS90IcBkE",
    "user": {
        "uid": "199",
        "name": "somone",
        "mail": "chancepianta@gmail.com",
        "created": "1425950363",
        "login": 1433518365,
        "roles": {
            "2":"authenticated user"
        },
        "field_date_of_birth": {
            "und":[{
                "value": "1990-03-13 00:00:00", 
                "timezone": "Asia/Singapore",
                "timezone_db": "Asia/Singapore",
                "date_type": "datetime"
            }]
        },
        "field_name": {
            "und": [{
                "value": "James"
            }]
        },
        "field_phone_number": {
            "und": [{
                "value": "555-555-5555"
            }]
        },
        "field_profile_picture": {
            "und": [{
                "fid": "219",
                "uid": "1",
                "filename": "james.jpg",
                "width": "100",
                "height": "100"
            }]
        }
    }
}
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>