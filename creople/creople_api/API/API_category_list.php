<?php require_once("includes/header.php"); ?>

<h1>Category - List</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_category/list.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user get the list of category.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        None
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns array of Node object.
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>tid</code></td>
                <td><code class="sample">Int</code> - Category ID</td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td><code class="sample">String</code> - Category Title</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_category/list.json - POST</pre>
        <h5>Response</h5>
        <pre>
[
    {
        "tid": "3",
        "vid": "4",
        "name": "Rotten Customers",
        "description": "",
        "format": "filtered_html",
        "weight": "0",
        "depth": 0,
        "parents":[
            "0"
        ]
    },
    {
        "tid": "4",
        "vid": "4",
        "name": "Bullies Unmasked",
        "description": "",
        "format": "filtered_html",
        "weight": "1",
        "depth": 0,
        "parents":[
            "0"
        ]
    },
    ...
    {
        ...
    }
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>