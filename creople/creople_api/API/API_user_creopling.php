<?php require_once("includes/header.php"); ?>

<h1>User - Creopled By</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_user/[uid]/creopling.json</strong></code> - <code class="sample">GET</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user get the creopling users.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        None
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns array of User object.
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="2">
                    User Data
                </td>
            </tr>
            <tr>
                <td><code>creopled_time</code></td>
                <td><code class="sample">Timestamp</code> - Creopled Timestamp</td>
            </tr>
            <tr>
                <td><code>creopled_nid</code></td>
                <td><code class="sample">Int</code> - Node ID</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_user/123/creopling.json - GET</pre>
        <h5>Response</h5>
        <pre>
[
    {
        "uid": "187",
        ...
        "creopled_time": "1435255629"
        "creopled_nid": "270"
    },
    ...
    {
        ...
    }
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>