<?php require_once("includes/header.php"); ?>

<h1>Post - Create</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node/[nid]/update.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called when user update node.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>type</code> <span class="required">*</span></td>
                <td>Node Type - Should be 'posts'</td>
            </tr>
            <tr>
                <td><code>title</code> <span class="required">*</span></td>
                <td>Post Title</td>
            </tr>
            <tr>
                <td><code>category_id</code> <span class="required">*</span></td>
                <td>Category ID</td>
            </tr>
            <tr>
                <td><code>place_name</code></td>
                <td>School/College Name</td>
            </tr>
            <tr>
                <td><code>street</code></td>
                <td>Street</td>
            </tr>
            <tr>
                <td><code>city</code></td>
                <td>City</td>
            </tr>
            <tr>
                <td><code>province</code></td>
                <td>State / Province - Ex: CA</td>
            </tr>
            <tr>
                <td><code>postal_code</code></td>
                <td>Postal Code</td>
            </tr>
            <tr>
                <td><code>country</code></td>
                <td>Country - Ex: us, sg</td>
            </tr>
            <tr>
                <td><code>body</code> <span class="required">*</span></td>
                <td>Post Content</td>
            </tr>
            <tr>
                <td><code>files[picture]</code></td>
                <td>Post Picture (png, gif, jpg, jpeg)</td>
            </tr>
            <tr>
                <td><code>files[video]</code></td>
                <td>Post Video (avi, mp4, 3gp, mov, wmv)</td>
            </tr>
            <tr>
                <td><code>files[audio]</code></td>
                <td>Post Audio (avi, mp4, 3gp, mov, wmv)</td>
            </tr>
            <tr>
                <td><code>make_anonymous</code></td>
                <td>Anonymous post: set 1 if anonymous post</td>
            </tr>
            <tr>
                <td><code>smeared_on</code></td>
                <td>Smeared On</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns array of Node object.
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>nid</code></td>
                <td><code class="sample">Int</code> - Node ID</td>
            </tr>
            <tr>
                <td><code>title</code></td>
                <td><code class="sample">String</code> - Title</td>
            </tr>
            <tr>
                <td><code>created</code></td>
                <td><code class="sample">Timestamp</code> - Created Date</td>
            </tr>
            <tr>
                <td><code>changed</code></td>
                <td><code class="sample">Timestamp</code> - Created Date</td>
            </tr>
            <tr>
                <td><code>status</code></td>
                <td><code class="sample">Int</code> - Status</td>
            </tr>
            <tr>
                <td><code>user_name</code></td>
                <td><code class="sample">String</code> - Writer's username</td>
            </tr>
            <tr>
                <td><code>user_fullname</code></td>
                <td><code class="sample">String</code> - Writer's Full Name</td>
            </tr>
            <tr>
                <td><code>user_picture_url</code></td>
                <td><code class="sample">String</code> - Writer's Profile Image</td>
            </tr>
            <tr>
                <td><code>body</code></td>
                <td><code class="sample">Object</code> - Node Description</td>
            </tr>
            <tr>
                <td><code>field_categories</code></td>
                <td><code class="sample">Object</code> - Node Category</td>
            </tr>
            <tr>
                <td><code>field_address</code></td>
                <td><code class="sample">Object</code> - Node Address</td>
            </tr>
            <tr>
                <td><code>field_geofield</code></td>
                <td><code class="sample">Object</code> - Node Geo Info</td>
            </tr>
            <tr>
                <td><code>comment_count</code></td>
                <td><code class="sample">Int</code> - The number of comment</td>
            </tr>
            <tr>
                <td><code>path</code></td>
                <td><code class="sample">String</code> - The URL of node</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node/170/update.json - POST</pre>
        <h5>Response</h5>
        <pre>
{
    "uid": "170",
    "title": "Messy Boss",
    "status": "1",
    "nid": "235",
    "type": "posts",
    "language": "und",
    "created": "1420685453",
    "changed": "1420685453",
    "body":{
        "und":[
            {
                "value": "Was not that great of a boss"
            }
        ]
    },
    "field_categories":{
        "und":[
            {
                "tid": "5",
                "name": "Bosses / Co-Workers"
            }
        ]
    },
    "field_address":{
        "und":[
            {"lid": "177", "name": "", "street": "", "additional": "",…}
        ]
    },
    "field_geofield":{
        "und":[
            {"geom": null, "geo_type": null, "lat": null, "lon": null, "left": null,…}
        ]
    },
    "field_upload_media":{
        "und":[
            {
                "fid": "234",
                "uid": "213",
                "filename": "james1.jpg",
                "uri": "public://james1_0.jpg",
                "filemime": "image/jpeg",
                "filesize": "28047",
                "status": "1",
                "timestamp": "1434002653",
                "type": "image",
                "field_file_image_alt_text":[],
                "field_file_image_title_text":[],
                "rdf_mapping":[],
                "metadata":{"height": 356, "width": 300},
                "alt": "",
                "title": "",
                "display": "1",
                "description": ""
            }
        ]
    },
    "field_upload_video":[],
    "field_upload_audio":[],
    "comment_count": "3",
    "path": "http://www.creople.com/node/235",
    "user_name": "testuser",
    "user_picture_url": "http://www.creople.com/sites/default/files/default_avatar.png",
    "user_fullname": "test user"
}
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>