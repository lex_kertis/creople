<?php require_once("includes/header.php"); ?>

<h1>Post - Most Popular Posts</h1>
<div class="panel panel-default">
    <div class="panel-heading"><h3>URL</h3></div>
    <div class="panel-body">
        <span class="code">http://www.creople.com/mobileapi/</span><code class=""><strong>creople_node/most_popular.json</strong></code> - <code class="sample">POST</code>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>When will this API called?</h3></div>
    <div class="panel-body">
        This api is called while getting flagged posts.
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Request Parameters</h3></div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Field</th>
                <th class="last">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><code>page</code></td>
                <td>The zero-based index of the page to get, defaults to 0.</td>
            </tr>
            <tr>
                <td><code>pagesize</code></td>
                <td>Number of records to get per page. defaults to 20</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Notes</h3></div>
    <div class="panel-body">
        None
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h3>Response Parameters - 200 OK</h3></div>
    <div class="panel-body">
        Returns array of nodes.
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading"><h3>Sample</h3></div>
    <div class="panel-body">
        <h5>Request</h5>
        <pre>http://www.creople.com/mobileapi/creople_node/most_popular.json - POST</pre>
        <h5>Response</h5>
        <pre>
[
    {
        "uid": "170",
        "title": "Messy Boss",
        "status": "1",
        "nid": "235",
        "type": "posts",
        "language": "und",
        "created": "1420685453",
        "changed": "1420685453",
        "body":{
            "und":[
                {
                    "value": "Was not that great of a boss"
                }
            ]
        },
        "field_categories":{
            "und":[
                {
                    "tid": "5",
                    "name": "Bosses / Co-Workers"
                }
            ]
        },
        "field_address":{
            "und":[
                {"lid": "177", "name": "", "street": "", "additional": "",…}
            ]
        },
        "field_geofield":{
            "und":[
                {"geom": null, "geo_type": null, "lat": null, "lon": null, "left": null,…}
            ]
        },
        "field_upload_media":{
            "und":[
                {
                    "fid": "234",
                    "uid": "213",
                    "filename": "james1.jpg",
                    "uri": "public://james1_0.jpg",
                    "filemime": "image/jpeg",
                    "filesize": "28047",
                    "status": "1",
                    "timestamp": "1434002653",
                    "type": "image",
                    "field_file_image_alt_text":[],
                    "field_file_image_title_text":[],
                    "rdf_mapping":[],
                    "metadata":{"height": 356, "width": 300},
                    "alt": "",
                    "title": "",
                    "display": "1",
                    "description": ""
                }
            ]
        },
        "field_upload_video":[],
        "field_upload_audio":[],
        "comment_count": "3",
        "path": "http://www.creople.com/node/235",
        "user_name": "testuser",
        "user_picture_url": "http://www.creople.com/sites/default/files/default_avatar.png",
        "user_fullname": "test user"
    },
    ...
    {
        ...
    }
]
        </pre>
    </div>
</div>

<?php require_once("includes/footer.php"); ?>