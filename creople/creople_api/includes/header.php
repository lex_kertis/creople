<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <title>Creople App Api</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="css/styles.css" />
<!--    <link rel="shortcut icon" href="images/fav.jpg">-->
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">Creople API</a>
        </div>
<!--        <div class="collapse navbar-collapse">-->
<!--            <ul class="nav navbar-nav">-->
<!--                <li class="active"><a href="#">Home</a></li>-->
<!--                <li><a href="#about">About</a></li>-->
<!--                <li><a href="#contact">Contact</a></li>-->
<!--            </ul>-->
<!--        </div>-->
    </div>
</div>

<div id="api_main_content" class="container">
    <div class="row">
        <?php require_once("includes/left.php") ?>
        <div id="api_content" class="col-sm-9">