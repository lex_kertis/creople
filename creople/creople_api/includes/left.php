<script type="text/javascript">
    var apiParam = "<?php echo (empty($_GET['API'])?'':$_GET['API']); ?>";
</script>
<div id="api_left_menu" class="col-sm-3 blog-sidebar">
    <ul class="nav nav-sidebar">
        <li class="parent active">
            <a href="#">Authenticate</a>
            <ul>
                <li><a href="index.php?API=sign_in.php">Sign In</a></li>
                <li><a href="index.php?API=sign_up.php">Sign Up</a></li>
                <li><a href="index.php?API=sign_out.php">Sign Out</a></li>
                <li><a href="index.php?API=reset_password.php">Forgot Password</a></li>
            </ul>
        </li>
        <li class="parent">
            <a href="#">User</a>
            <ul>
                <li><a href="index.php?API=user_retrieve.php">Retrieve</a></li>
                <li><a href="index.php?API=user_update.php">Update</a></li>
                <li><a href="index.php?API=user_attach_file.php">Attach File</a></li>
                <li><a href="index.php?API=user_nodes.php">Node List</a></li>
                <li><a href="index.php?API=user_creopling.php">Creopled By</a></li>
                <li><a href="index.php?API=user_favourites.php">Favourites</a></li>
            </ul>
        </li>
        <li class="parent">
            <a href="#">Category</a>
            <ul>
                <li><a href="index.php?API=category_list.php">List</a></li>
                <li><a href="index.php?API=category_node_list.php">Nodes</a></li>
            </ul>
        </li>
        <li class="parent">
            <a href="#">Post</a>
            <ul>
                <li><a href="index.php?API=post_list.php">List</a></li>
                <li><a href="index.php?API=post_view.php">View</a></li>
                <li><a href="index.php?API=post_create.php">Create</a></li>
                <li><a href="index.php?API=post_update.php">Update</a></li>
                <li><a href="index.php?API=post_delete.php">Delete</a></li>
                <li><a href="index.php?API=post_search.php">Search</a></li>
                <li><a href="index.php?API=post_vote.php">Vote Score</a></li>
                <li><a href="index.php?API=post_vote_up.php">Vote Up</a></li>
                <li><a href="index.php?API=post_vote_down.php">Vote Down</a></li>
                <li><a href="index.php?API=post_flag.php">Flag</a></li>
                <li><a href="index.php?API=post_flagged.php">Flagged Posts</a></li>
                <li><a href="index.php?API=post_most_popular.php">Most Popular Posts</a></li>
                <li><a href="index.php?API=post_comments.php">Comments</a></li>
                <li><a href="index.php?API=post_create_comment.php">Create Comment</a></li>
                <li><a href="index.php?API=post_favourite.php">Favourite</a></li>
            </ul>
        </li>
        <li class="parent">
            <a href="#">Friends</a>
            <ul>
                <li><a href="index.php?API=friends_list.php">List</a></li>
                <li><a href="index.php?API=friends_requests.php">Requests</a></li>
                <li><a href="index.php?API=friends_pendings.php">Pendings</a></li>
                <li><a href="index.php?API=friends_invite.php">Invite</a></li>
                <li><a href="index.php?API=friends_remove.php">Remove</a></li>
                <li><a href="index.php?API=friends_approve.php">Approve</a></li>
                <li><a href="index.php?API=friends_disapprove.php">Disapprove</a></li>
            </ul>
        </li>
    </ul>
</div>