$(document).ready(function() {

    $('#api_left_menu ul li > a').click(function() {
        if ($(this).parent().hasClass('parent')) {
            $(this).next().slideToggle();

            return false;
        } else {
            $('#api_left_menu ul li').removeClass('menu-hover');
            $(this).parent().addClass('menu-hover');
        }
    });

    $('#api_left_menu ul li a').each(function() {

        if (this.href == '#' || this.href.lastIndexOf('#') == this.href.length - 1) {
            return;
        }

        if (this.href.indexOf(apiParam) >= 0) {
            $(this).addClass('active');
        }
    });
});